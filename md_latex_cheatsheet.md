# 0. Sommaire
- [I. Partie sur les titres](#i-partie-sur-les-titres)
- [II. Partie sur les séparateurs](#ii-partie-sur-les-séparateurs)
- [III. Listes](#iii-listes)
- [IV. Code](#iv-code)
- [V. Traitement de texte](#v-traitement-de-texte)
- [VI. Liens](#vi-liens)
- [VII. Mise en forme](#vii-mise-en-forme)
- [VIII. Trucs randoms](#viii-trucs-randoms)
    - [Video youtube](#video-youtube)
- [IX. Formules mathématiques (LaTeX)](#ix-formules-mathématiques-latex)


[I. Partie sur les titres](#0-sommaire)
=========

# Titre de niveau 1 (#)

Même taille (avec les =======)
====================================

## Titre niveau 2 (##)
### Titre niveau 3 (###)
#### Titre niveau 4 (####)
##### Titre niveau 5 (#####)
###### Titre niveau 6 (niveau max) (######)



[II. Partie sur les séparateurs](#0-sommaire)
===================================

# Séparateur automatique pour les textes de niveau 1 (#)

Séparateur de texte normal (texte plus gros)
-------------

### Séparateur de titre (2-6 # + -----)
-----------

1. Liste avec numérotation automatique (< chiffre > .)
9999999. Le nombre avant le point importe peu.
42. BlaBlaBla



[III. Listes](#0-sommaire)
==================================

- avec tiret 1
- avec tiret 2
- avec tiret 3
* avec asterix 1
* avec asterix 2
* avec asterix 3
- Séparation automatique des listes,
- selon le signe.

### Listes avec checkbox :
- [x] Truc fait.
- [ ] truc à faire.




[IV. Code](#0-sommaire)
==================================

    function fond noir genre shell (>1 tabulation).
        2 tabulations.
            3 tabs etc.
            
`surlignage` (d'une commande par exemple).

Morceau de code :

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```


[V. Traitement de texte](#0-sommaire)
===================================
~~Barrer une ligne~~, c'est fait.

_Mettre en italique_, c'est fait.

**Mettre en gras**, c'est fait.
__On peut aussi faire comme ça__



[VI. Liens](#0-sommaire)
===============================
[Voici comment faire un lien http](https://www.google.com) 
<- tu peux cliquer.

[Voici un lien avec tooltip](https://www.google.com "Tooltip que tu veux").



[VII. Mise en forme](#0-sommaire)
================================
### Tableau :
| Tables        | Are           | Cool  |
| ------------- | :-----------: | ----: |
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      | $12   |
| zebra stripes | are neat      | $1    |

### Pas besoin de mettre les bons espaces : 
| Markdown | Less      | Pretty     |
| -------- | --------- | ---------- |
| *Still*  | `renders` | **nicely** |
| 1        | 2         | 3          |

### Citations :
> Ceci est un block de citation.
> Il peut rassembler plusieurs lignes.
> Et les **traitements habituels fonctionnent**.

> _Pas de retour à la ligne possible sans découper la citation._



[VIII. Trucs randoms](#0-sommaire)
================================
## Video youtube :
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/QFfEneG_pIo/0.jpg)](http://www.youtube.com/watch?v=QFfEneG_pIo)
## On peut utiliser du html traditonnel :
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>


<style TYPE="text/css">
code.has-jax {font: inherit; font-size: 150%; background: inherit; border: inherit;}
.MathJax {
font-size: 1.3em;
}
</style>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [['$','$'], ['\\(','\\)']],
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'] // removed 'code' entry
    }
});
MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax(), i;
    for(i = 0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML' async></script>

[IX. Formules mathématiques (LaTeX)](#0-sommaire)
====================================
* Pas géré de base par Markdown. `Cependant` voir les toutes premières lignes du fichier pour voir comment inclure un plugin javascript qui permet de gérer celà !
* On peut utiliser `mathjax` !

### Cheasheet détaillé :
-----
[MathJax basic tutorial and quick reference](https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference) 

### Symboles clés : 
------------------------------------
* affichage centré de la formule :

        DOLLAR DOLLAR <formule latex> DOLLAR DOLLAR (DOLLAR = $)
* affichage normal de la formule (en début de ligne) :
        
        DOLLAR <formule latex> DOLLAR (DOLLAR = $)

### Formules
----------------------------------
#### `Tabulation` :
    \quad

#### `Indices (var_ind)` :
$$
x_i, P_{min} 
$$

#### `Puissance / exposant` :
$
x^2, 10 ^ {-15} 
$
#### `Accolades verticales` :


#### `Fraction` :
$ \Huge{ 
    \frac{ x_{numerateur} }{ y_{denominateur} }
}$

#### `Accolades horizontales` :
* verticales :
$ \quad \Large { 
E =  \\{  e_1, e_2, ..., e_n  \\}
}$

* horizontales :
$ \quad\quad \Large{ 
    \overbrace{dessous}^{dessus}, \quad
    \underbrace{dessus}_{dessous} 
}$

#### `Barre supérieure` :
    $ \overline{A}$
 
$\quad \LARGE{ \overline{A}}$


#### `Tailles de police` :

| Taille { formule } | $ x_a^2 $                  |
| ------------------ | -------------------------: |
| \tiny { }          | $ \tiny{ x_a^2 } $         |
| \scriptsize { }    | $ \scriptsize{ x_a^2  } $  |
| \footnotesize { }  | $ \footnotesize{ x_a^2 } $ |
| \small { }         | $  \small{ x_a^2  } $      |
| \normalsize { }    | $  \normalsize{ x_a^2  } $ |
| \large { }         | $  \large{ x_a^2  } $      |
| \Large { }         | $  \Large{ x_a^2  } $      |
| \LARGE { }         | $  \LARGE{ x_a^2  } $      |
| \huge { }          | $  \huge{ x_a^2  } $       |
| \Huge { }          | $  \Huge{ x_a^2  } $       |

$
 \large{ ne fonction pas pour autre chose que des formules }
$
$
 \large{ \text{Sauf si on fait ça. :^) } }
$

#### `Symboles` :
[Lien vers un tableau plus exhaustif](https://www.commentcamarche.net/contents/620-latex-table-de-caracteres)

| En français       | En LaTex  | Résultat |
| ----------------- | :-------: | -------: |
| ∈ (appartient)    | \in       | $ \in $  |
| <= (inf. ou égal) | \le       | $ \le $  |
| ∅ ens. vide       | \emptyset | $ \emptyset $  |
| -> flèche droite | \Rightarrow | $ \Rightarrow $ |
| -> flèche gauche | \Leftarrow | $ \Leftarrow $ |
| <-> equivalent | \Leftrightarrow| $ \Leftrightarrow $ |
| Ω Oméga | \Omega | $ \Omega $ |
| ⊂ strictement inclus | \subset | $ \subset $ |
| ⊆ inclus (ou égal) | \subseteq | $ \subseteq $ |
| U union | \cup | $ \cup $ |
| ∩ intersection | \cap | $ \cap $ |
| ∞ infini | \infty | $ \infty $ |