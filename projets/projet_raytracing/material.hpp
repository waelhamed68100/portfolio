///@file material : indique comment une surface impacte un rayon lumineux qui l'atteint (comment le rayon rebondit et quelle est la couleur du point de collision)
#ifndef MATERIAL_H
#define MATERIAL_H

#include "rayon.hpp"
#include "intersec_data.hpp"

class intersec_data; // dépendances circulaires

class material {
public: 

    /** @brief calcule le nouveau rayon lumineux (de rendu) sortant à partir de celui entrant
     * @param rayon_entrant le rayon touchant la surface
     * @param inter_data Données de l'intersection
     * @param attenuation Paramètres pour le calcul du nouveau rayon sortant
     * @param rayon_sortant Le nouveau rayon sortant calculé (rebondissant ou absorbé) 
     */
    virtual bool calcul_impacte(
        const rayon& rayon_entrant, const intersec_data& inter_data, 
        vec3& attenuation, rayon& rayon_sortant
    ) const = 0;

    /** @brief calcule la lumière émise au point d'intersection
     * @param inter_data Données de l'intersection
     * @note De base les materiaux n'emettent pas de lumière : 
     * les classes filles de material qui émettent de la lumière doivent override cette fonction
     */
    virtual vec3 emission_lumiere(const intersec_data& inter_data) const {
        //aucune lumière émise par défaut
        return vec3(0,0,0);
    }
};

#endif //MATERIAL_H
