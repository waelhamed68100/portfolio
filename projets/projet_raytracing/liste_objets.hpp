///@file Représente une liste d'objets visibles (que les rayons de rendu peuvent intercepter)
#ifndef LISTE_OBJETS_H
#define LISTE_OBJETS_H

#include "objet.hpp"

class liste_objets: public objet {
public: 

    objet **liste; ///liste contenant les objets
    int nb_elem; ///nombre d'éléments dans la liste

    //constructeur basique
    liste_objets() {}
    //constructeur complet
    liste_objets(objet ** list, int nb) { 
        liste = list; 
        nb_elem = nb;
        /*
        printf("liste_objets : nb_elem : %d\n", nb);
        for(int i = 0; i < nb; i++){
            vec3 p = list[i]->centre;
            printf("liste_objets: position élément %i : %f, %f, %f\n",i, p.x(), p.y(), p.z());
        } 
        */
    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool liste_objets::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    intersec_data derniere_inter; //dernière intersection pour ce rayon 
    bool une_collision = false; //indique s'il y a eu au moins une collision
    float dist_plus_proche = dist_max; //indique la distance à l'objet le plus proche rencontré pour l'instant
    //calcul des collisions entre le rayon et chacun des objets de la scene
    for(int i = 0; i < nb_elem; i++){
        //s'il y eu collision avec un objet et dans la limite raccourcie
        if(liste[i]->collision(ray, dist_min, dist_plus_proche, derniere_inter)
        ) {
            //on met à jour les données de la collision associées au rayon
            une_collision = true;
            dist_plus_proche = derniere_inter.dist;
            inter_data = derniere_inter; //on ne garde que l'intersection avec l'objet le plus proche
        }
    }
    return une_collision;
};

#endif //LISTE_OBJETS_H
