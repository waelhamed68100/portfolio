///@file Texture : indique la couleur du point d'intersection d'un objet avec le rayon lumineux

#ifndef TEXTURE_H
#define TEXTURE_H

#include "intersec_data.hpp"

class texture {
public: 

    /** @brief Indique la couleur au point d'intersection
     * @param inter_data Données de l'intersection entre l'objet et le rayon
     */
    virtual vec3 valeur(const intersec_data& inter_data) const = 0;
};

#endif //TEXTURE_H