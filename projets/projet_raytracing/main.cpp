#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <float.h> // FLT_MAX

#include <omp.h>

#include "vec3.hpp"
#include "rayon.hpp"
#include "liste_objets.hpp"
#include "sphere.hpp"
#include "camera.hpp"
#include "lambert_mat.hpp"
#include "metal_mat.hpp"
#include "uniform_texture.hpp"
#include "damier_texture.hpp"
#include "rect_xy.hpp"
#include "rect_xz.hpp"
#include "rect_yz.hpp"
#include "inverse_normales_op.hpp"
#include "cube.hpp"
#include "normales_textures.hpp"
#include "lumineux_material.hpp"
#include "pave_droit.hpp"
#include "rotation_y_op.hpp"
#include "eponge_menger.hpp"
#include "bvh_eponge_menger.hpp"
#include "cylindre_y.hpp"
#include "cone_y.hpp"

//dimensions de l'image produite
int _largeur, _hauteur;
//nombre de lancés de rayon de rendu par pixel de l'image finale
int _nb_traces;

//couleurs 

vec3 BLANC;
vec3 BLEU;
vec3 ROUGE;
vec3 VERT;
vec3 NOIR;
vec3 GRIS;

vec3 TURQUOISE;
vec3 FUSHIA;
vec3 ROSE;
vec3 BLEU_CIEL;
vec3 JAUNE;
vec3 GOLD_ALBEDO;

//couleurs du fond de la scène
vec3 couleur_fond_bas; 
vec3 couleur_fond_haut;

#define NB_MAX_REBONDS  50 //indique le nombre de rebonds max que peut effectuer un rayon

//référence vers la caméra
camera *cam;

//référence vers la liste des objets de la scène
liste_objets* scene;

/** @brief Calcul la couleur du rayon r lancé 
 *  @param ray Rayon de rendu
 * @param scene Liste d'objets visibles dans la scène
 * @param rebonds Nombre de rebonds du rayon de rendu pour le pixel courant  
 */
vec3 calc_couleur(const rayon& ray, liste_objets* scene, int rebonds) {
    intersec_data inter_data; //données d'intersection du rayon avec les objets
    
    //si collision avec au moins un objet de la scène
    if( (scene)->collision(ray, 0.001, FLT_MAX, inter_data)) {
        
        //lumière (rayon) est modifiée selon le material de l'objet touché
        rayon rayon_sortant; //rayon après rebondissement
        vec3 attenuation; //attenuation lumineuse (variation de la couleur finale du pixel)
         //lumière émise par l'objet touché par le rayon de rendu = couleur ajoutée à la valeur finale du pixel
        vec3 lumiere_emise = inter_data.ptr_material->emission_lumiere(inter_data);

        //on calcule la couleur du pixel après le rebond selon le material de l'objet touché
        if(rebonds < NB_MAX_REBONDS 
            && inter_data.ptr_material->calcul_impacte(ray, inter_data, attenuation, rayon_sortant)){
                rayon_sortant.B = vect_unit(rayon_sortant.B);
                return lumiere_emise + attenuation * calc_couleur(rayon_sortant, scene, rebonds + 1);
        }
        //si le photon a rebondi le max de fois ou que le rayon sortant n'a rien touché
        else {
            //représente la force des ombres
            //return vec3(0,0,0);
            return lumiere_emise;
        }
    }
    else {
        //si aucune intersection alors couleur du fond en fonction de la direction du rayon lancé
        vec3 vecteur_unit = vect_unit(ray.direction());
        //interpolation de la couleur blanche vers bleue selon la direction y du rayon
        float i = 0.5 * (vecteur_unit.y() + 1.0);
        return (1.0 - i) * couleur_fond_bas + i * couleur_fond_haut;
    }
    
}

//nettoyage de la scène précédente si elle existe
void cleanup_scene(){
    if(cam != NULL) free(cam);

    if(scene != NULL) {
        free(scene->liste);
        free(scene);
    }
}

void setup_scene_test_cone(){
    //nettoyage scène précédente
    cleanup_scene();

    //caméra pour tester les axes
    cam = new camera(
        75, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(0, 3.5, 2),// position (légèrement surélevée)
        vec3(0,0,0),//on regarde vers le centre de la scène (le cube)
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = ROSE;
    couleur_fond_haut = BLANC;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 3;
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets));
    //objet **liste = new objet*[nb_objets];
    int ind = 0;
    
    //cube à gauche
    liste[ind++] = new cube(
        vec3(-1.5, 0, 0), 1,
        new lambert_mat(new uniform_texture(BLANC))
    );
    

    liste[ind++] = new cone_y(
        vec3(0, 0.5, 0), 1, 0.5,
        new lambert_mat(new uniform_texture(BLANC))
    );
  
    //sol à y = -0.5
    liste[ind++] = new rect_xz(
        -10,10,-10,10, -0.5, 
        new lambert_mat(
            new damier_texture(
                new uniform_texture(ROSE),
                new uniform_texture(TURQUOISE),
                0.06
            )
        )
    );

    scene = new liste_objets(liste, nb_objets);
    //scene = new BVH_noeud(0, nb_objets, liste);
}

//affichage des primitives
void setup_scene_primitives(){
    //nettoyage scène précédente
    cleanup_scene();

    //caméra pour tester les axes
    cam = new camera(
        65, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(0,2,5),// position (légèrement surélevée)
        vec3(0,0,1.5),//on regarde vers le centre de la scène (le cube)
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = ROSE;
    couleur_fond_haut = BLANC;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 5;
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets*2));
    //objet *s*liste = new objet*[nb_objets];
    int ind = 0;

    float marge = 0.25;

    //material des primitives
    //material *mat = new lambert_mat(new normales_texture());
    material *mat = new lambert_mat(new uniform_texture(BLANC));

    //sol à y = -1
    liste[ind++] = new rect_xz(
        -10,10,-10,10, -0.5, 
        new lambert_mat(
            //new uniform_texture(BLANC)
            
            new damier_texture(
                new uniform_texture(ROSE),
                new uniform_texture(TURQUOISE),
                0.06
            )
        )
    );

    //sphère au milieu 
    liste[ind++] = new sphere(
        vec3(0,0,0), 0.5, 
        mat
    );
    //cylindre à droite
    liste[ind++] = new cylindre(
        vec3(1+marge,0,0), 0.5, 
        1,
        mat
        /*new lambert_mat(
            new damier_texture(
                new uniform_texture(ROUGE),
                new uniform_texture(JAUNE),
                0.06
            )
        )*/
    );

    //cube à gauche
    liste[ind++] = new cube(
        vec3(-1-marge, 0, 0), 1,
        mat
    );

    //cone en face à gauche
    liste[ind++] = new cone_y(
        vec3(-0.5-marge, 0, 2), 1, 0.5,
        mat
    );

    //scene = new liste_objets(liste, nb_objets);
    scene = new BVH_noeud(0, nb_objets, liste);
}

//affichage d'une éponge de menger avec optimisation bvh
void setup_scene_bvh_eponge_menger(){
        //nettoyage scène précédente
    cleanup_scene();

    //caméra pour tester les axes
    cam = new camera(
        65, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(0.75,0.7,2),// position (légèrement surélevée)
        vec3(0,-0.1,0),//on regarde vers le centre de la scène (le cube)
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = ROSE;
    couleur_fond_haut = BLANC;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 2;
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets));
    //objet **liste = new objet*[nb_objets];
    int ind = 0;
    
    //sol à y = -1
    liste[ind++] = new rect_xz(
        -10,10,-10,10, -0.5, 
        new lambert_mat(
            new damier_texture(
                new uniform_texture(ROSE),
                new uniform_texture(TURQUOISE),
                0.06
            )
        )
    );

    //avec rotation    
    /*
    liste[ind++] = new rotation_y_op( new bvh_eponge_menger(
        vec3(0, 0, 0), 1,
        3, 
        new lambert_mat(new uniform_texture(BLANC)),
        NULL
    ), -30);
    */

    //sans rotation
    liste[ind++] = new bvh_eponge_menger(
        vec3(0, 0, 0), 1,
        3, 
        new lambert_mat(new uniform_texture(BLANC)),
        NULL
    );
    
    scene = new liste_objets(liste, nb_objets);
}

//affichage d'au moins une éponge de menger 
void setup_scene_eponge_menger(){
    //nettoyage scène précédente
    cleanup_scene();

    //caméra pour tester les axes
    cam = new camera(
        65, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(0.75,0.7,2),// position (légèrement surélevée)
        vec3(0,-0.1,0),//on regarde vers le centre de la scène (le cube)
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = ROSE;
    couleur_fond_haut = BLANC;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 2;
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets));
    //objet **liste = new objet*[nb_objets];
    int ind = 0;
    
    //sol à y = -1
    liste[ind++] = new rect_xz(
        -10,10,-10,10, -0.5, 
        new lambert_mat(
            new damier_texture(
                new uniform_texture(ROSE),
                new uniform_texture(TURQUOISE),
                0.06
            )
        )
    );
    
    
    liste[ind++] = new eponge_menger(
        vec3(0, 0, 0), 1,
        3, 
        new lambert_mat(new uniform_texture(BLANC))
    );
    
    scene = new liste_objets(liste, nb_objets);
}

// boite de Cornell 
void setup_scene_boite_cornell() {
    //nettoyage scène précédente
    cleanup_scene();

    //caméra pour tester les axes
    cam = new camera(
        60, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(0,0, 2.25), // position (légèrement surélevée)
        vec3(0,0,0), //on regarde vers le centre de la scène (le cube)
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = NOIR;
    couleur_fond_haut = NOIR;

    //boite externe (cube avec face avant manquante)

    vec3 centre = vec3(0,0,0);
    float cote = 1;
    material *mur_blanc = new lambert_mat(new uniform_texture(BLANC));
    material *mur_rouge = new lambert_mat(new uniform_texture(ROUGE));
    material *mur_vert = new lambert_mat(new uniform_texture(VERT));

    float mc = cote / 2; //moitié d'un côté

    // 5 faces de la boite
    rect_xz *bas, *haut; 
    rect_yz *gauche, *droite;
    rect_xy *avant, *arriere;

    bas = new rect_xz(
        centre.x() - mc, centre.x() + mc,
        centre.z() - mc, centre.z() + mc,
        centre.y() - mc,
        mur_blanc
    ); 

    haut = new rect_xz(
        centre.x() - mc, centre.x() + mc,
        centre.z() - mc, centre.z() + mc,
        centre.y() + mc,
        mur_blanc
    ); 

    gauche = new rect_yz(
        centre.y() - mc, centre.y() + mc,
        centre.z() - mc, centre.z() + mc,
        centre.x() - mc,
        mur_rouge
    ); 

    droite = new rect_yz(
        centre.y() - mc, centre.y() + mc,
        centre.z() - mc, centre.z() + mc,
        centre.x() + mc,
        mur_vert
    ); 

    arriere = new rect_xy(
        centre.x() - mc, centre.x() + mc,
        centre.y() - mc, centre.y() + mc,
        centre.z() - mc,
        mur_blanc
    ); 

    objet **faces_ = new objet*[5];

    //normales inversées par rapport à un cube habituel 
    //(pck on regarde l'interieur de la boite)
    faces_[0] = bas;
    faces_[1] = new inverse_normales_op( haut );
    faces_[2] = gauche;
    faces_[3] = new inverse_normales_op( droite );
    faces_[4] = arriere;

    //définition de la scène (liste d'objets visibles)
    int nb_objets = 4;
    //objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets*50));
    objet **liste = new objet*[nb_objets];
    int ind = 0;

    //boite
    liste[ind++] = new liste_objets(faces_, 5); 

    float decalage_mur = 0.05;

    //le cube
    float cote_cube = 0.3;
    liste[ind++] = new rotation_y_op( new cube(
        vec3(0.5 -cote_cube - decalage_mur + 0.05, -0.5 + cote_cube/2, 0.5-cote_cube), 
        cote_cube,
        new lambert_mat(new uniform_texture(BLANC)) 
    ), -15);

    //le pavé droit
    float pv_l = 0.3; //largeur
    float pv_h = 0.6; // hauteur
    float pv_p = 0.3; // profondeur

    liste[ind++] = new rotation_y_op( new pave_droit(
        vec3(-0.5 + pv_l + decalage_mur, -0.5 + pv_h/2, -0.5 + pv_p), pv_l, pv_h, pv_p,
        new lambert_mat(new uniform_texture(BLANC)) 
    ),15);

    //la lumière au plafond
    float cote_lumiere = 0.5; //largeur du carré de lumière
    float mcl = cote_lumiere / 2; //moitie de la largeur
    liste[ind++] = new rect_xz(
        -mcl, mcl, -mcl, mcl,
        0.49,
        //lumière légèrement jaune pck plus réaliste
        new lumineux_material(new uniform_texture(vec3(1,1,0.5)), 0.7)
    );

    scene = new liste_objets(liste, nb_objets);
}

//test materiau qui émet de la lumière
void setup_scene_emission_lumiere() {
    //nettoyage scène précédente
    cleanup_scene();

    //caméra pour tester la lumière
    cam = new camera(
        80, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(0,0,1.2),// position (légèrement surélevée)
        vec3(0,-0.5,0),//on regarde vers le centre de la scène (le cube)
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = NOIR;
    couleur_fond_haut = NOIR;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 6;
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets));
    //objet **liste = new objet*[nb_objets];
    int ind = 0;
    
    //sol à y = -0.5
    float val_gris = 0.7;
    liste[ind++] = new rect_xz(
        -10,10,-10,10, -0.5, 
        new lambert_mat(
            new damier_texture(
                new uniform_texture(BLANC),
                new uniform_texture(vec3(val_gris, val_gris, val_gris)),
                0.1
            )
        )
    );

    //sphere unitaire au chentre du repère
    liste[ind++] = new sphere(
        vec3(0, 0, 0), 0.5,
        new lambert_mat(new uniform_texture(BLANC))
    );

    //carré lumineux bicolor :)
    vec3 pos_rect = vec3(0.4, -0.6, 0); 
    float largeur_cote = 1;
    largeur_cote /= 2;

    liste[ind++] = new rect_yz(
        //-0.4, -0.2, 0.5, 0.7,  0.25,
        pos_rect.y() - largeur_cote, pos_rect.y() + largeur_cote, 
        pos_rect.z() - largeur_cote, pos_rect.z() + largeur_cote,
        pos_rect.x(),
        //new lambert_mat(new uniform_texture(ROUGE))
        //new lumineux_material(new uniform_texture(BLANC))
        new lumineux_material(
            new damier_texture(
                new uniform_texture(TURQUOISE),
                new uniform_texture(ROSE),
                0.01
            ),
            1
        )
    );
    
    //sphères lumineuses / illuminées posées sur le sol
    float rayon_sphere;
    rayon_sphere = 0.2;
    liste[ind++] = new sphere(
        vec3(-0.5, (-0.5 + rayon_sphere), 0.35), rayon_sphere,
        //vec3(0, 0, 0.1), 0.5,
        new lumineux_material(
            new uniform_texture(BLANC),
            1.5
        )
    );

    rayon_sphere = 0.05;
    liste[ind++] = new sphere(
        vec3(0, (-0.5 + rayon_sphere), 0.5), rayon_sphere,
        new lumineux_material(
            new uniform_texture(ROUGE),
            1
        )
    );

    rayon_sphere = 0.07;
    liste[ind++] = new sphere(
        vec3(-0.2, (-0.5 + rayon_sphere), 0.35), rayon_sphere,
        new lambert_mat(new uniform_texture(JAUNE))
    );

    scene = new liste_objets(liste, nb_objets);
}

//test axes bien placés
void setup_scene_axes() {
    //nettoyage scène précédente
    cleanup_scene();

    //caméra pour tester les axes
    cam = new camera(
        80, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(2,2,2),// position (légèrement surélevée)
        vec3(0,0,0),//on regarde vers le centre de la scène (le cube)
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = BLANC;
    couleur_fond_haut = BLANC;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 5;
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets));
    //objet **liste = new objet*[nb_objets];
    int ind = 0;
    
    //sol à y = -1
    liste[ind++] = new rect_xz(
        -10,10,-10,10, -1, 
        new lambert_mat(new uniform_texture(ROSE))
    );

    //sphere unitaire au chentre du repère
    liste[ind++] = new sphere(
        vec3(0, 0, 0), 0.5,
        //new lambert_mat(new uniform_texture(BLANC))
        new metal_mat(
            vec3(0.5,0.5,0.5),
            0
        )
    );

    //cubes unitaires sur chacun des axes orthogonaux
    float taille_cote = 0.5;
    liste[ind++] = new cube(
        vec3(1, 0, 0), taille_cote,
        //new lambert_mat(new uniform_texture(BLANC))
        new lambert_mat(
            new damier_texture(
                new uniform_texture(ROUGE),
                //new uniform_texture(NOIR),
                new normales_texture(),
                0.06
            )
        )
    );
        liste[ind++] = new cube(
        vec3(0, 1, 0), taille_cote,
        //new lambert_mat(new uniform_texture(BLANC))
        new lambert_mat(
            new damier_texture(
                new uniform_texture(VERT),
                //new uniform_texture(NOIR),
                new normales_texture(),
                0.06
            )
        )
    );
        liste[ind++] = new cube(
        vec3(0, 0, 1), taille_cote,
        //new lambert_mat(new uniform_texture(BLANC))
        new lambert_mat(
            new damier_texture(
                new uniform_texture(BLEU),
                //new uniform_texture(NOIR),
                new normales_texture(),
                0.06
            )
        )
    );

    scene = new liste_objets(liste, nb_objets);
}

//test d'orientation des normales du cube
void setup_scene_b() {
    //nettoyage scène précédente
    cleanup_scene();

    bool test = false; //vrai si scene en mode test, false pour le screenshot

    if(test) {
        //caméra pour tester les axes
        cam = new camera(
            90, //fov
            float(_largeur)/float(_hauteur), //ratio écran
            vec3(0,-2,0), // position 
            vec3(0,0,0), //on regarde vers le centre de la scène (le cube)
            vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
        );
    }
    else {
        //joli angle
        cam = new camera(
            65, //fov
            float(_largeur)/float(_hauteur), //ratio écran
            vec3(1,1,-3), // position (légèrement surélevée)
            vec3(0,0,0), //on regarde vers le centre de la scène
            vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
        );
    }

    //fond de la scène
    couleur_fond_bas = test ? NOIR : NOIR;
    couleur_fond_haut = test ? NOIR : NOIR;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = test ? 6 : 7;
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets ));
    //objet **liste = new objet*[nb_objets];
    int ind = 0;
    
    if(!test){
        //sol réfléchissant
        liste[ind++] = new rect_xz(
            -10,10,-10,10, -0.75, 
            new metal_mat(
                vec3(0.5,0.5,0.5),
                0.025
            )
        );
    }

    //grosse sphère réfléchissante
    liste[ind++] = new sphere(
        vec3(-1.5, 0.85, 1.5), 1,
                new metal_mat(
            vec3(0.5,0.5,0.5),
            0
        )
    );

    //cube centré cotés parallèles aux plans des axes
    liste[ind++] = new cube(
        vec3(0, 0, 0), 1,
        //new lambert_mat(new uniform_texture(BLANC))
        new lambert_mat(
            new damier_texture(
                new uniform_texture(BLANC),
                //new uniform_texture(NOIR),
                new normales_texture(),
                0.06
            )
        )
    );

    //boule rose
    liste[ind++] = new sphere(
        vec3(-1, 0, -1), 0.35,
        //new lambert_mat(new uniform_texture(ROSE))
        new lumineux_material(new uniform_texture(ROSE),1)
    );

    //boule jaune
    liste[ind++] = new sphere(
        vec3(1, 0, 1), 0.5,
        //new lambert_mat(new uniform_texture(JAUNE))
        new lumineux_material(new uniform_texture(JAUNE),1)
    );

    //boule blanche dessus
    liste[ind++] = new sphere(
        vec3(0, 2.5, 0), 0.3,
        //new lambert_mat(new uniform_texture(JAUNE))
        new lumineux_material(new uniform_texture(BLANC),1)
    );

    //autre boule blanche dessous
    liste[ind++] = new sphere(
        vec3(0, -2.5, 0), 0.3,
        //new lambert_mat(new uniform_texture(JAUNE))
        new lumineux_material(new uniform_texture(BLANC),1)
    );

    scene = new liste_objets(liste, nb_objets);
}

//charge les éléments de la scène a : sphères colorées
void setup_scene_a() {
    //nettoyage scène précédente
    cleanup_scene();

    //définition de la caméra (point de vue et angle de vue)
    cam = new camera(
        110, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(0,1,0.5), // position (légèrement surélevée)
        vec3(0.5 ,-0.25, -1.1), //on regarde une des sphères
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = TURQUOISE;
    couleur_fond_haut = BLANC;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 11;
    //objet *liste[nb_objets];
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets)); 
    int ind = 0;

    //sol (sphere géante)
    liste[ind++] = new sphere(
        vec3(0, -1000.5, -1), 1000,
        new lambert_mat(new uniform_texture(ROSE))
    );

    //sphere bleue
    liste[ind++] = new sphere(
        vec3(0,0, -1.25), 0.5,
        new lambert_mat(new uniform_texture(BLEU_CIEL))
    );

    //petite sphère jaune (test collision avec sphère bleue)
    liste[ind++] = new sphere(
        vec3(0.5 ,-0.25, -1.1), 0.25,
        new lambert_mat(new uniform_texture(JAUNE))
    );

    //sphére jaune métallique
    liste[ind++] = new sphere(
        vec3(1.5, 0.25, -2), 0.3,
        new metal_mat(vec3(GOLD_ALBEDO),0)
    );

    //sphère métallique rugueuse
    liste[ind++] = new sphere(
        vec3(-1,0, -1.5), 0.5,
        new metal_mat(vec3(0.8,0.8,0.8),0.5)
    );

    //sphère métallique lisse
    liste[ind++] = new sphere(
        vec3(-1,-0.5, -0.45), 0.3,
        new metal_mat(vec3(0.5,0.5,0.5),0)
    );

    //test damier
    liste[ind++] = new sphere(
        vec3(1.5 , -0.2 , -0.5), 0.4,
        new lambert_mat(
            new damier_texture(
                new uniform_texture(BLANC),
                new uniform_texture(NOIR),
                0.01
            )
        )
    );

    //test mini damier (diff largeur carreaux)
    liste[ind++] = new sphere(
        vec3(1.5, -0.35, 0.1), 0.15,
        new lambert_mat(
            new damier_texture(
                new uniform_texture(TURQUOISE),
                new uniform_texture(JAUNE),
                0.01
            )
        )
    );

    //test rectangle parallèle au plan xy
    liste[ind++] = new rect_xy(
        0, 1, 0, 0.6, -2, 
        new lambert_mat(
            new damier_texture(
                new uniform_texture(BLANC),
                new uniform_texture(NOIR),
                0.01
            )
        )
    );

    //test rectangle parallèle au plan xz
    liste[ind++] = new rect_xz(
        0, 0.5, -0.35, 0.1,  -0.3, 
        new lambert_mat(
            new damier_texture(
                new uniform_texture(BLANC),
                new uniform_texture(NOIR),
                0.01
            )
        )
    );

    //test rectangle parallèle au plan yz
    liste[ind++] = new rect_yz(
        0, 0.5, -0.25, 0, 1.75, 
        new lambert_mat(
            new damier_texture(
                new uniform_texture(BLANC),
                new uniform_texture(NOIR),
                0.01
            )
        )
    );

    bool opti_bvh = false; //indique s'il faut optimiser l'ensemble des objets avec un bvh
    if(opti_bvh) {
        printf("opti_bvh pour les objets de la scène\n");
        scene = new BVH_noeud(0, nb_objets, liste);
        //note : version bvh plus lente (normale du fait du nombre et des type d'intersections dans cette scene)
    } else {
        scene = new liste_objets(liste, nb_objets);
    }
}

//charge la scène similaire à celle dans le pdf du sujet 
//(plateau avec plusieurs éponges de menger)
void setup_scene_sujet() {
    //nettoyage scène précédente
    cleanup_scene();

    //définition de la caméra (point de vue et angle de vue)
    cam = new camera(
        100, //fov
        float(_largeur)/float(_hauteur), //ratio écran
        vec3(1.5,2.25,5), // position (légèrement surélevée)
        vec3(0,0,0), //on regarde le centre du repère
        vec3(0,1,0) //rotation de la caméra sur elle même (aucune ici)
    );

    //fond de la scène
    couleur_fond_bas = NOIR;
    couleur_fond_haut = NOIR;
    
    //définition de la scène (liste d'objets visibles)
    int nb_objets = 9;
    //objet *liste[nb_objets];
    objet **liste = (objet**)malloc(sizeof(objet) * (nb_objets)); 
    int ind = 0;

    //sol (plateau cylindre)
    liste[ind++] = new cylindre(
        vec3(0,0,-1), 5, 0.1,
        //new lambert_mat(new uniform_texture(NOIR))
        new metal_mat(GRIS, 0.01)
    );

    //éponge central
    float cote = 2.85;
    liste[ind++] = new bvh_eponge_menger(
        vec3(0,cote/2, 0), cote, 4,
        new lambert_mat(new uniform_texture(BLANC)),
        NULL
    );

    //point de départ avant rotation
    float x_d = -3;
    float z_d = 0;
    int nb_petites_eponges = 6;
    vec3 couleurs_eponges[nb_petites_eponges] = {
        BLEU_CIEL, ROUGE, TURQUOISE, ROSE, JAUNE, GOLD_ALBEDO
    };
    //petites éponges tout autour
    for(int i = 0; i < nb_petites_eponges; i++){
        float x,z;
        //angle de rotation sur l'axe y
        float angle_rad = 2*M_PI / nb_petites_eponges;
        x = x_d * cos(angle_rad * i) + z_d * sin(angle_rad * i);
        z = -sin(angle_rad * i) * x_d + z_d * cos(angle_rad * i);
        cote = 1.2;
        /*
        liste[ind++] = new bvh_eponge_menger(
            vec3(x,cote/2, z), cote, 3,
            new lambert_mat(new uniform_texture(couleurs_eponges[i])),
            NULL
        );
        */
        liste[ind++] = new bvh_eponge_menger(
            vec3(x,cote/2, z), cote, 3,
            new lambert_mat(new uniform_texture(couleurs_eponges[i])),
            NULL
        );
    }

    // lumière
    cote = 10;
    liste[ind++] = new rect_xz(
        -cote,cote,-cote,cote,
        4,
        new lumineux_material(new uniform_texture(BLANC),1)
    );

    scene = new BVH_noeud(0, nb_objets, liste);

}


void raytracing_v1(std::ofstream *output) {

    //création de la matrice qui va stocker tous les pixels
    //note: utile pour envoyer toutes les données au gpu et ainsi éviter des temps de transfert
    vec3 *image = (vec3*)malloc(sizeof(vec3) * _largeur * _hauteur);

    for(int i = 0; i < _largeur * _hauteur; i++){
        image[i] = vec3(0,0,0);
    }

    printf("Début rendering...\n");
    //pixels décrit à partir du coin en haut à gauche donc :
    //de haut en bas
    #pragma omp parallel for collapse(2) num_threads(4)
    for(int ligne = _hauteur; ligne > 0; ligne--){
        //de gauche à droite
        //#pragma omp parallel for
        for(int colonne = 0; colonne < _largeur; colonne++){
            
            //vec3 couleur = vec3(0,0,0);
            float a,b,c;
            a = b = c = 0;

            //plusieurs rayons de rendu pour le même pixels
            //#pragma omp parallel for reduction(+:a,b,c)
            for(int r = 0; r < _nb_traces; r++){

                //définition du rayon de rendu pour chaque point de l'écran
                float u = (float(colonne)+ drand48()) / float(_largeur); //distance du rayon par rapport au bords gauche de l'écran (en %)
                float v = (float(ligne) + drand48()) / float(_hauteur); //distance du rayon par rapport au bas de l'écran (en % aussi) 
                
                //rayon de rendu
                rayon ray = cam->rayon_depuis_pourcentage(u, v);
                
                //on ajoute la valeur du pixel calculée
                //couleur = couleur + calc_couleur(ray, scene, 0);
                vec3 res = calc_couleur(ray, scene, 0);
                a += res.x();
                b += res.y();
                c += res.z();
                //printf("thread %d, ligne = %d, colonne = %d, r = %d\n", omp_get_thread_num(), ligne, colonne,r);
            }

            
            vec3 couleur = vec3(a,b,c);
            //on fait la moyenne des couleurs calculées pour ce pixel
            couleur = couleur / _nb_traces;
            //correction gamma (luminosité)
            couleur = vec3(sqrt(couleur.x()), sqrt(couleur.y()), sqrt(couleur.z()));
            if(couleur.x() < 0 || couleur.y() < 0 || couleur.z()  < 0){
                printf("ERREUR : \n");
                printf("a = %f, b = %f, c =%f \n", a, b ,c);
                printf("ligne = %d, colonne = %d\n", ligne, colonne);
                printf("couleur : %f, %f, %f\n", couleur.x(), couleur.y(), couleur.z());
            }
            //printf("%d\n",ligne * _hauteur + colonne * _largeur);
            image[(_hauteur - ligne ) * _largeur + colonne] = couleur;            
        }
    }
    printf("Fin rendering !\n");
    
    //copie de l'image finale dans un fichier ppm
    *output << "P3\n" << _largeur << " " << _hauteur << "\n255\n";
    for(int pix = 0; pix < _largeur * _hauteur; pix++){
            vec3 couleur = image[pix];
            //conversion vers [0,255]
            int rc, gc, bc; 
            rc = int(255.99 * couleur.x());
            gc = int(255.99 * couleur.y());
            bc = int(255.99 * couleur.z());
            //ajout dans le fichier ppm  
            *output << rc << " " << gc << " " << bc << "\n";
        
    }

    free(image);   
}
int main(int argc, char** argv){
    

    if(argc != 5){
        printf("usage: %s <largeur> <hauteur> <nombre de lancés par pixel> <nom_fichier_sortie>\n", argv[0]);
        return 0;
    }
    //définition de certaines couleurs
    BLANC = vec3(1.0, 1.0, 1.0);
    BLEU = vec3(0.0, 0.0, 1.0);
    ROUGE = vec3(1.0, 0.0, 0.0);
    VERT = vec3(0.0, 1.0, 0.0);
    NOIR = vec3(0,0,0);
    GRIS = vec3(0.5, 0.5, 0.5);

    TURQUOISE = vec3(float(0./255.), float(255./255.), float(188./255.));
    FUSHIA = vec3(float(255./255.), float(0./255.), float(137./255.));
    ROSE = vec3(float(255./255.), float(170./255.), float(216./255.));
    BLEU_CIEL = vec3(float(0./255.), float(213./255.), float(255./255.));
    JAUNE = vec3(1, float(247./255.), 0);
    GOLD_ALBEDO = vec3(float(255./255.), float(211./255.), float(87./255.));

    //dimensions de l'image produite
    _largeur = atoi(argv[1]);
    _hauteur = atoi(argv[2]);
    _nb_traces = atoi(argv[3]); //nb de lancés par pixel

    printf("Génération d'une image %d x %d, %d rayons par pixel, dans %s ... \n",
    _largeur, _hauteur, _nb_traces, argv[4]);

    //ouverture/création fichier image.ppm
    std::ofstream file;
    file.open(argv[4]);

    //chargement de la scène (objets et leur placement)

    //setup_scene_a();
    //setup_scene_b();
    //setup_scene_axes();
    //setup_scene_emission_lumiere();
    setup_scene_boite_cornell();
    //setup_scene_eponge_menger();
    //setup_scene_bvh_eponge_menger();
    //setup_scene_primitives();
    //setup_scene_sujet();

    //début chrono
    double time = omp_get_wtime();

    //rendu
    raytracing_v1(&file);

    //fin chrono
    time = omp_get_wtime() - time;
    
    printf ("Temps de rendu : %.2lf secondes.\n", float(time) );

    return 1;
}