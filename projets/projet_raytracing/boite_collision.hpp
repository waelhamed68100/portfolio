///@file Boite de collision : permet de calculer rapidement si un rayon intersecte ce volume.
/// note : toutesles faces sont parallèles aux axes

#ifndef BOITE_COLLISION_H
#define BOITE_COLLISION_H

/* //simplification de l'algo d'intersection grace au test des bornes
#include "objet.hpp"
#include "rayon.hpp"

#include "rect_xy.hpp"
#include "rect_yz.hpp"
#include "rect_xz.hpp"
*/

#include "vec3.hpp"
#include "rayon.hpp"

//versions rapides de max et min
inline float min_r(float a, float b) { return a < b ? a : b; }
inline float max_r(float a, float b) { return a > b ? a : b; }


class boite_collision {
public:

    vec3 coin_bas; //coin avec les coordonées minimales
    vec3 coin_haut; //coin avec les coordonnées maximales

    
    /** @brief Constructeur complet
     */ 
    boite_collision(vec3 coin_bas_, vec3 coin_haut_) {
        //printf("boite_collision : coin_bas_ : %f, %f, %f\n", coin_bas_.x(), coin_bas_.y(), coin_bas_.z());
        //printf("boite_collision : coin_haut_ : %f, %f, %f\n", coin_haut_.x(), coin_haut_.y(), coin_haut_.z());
        coin_bas = coin_bas_;
        coin_haut = coin_haut_;
    }

    //fonction de collision. Indique seulement si le rayon entre en collision avec la boite
    virtual bool collision(const rayon& ray, float dist_min, float dist_ma) const;
};

//TODO
//note : ne fonctionne pas si l'objet a été pivoté
bool boite_collision::collision(const rayon& ray, float dist_min, float dist_max) const {
    /*
    //version d'un ingénieur
    for (int a = 0; a < 3; a++) {
        float t0 = min_r((coin_bas.c[a] - ray.origine().c[a]) / ray.direction().c[a],  
                        (coin_haut.c[a] - ray.origine().c[a]) / ray.direction().c[a]);
        float t1 = max_r((coin_bas.c[a] - ray.origine().c[a]) / ray.direction().c[a],  
                        (coin_haut.c[a] - ray.origine().c[a]) / ray.direction().c[a]);
        dist_min = max_r(t0, dist_min);
        dist_max = min_r(t1, dist_max);
        if (dist_max <= dist_min)
            return false;
    }
    return true;
    */
   
    
    //ma version (plus rapide !)

    //colision faces xy (avant et arrière)
    
    //intersection possible seulement si rayon non parallèle
    if(ray.direction().z() != 0){ 
        //on calcule le paramètre t pour le point d'intersection
        float t;
        
        //face avant
        t = (coin_bas.z() - ray.origine().z()) / ray.direction().z();
        //check bonne distance
        if(dist_min < t && t < dist_max){
            //on calcule les coordonnées x et y du point dans ce plan
            float x = ray.origine().x() + t * ray.direction().x();
            float y = ray.origine().y() + t * ray.direction().y();

            //on regarde si les coordonnées sont dans les bornes du rectangle du plan
            if(coin_bas.x() <= x && x <= coin_haut.x() && coin_bas.y() <= y && y <= coin_haut.y()){ 
                return true;
            }
        }

        //face arrière
        t = (coin_haut.z() - ray.origine().z()) / ray.direction().z();
        //check bonne distance
        if(dist_min < t && t < dist_max){
            //on calcule les coordonnées x et y du point dans ce plan
            float x = ray.origine().x() + t * ray.direction().x();
            float y = ray.origine().y() + t * ray.direction().y();

            //on regarde si les coordonnées sont dans les bornes du rectangle du plan
            if(coin_bas.x() <= x && x <= coin_haut.x() && coin_bas.y() <= y && y <= coin_haut.y()){ 
                return true;
            }
        }
    }

    //colision faces yz (gauche et droite)
    
    //intersection possible seulement si rayon non parallèle
    if(ray.direction().x() != 0){ 
        //on calcule le paramètre t pour le point d'intersection
        float t;
        
        //face gauche
        t = (coin_bas.x() - ray.origine().x()) / ray.direction().x();
        //check bonne distance
        if(dist_min < t && t < dist_max){
            //on calcule les coordonnées x et y du point dans ce plan
            float y = ray.origine().y() + t * ray.direction().y();
            float z = ray.origine().z() + t * ray.direction().z();

            //on regarde si les coordonnées sont dans les bornes du rectangle du plan
            if(coin_bas.z() <= z && z <= coin_haut.z() && coin_bas.y() <= y && y <= coin_haut.y()){ 
                return true;
            }
        }

        //face droite
        t = (coin_haut.x() - ray.origine().x()) / ray.direction().x();
        //check bonne distance
        if(dist_min < t && t < dist_max){
            //on calcule les coordonnées x et y du point dans ce plan
            float y = ray.origine().y() + t * ray.direction().y();
            float z = ray.origine().z() + t * ray.direction().z();

            //on regarde si les coordonnées sont dans les bornes du rectangle du plan
            if(coin_bas.z() <= z && z <= coin_haut.z() && coin_bas.y() <= y && y <= coin_haut.y()){ 
                return true;
            }
        }
    }

    //colision faces xz (bas et haut)
    
    //intersection possible seulement si rayon non parallèle
    if(ray.direction().y() != 0){ 
        //on calcule le paramètre t pour le point d'intersection
        float t;
        
        //face bas
        t = (coin_bas.y() - ray.origine().y()) / ray.direction().y();
        //check bonne distance
        if(dist_min < t && t < dist_max){
            //on calcule les coordonnées x et y du point dans ce plan
            float x = ray.origine().x() + t * ray.direction().x();
            float z = ray.origine().z() + t * ray.direction().z();

            //on regarde si les coordonnées sont dans les bornes du rectangle du plan
            if(coin_bas.z() <= z && z <= coin_haut.z() && coin_bas.x() <= x && x <= coin_haut.x()){ 
                return true;
            }
        }

        //face haut
        t = (coin_haut.y() - ray.origine().y()) / ray.direction().y();
        //check bonne distance
        if(dist_min < t && t < dist_max){
            //on calcule les coordonnées x et y du point dans ce plan
            float x = ray.origine().x() + t * ray.direction().x();
            float z = ray.origine().z() + t * ray.direction().z();

            //on regarde si les coordonnées sont dans les bornes du rectangle du plan
            if(coin_bas.z() <= z && z <= coin_haut.z() && coin_bas.x() <= x && x <= coin_haut.x()){ 
                return true;
            }
        }
    }

    //aucune intersection
    return false;
    

}

#endif // PAVE_DROIT_H
