///@file Arbre bvh : découpe la scène en sous volumes (qui peuvent se chevaucher) pour éviter les calculs de collisions inutiles

#ifndef BVH_NOEUD_H
#define BVH_NOEUD_H

#include "objet.hpp"
#include "liste_objets.hpp"

#include "boite_collision.hpp"

#define NB_OBJETS_GROUPE 3 // nombre d'objets max par groupe de feuille

#define OFFSET_BVH 0.0 // marge entre la boite de collision et les objets

//fonctions données à qsort pour trier les objets entre eux
int comparaison_x(const void * obj_a, const void * obj_b) {
    //printf("qsort\n");
    //objet *a = &( ((objet*)obj_a)[0]);
    objet *a =  ((objet*)obj_a);  
    objet *b =  ((objet*)obj_b); 
    //tri selon le centre de l'objet
    vec3 pos_a = a->centre;

    //printf("centre a : %f, %f, %f\n", pos_a.x(), pos_a.y(), pos_a.z());
    vec3 pos_b = b->centre;
    return pos_a.x() < pos_b.x() ? -1 : 1; // l'ordre importe peu 
}
int comparaison_y(const void * obj_a, const void * obj_b) {
    objet *a =  ((objet*)obj_a);  
    objet *b =  ((objet*)obj_b); 
    //tri selon le centre de l'objet
    vec3 pos_a = a->centre;
    vec3 pos_b = b->centre;
    return pos_a.y() < pos_b.y() ? -1 : 1; // l'ordre importe peu 
}
int comparaison_z(const void * obj_a, const void * obj_b) {
    objet *a =  ((objet*)obj_a);  
    objet *b =  ((objet*)obj_b); 
    //tri selon le centre de l'objet
    vec3 pos_a = a->centre;
    vec3 pos_b = b->centre;
    return pos_a.z() < pos_b.z() ? -1 : 1; // l'ordre importe peu 
}

//note étend liste objet seulement pour pouvoir être appellé comme telle lors du rendu 
class BVH_noeud : public liste_objets {
public:

    liste_objets *feuilles; // si noeud feuille alors aucun noeuds fils mais liste d'objets    
    bool est_feuille; // vrai : pas de noeuds fils
    int profondeur; // pas obligatoire
    BVH_noeud *fg = NULL, *fd = NULL; // 2 noeuds fils
    boite_collision *c_box; //boite de collision de l'esemble des objets de ce noeud

    BVH_noeud() {}
    /** @brief Constructeur
     * @param profondeur : profondeur du noeud (servait à déterminer selon quel axe les 2 sous noeuds sont divisés)
     * @param nb_elements : nombre de sous objets restant à répartir dans le sous arbre
     * @param feuilles : liste d'objets à répartir dans le sous arbre
     */
    BVH_noeud(int profondeur_, int nb_elements, objet ** feuilles_) {

        if(profondeur_ == 0) {
            printf("Génération d'un BVH pour %d éléments.\n", nb_elements);
        }
        //printf("BVH_noeud : prof : %d, nb_elem : %d\n", profondeur_, nb_elements);
        /*
        for(int i = 0; i < nb_elements; i++){
            vec3 p = feuilles_[i]->centre;
            printf("bvh: position élément %i : %f, %f, %f\n",i, p.x(), p.y(), p.z());
        }
        */
        if(nb_elements == 0){
            printf("ERREUR BVH : liste vide reçue dans le constructeur.\n");
            exit(EXIT_FAILURE);
        }
        profondeur = profondeur_;
        //s'il n'y a pas besoin de diviser le sous groupe actuel
        if(nb_elements <= NB_OBJETS_GROUPE) {
            //on définit ce noeud comme une feuille
            est_feuille = true;
            feuilles = new liste_objets(feuilles_, nb_elements);
            //on calcule la boite de collision de ce noeud
            //on cherche les bornes de cette boite
            vec3 coin_bas, coin_haut;
            
            coin_bas = feuilles_[0]->coin_min;
            coin_haut = feuilles_[0]->coin_max;
            
            for(int i = 1; i < nb_elements; i++){
                //on récupère les valeurs minimales des boites de collision de chacun des objets
                vec3 c_b = feuilles_[i]->coin_min;  
                vec3 c_h = feuilles_[i]->coin_max;

                if(c_b.x() < coin_bas.x()) { coin_bas.c[0] = c_b.x(); }  
                if(c_b.y() < coin_bas.y()) { coin_bas.c[1] = c_b.y(); }  
                if(c_b.z() < coin_bas.z()) { coin_bas.c[2] = c_b.z(); }  

                if(c_h.x() > coin_haut.x()) { coin_haut.c[0] = c_h.x(); }  
                if(c_h.y() > coin_haut.y()) { coin_haut.c[1] = c_h.y(); }  
                if(c_h.z() > coin_haut.z()) { coin_haut.c[2] = c_h.z(); }  
            }
            //ajout des marges
            coin_bas = vec3(coin_bas.x()-OFFSET_BVH, coin_bas.y()-OFFSET_BVH, coin_bas.z()-OFFSET_BVH);
            coin_haut = vec3(coin_haut.x()+OFFSET_BVH, coin_haut.y()+OFFSET_BVH, coin_haut.z()+OFFSET_BVH);
            //création de la boite de collision
            
            c_box = new boite_collision(coin_bas, coin_haut);
        }
        //sinon on créer les 2 noeuds fils
        else {
            est_feuille = false;

            //note alternance du type de découpage pour avoir une répartition plus homogène
            
            //on choisi l'axe qui sera utilisée pour découper la liste en 2
            //int axe = profondeur % 3; 

            int axe = drand48(); // axe choisi aléatoirement
            if(axe < 0.33){ axe = 0; }
            else if(axe < 0.66){ axe = 1; }
            else axe = 2; 
            
            if(axe == 0){ //découpage selon x
                //on tri les objets selon la position de leurs centres
                qsort(feuilles_, nb_elements, sizeof(objet*), comparaison_x);
            } else if(axe == 1){ //découpage selon y
                //on tri les objets selon la position de leurs centres
                qsort(feuilles_, nb_elements, sizeof(objet*), comparaison_x);
            }
            else {
                //on tri les objets selon la position de leurs centres
                qsort(feuilles_, nb_elements, sizeof(objet*), comparaison_y);
            }
            //on créer les 2 fils
            fg = new BVH_noeud(profondeur + 1, nb_elements/2, feuilles_ );
            fd = new BVH_noeud(profondeur + 1, nb_elements - nb_elements/2 , &(feuilles_[nb_elements/2]) );
            //on créer la boite de collision de ce noeud
            vec3 cb_fg = fg->c_box->coin_bas; //coin bas du fg
            vec3 ch_fg = fg->c_box->coin_haut; // coin haut du fg
            vec3 cb_fd = fd->c_box->coin_bas; //coin bas du fd

            vec3 ch_fd = fd->c_box->coin_haut; // coin haut du fd
            //coin bas de la boite de collision de ce noeud : min des coins bas des 2 noeuds fils

            vec3 cb = vec3( 
                min_r(cb_fg.x(), cb_fd.x()),
                min_r(cb_fg.y(), cb_fd.y()),
                min_r(cb_fg.z(), cb_fd.z())
            );
            //coin haut de la boite de collision de ce noeud : min des coins bas des 2 noeuds fils
            vec3 ch = vec3( 
                max_r(ch_fg.x(), ch_fd.x()),
                max_r(ch_fg.y(), ch_fd.y()),
                max_r(ch_fg.z(), ch_fd.z())
            );
            
            c_box = new boite_collision(cb, ch);
        }
    }

    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool BVH_noeud::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //printf("ici a\n");
    //on regarde si le rayon touche le volume de ce noeud
    if(c_box->collision(ray, dist_min, dist_max)) {

        //si on est une feuille : calcul de la collision avec un des objets
        if(est_feuille){
            //printf("optimisation bvh : calcul de collision entre %d objets.\n", feuilles->nb_elem);
            return feuilles->collision(ray, dist_min, dist_max, inter_data);
        } 
        //sinon calcul des collisions avec les 2 noeuds fils
        else {
            intersec_data id_fg, id_fd;
            bool collision_fg = fg->collision(ray, dist_min, dist_max, id_fg);
            bool collision_fd = fd->collision(ray, dist_min, dist_max, id_fd);
            //on définit les données d'intersection selon quels fils ont été touché
            if(collision_fg && collision_fd) { //les deux
                //intersection avec le fils le plus proche qui compte
                if(id_fg.dist < id_fd.dist) { inter_data = id_fg; }
                else { inter_data = id_fd; }
                return true;
            }
            else if(collision_fg) { // seulement fils gauche
                inter_data = id_fg;
                return true;
            }
            else if(collision_fd) { //seulement fils droit
                inter_data = id_fd;
                return true;
            }
            else { return false; } // rien n'a été touché
        }
    }
    else {
        return false;
    }

}

#endif // BVH_NOEUD_H