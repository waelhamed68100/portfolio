//@file Lambertian mat = Source lumineuse orthotrope = lumière diffuse aléatoire (surfaces mattes)

#ifndef LAMBERT_MAT_H
#define LAMBERT_MAT_H

#include "material.hpp"
#include "texture.hpp"

class lambert_mat : public material {
public : 

    texture* albedo; //albedo = couleur de l'objet (sa façon d'absorber la lumière)
    
    //constructeur
    lambert_mat(texture* couleur) { albedo = couleur; }

    //voir material::calcul_impacte pour plus de détails
    virtual bool calcul_impacte(
        const rayon& rayon_entrant, const intersec_data& inter_data, 
        vec3& attenuation, rayon& rayon_sortant) const {

            //direction du rayon après rebondissement:
            // direction légèrement aléatoire par rapport à la normale de 
            // l'objet au point d'intersection
            vec3 nouvelle_direction = 
                inter_data.position + inter_data.normale + vect_alea_dans_sphere_unitaire();
            // rayon après rebondissement :
            // a pour origine le point d'intersection
            // et a pour direction la nouvelle direction
            rayon_sortant = rayon(inter_data.position, nouvelle_direction - inter_data.position);
            //on atténue la luminosité selon la couleur du material
            attenuation = albedo->valeur(inter_data);
            return true;
    }
};

#endif
