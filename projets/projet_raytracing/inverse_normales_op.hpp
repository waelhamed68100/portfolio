///@file Opération : inverse les normales d'un objet
#ifndef INVERSE_NORMALES_OP_H
#define INVERSE_NORMALES_OP_H

#include "objet.hpp"

class inverse_normales_op: public objet {
public: 

    //l'objet dont il faut inverser les normales lors de l'intersection
    objet * obj; 

    //constructeur 
    inverse_normales_op(objet* obj_) { obj = obj_; }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool inverse_normales_op::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    if(obj->collision(ray, dist_min, dist_max, inter_data)){
        inter_data.normale = -inter_data.normale;
        return true;
    }

    return false;
};

#endif //INVERSE_NORMALES_OP_H
