///@file Objet de la scène : un cone fini orienté sur l'axe Y

#ifndef CONE_Y_H
#define CONE_Y_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"


class cone_y: public objet {
public:
    vec3 S; // sommet du cone
    float y0, y1; // bornes du cone sur l'axe Y
    float angle; // angle entre axe vertical et base
    vec3 V; //vecteur unitaire de la direction de la pointe vers le centre du disque
    float r; //rayon du disque de la base
    float hauteur; // hauteur du cone = distance entre le sommet et la base

    /** @brief Constructeur complet
     * @param centre_ Centre du cone
     * @param hauteur la hauteur : distance entre la base et le sommet
     * @param r_ rayon du disque à la base du cone
     * @param mat_ Material de la surface du cube
     */ 
    cone_y(vec3 centre_, float hauteur_, float r_, material* m) {
        
        if(angle < 0) {
            printf("ERREUR constructeur cylindre : rayon négatif.\n");
            exit(EXIT_FAILURE);
        }
        if(hauteur < 0) {
            printf("ERREUR constructeur cylindre : hauteur négative.\n");
            exit(EXIT_FAILURE);
        }
        centre = centre_;
        hauteur = hauteur_;
        y0 = centre_.y() - hauteur / 2;
        y1 = centre_.y() + hauteur / 2;

        S = centre;
        S.c[1] += hauteur / 2;
        //angle = angle_ * (M_PI/180.0); // conversion degrés -> radians
        angle = atanf(r_/hauteur);
        printf("hauteur sommet cylindre : %f\n", S.y());
        mat = m;

        V = vec3(0, -1, 0);
        r = r_;

        //calcul bornes boite collision
        vec3 c = centre;
        float lp = r; // largeur = profondeur = rayon (depuis centre bounding box)
        float h = hauteur / 2; // hauteur
        coin_min = vec3( c.x() - lp, c.y() - h, c.z() - lp);  
        coin_max = vec3( c.x() + lp, c.y() + h, c.z() + lp);  

    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool cone_y::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //translation du rayon car calcul avec le cylindre infini centré sur Y
    rayon t_ray = rayon(ray.origine() - centre, ray.direction());
    //printf("début\n");
    //pour raccourcir les lignes de code :
    vec3 O = t_ray.origine();
    vec3 D = t_ray.direction();

    float o_x = t_ray.origine().x();
    float o_y = t_ray.origine().y();
    float o_z = t_ray.origine().z();

    float d_x = t_ray.direction().x();    
    float d_y = t_ray.direction().y();
    float d_z = t_ray.direction().z();

    //test d'intersection avec la base 
    if(o_y < y0) { // possible seulement si origine du rayon sous le cone
        //on calcule le paramètre t pour le point d'intersection avec le plan du disque
        float t = (y0 - o_y) / d_y;
        
        //si la distance est dans les bornes acceptables 
        if( dist_min < t || t < dist_max){
            //on calcule les coordonnées x et y du point dans ce plan
            float x = o_x + t * d_x;
            float z = o_z + t * d_z; 
            //on regarde si c'est dans le disque
            if(x*x + z*z < r*r){
                vec3 it = t_ray.lancer(t); //point d'intersection
                
                inter_data.dist = t;
                inter_data.normale = V;
                inter_data.position = it + centre;
                inter_data.ptr_material = mat;
                return true;
            }
        }
    }

    //vecteur distance entre origine du rayon et sommet du cone
    vec3 SO = O - S; 
    //calcul du déterminant : voir note.txt pour le détail de la formule
    float a,b,c; // valeurs de l'eq du disque : t^2 * a + t * b + c = 0
    //calcul paramètres de l'equation

    /*
    a = prod_scal(D, V)*prod_scal(D, V) - cos(angle)*cos(angle);
    b = 2 * (prod_scal(D,V) * prod_scal(SO, V) - prod_scal(D, SO) * cos(angle)*cos(angle));
    c = prod_scal(SO, V)*prod_scal(SO, V) - prod_scal(SO, SO) * cos(angle)*cos(angle);
    */

    // autre formule d'intersection quadratique
    float k = r / hauteur; 
    float yk = o_y - S.y();

    a = d_x*d_x + d_z*d_z - k*k * d_y*d_y;
    b = 2 * (d_x * o_x + d_z * o_z - k*k * yk * d_y);
    c = o_x*o_x + o_z*o_z - k*k * yk*yk;

    //calcul discriminant
    float delta = b*b - 4 * a * c;
    //calcul valeurs de t pour les intersections 
    if(delta <= 0) { // pas d'intersection
        //printf("delta < 0\n");
        return false;
    }
    float t0, t1;
    float sqrtd = sqrt(delta); //opti
    t0 = (-b - sqrtd ) / (2*a);
    t1 = (-b + sqrtd ) / (2*a);
    //test limite distance rayon
    if (t0>t1) {float tmp = t0; t0=t1; t1=tmp;}
    //if(t0 < dist_min || t0 > dist_max) return false;
    //on calcule les points d'intersections avec le cylindre infini
    vec3 i0, i1;
    i0 = t_ray.lancer(t0);
    //on verifie que c'est la bonné moitiée de cone
    if(i0.y() > S.y()) { return false; }
    if(i0.y() < y0) {return false; }
    //calcul normale
    /* //version a
    vec3 normale = i0 - centre;
    normale.c[1] = normale.norme() * r / hauteur;
    normale = vect_unit(normale);
    */
    /* // veriosn b
    vec3 ce = centre;
    vec3 nn = vec3(i0.x()-ce.x(),0, i0.z()-ce.z());
    float m = nn.norme();
    nn = nn / m;

    float hr = (hauteur/r);
    vec3 normale = vec3(nn.x() * hr, r/hauteur, nn.z() * hr) ;
    normale = vect_unit(normale);
    */
    //verison c
    vec3 cp = i0 - S;
    vec3 normale = prod_scal(V, cp) / prod_scal(cp, cp) * cp - V;
    normale = vect_unit(normale);
    
    inter_data.dist = t0;
    inter_data.normale = normale;
    inter_data.position = i0 + centre;
    inter_data.ptr_material = mat;

    return true;

}

#endif // CONE_Y_H

