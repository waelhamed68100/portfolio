///@file objet de la scène, définit :
// * la forme de l'objet (collision() : quand est-ce que le rayon touche l'objet ?) 
// * comment il affecte u rayon de rendu qui le touche (rebondissement) : material 

#ifndef OBJET_H
#define OBJET_H

#include "rayon.hpp"
#include "intersec_data.hpp"



class objet {
public:
    vec3 centre; //centre de l'objet (utilisé pour la répartition des objets dans le bvh)
    material *mat; //material de la surface de l'objet
    vec3 coin_min, coin_max; //coin des bornes utilisées pour le cacul des bounding box
    
    /** @brief Fonction de calcul de la collision entre le rayon de rendu et un objet.
     * @param ray Rayon de rendu
     * @param dist_min Distance minimale de l'origine du rayon au point d'interestion pour que la collision soit acceptée
     * @param dist_max Distance maximale de l'origine du rayon au point d'interestion pour que la collision soit acceptée
     * @param inter_data Données en lien avec l'intersection
     * @note const : pas de modification de l'objet
     * @note fct = 0 : fonction purement virtuelle : les sous-classes doivent l'implémenter
     */
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const = 0;
};

#endif //OBJET_H


