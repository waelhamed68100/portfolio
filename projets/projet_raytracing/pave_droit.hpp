///@file Objet de la scène : un pavé droit (toutes les faces sont parallèles à une autre et les faces sont parallèles à un des axes orthogonaux) 
/// note : cube aurait pu hériter de pave droit, mais cube a été implémenté en 1er, et flemme de refactorer le code de cube 
#ifndef PAVE_DROIT_H
#define PAVE_DROIT_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"

#include "rect_xy.hpp"
#include "rect_yz.hpp"
#include "rect_xz.hpp"

#include "liste_objets.hpp"

#include "inverse_normales_op.hpp"

class pave_droit: public objet {
public:
    vec3 centre; //centre du pave = position dans l'espace
    float largeur, hauteur, profondeur; 
    
    // 6 faces
    rect_xz *bas, *haut; 
    rect_yz *gauche, *droite;
    rect_xy *avant, *arriere;

    //liste des 6 faces
    liste_objets *faces;    


    /** @brief Constructeur complet
     * @param centre_ Centre du cube
     * @param cote_ longueur = hauteur = profondeur de chacune des faces
     * @param mat_ Material de la surface du cube
     */ 
    pave_droit(vec3 centre_, float largeur_, float hauteur_, float profondeur_, material* m) {
        centre = centre_;
        
        largeur = largeur_;
        hauteur = hauteur_;
        profondeur = profondeur_;

        mat = m;

        float ml = largeur / 2; //moitié de la largeur
        float mh = hauteur / 2;
        float mp = profondeur / 2;

        bas = new rect_xz(
            centre.x() - ml, centre.x() + ml,
            centre.z() - mp, centre.z() + mp,
            centre.y() - mh,
            m
        ); 

        haut = new rect_xz(
            centre.x() - ml, centre.x() + ml,
            centre.z() - mp, centre.z() + mp,
            centre.y() + mh,
            m
        ); 

        gauche = new rect_yz(
            centre.y() - mh, centre.y() + mh,
            centre.z() - mp, centre.z() + mp,
            centre.x() - ml,
            m
        ); 

        droite = new rect_yz(
            centre.y() - mh, centre.y() + mh,
            centre.z() - mp, centre.z() + mp,
            centre.x() + ml,
            m
        ); 

        arriere = new rect_xy(
            centre.x() - ml, centre.x() + ml,
            centre.y() - mh, centre.y() + mh,
            centre.z() - mp,
            m
        ); 

        avant = new rect_xy(
            centre.x() - ml, centre.x() + ml,
            centre.y() - mh, centre.y() + mh,
            centre.z() + mp,
            m
        ); 

        objet **faces_ = new objet*[6];
        
        faces_[0] = new inverse_normales_op( bas );
        faces_[1] = haut;
        faces_[2] = new inverse_normales_op( gauche);
        faces_[3] = droite;
        faces_[4] = new inverse_normales_op( arriere);
        faces_[5] = avant ;

        faces = new liste_objets(faces_, 6);

        //calcul bornes boite collision
        vec3 c = centre; 
        coin_min = vec3( c.x() - ml, c.y() - mh, c.z() - mp);  
        coin_max = vec3( c.x() + ml, c.y() + mh, c.z() + mp);

    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool pave_droit::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection
    /*
    if(bas.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(haut.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(gauche.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(droite.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(avant.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(arriere.collision(ray, dist_min, dist_max, inter_data)) { return true; }
    
    return false;     
    */
   return faces->collision(ray, dist_min, dist_max, inter_data);
}

#endif // PAVE_DROIT_H

