# Projet de ray tracing

Année : 2019.

Génération d'images par lancés aléatoires de rayons (path tracing). Technologie : uniquement c++ et ses bibliothèques de base (et accélération mutlithreads cpu par openmp, optionnelle). 
Code basé sur les cours de Peter Shirley.

Notions principales acquises : 
 * méthode de rendu realiste (pour chaque pixel, moyenne des couleurs calculées pour plusieurs rayons rebondissant aléatoirement)
 * Optimisation **BVH**
 * calculs de collision segment - objets 3d (plans, sphères, cylindre, cubes et cône)
 * principes des matériaux et textures pour le ray tracing

## Dépendances
---
 * compilateur c++
 * openmp (optionnelle, si accélération multithread cpu voulue)
    
## Exécution
---
* version simple : éxecuter le fichier `run.sh`

## Quelques images pré-rendues présentes dans le [dossier img](/projets/projet_raytracing/img) :
---

![image introuvable](./img/scene_a_2000x1000-300r_164s.png "boules colorées, texutres et matériaux (300 rayons par pixel)")

![image introuvable](./img/bvh_menger_100r_269sec.png "éponge de Menger (100 rayons par pixel)")

![image introuvable](./img/boite_cornell.png "boîte de Cornell (100 rayons par pixel)")

![image introuvable](./img/couleurs_lumiere.png "matériau lumineux (100 rayons par pixel)")

![image introuvable](./img/1000x500_bvh_menger_100r_20min.png "plusieurs éponges, scène à plusieurs milliers d'objets")

![image introuvable](./img/primitives.png "primitives (plans, sphères, cylindre, cubes et cône)")
