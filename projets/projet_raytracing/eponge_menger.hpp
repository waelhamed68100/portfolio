///@file Objet de la scène : une éponge de Menger (fractale cubes récursifs)

#ifndef EPONGE_MENGER_H
#define EPONGE_MENGER_H

#include "rayon.hpp"
#include "material.hpp"

#include "cube.hpp"
#include "sphere.hpp"
#include "metal_mat.hpp"

#include "liste_objets.hpp"


class eponge_menger: public objet {
public:
    vec3 centre; //centre de l'éponge = position dans l'espace
    float cote; //largeur d'un côté

    //liste des 27 sous élements
    liste_objets *sous_eponges;    


    /** @brief Constructeur complet
     * @param centre_ Centre de l'éponge
     * @param cote_ longueur = hauteur = profondeur de l'éponge courante
     * @param niveau Niveau de la récursion : 1 = étape finale
     * @param mat_ Material de la surface de l'éponge
     */ 
    eponge_menger(vec3 centre_, float cote_, int niveau, material* m) {
        centre = centre_;
        cote = 0.95 * cote_; //pour laisser un espace entre les cubes 
        mat = m;

        float mc = cote / 2; //moitié d'un côté
        
        objet **sous_eponges_;
        int nb_elements; //nombre de sous éléments de cette étape de ce niveau de l'éponge (1 ou 20 cubes)
        

        //si on a atteint le dernier niveau alors cet objet est un simple cube
        if(niveau == 1) {
            sous_eponges_ = new objet*[1];

            sous_eponges_[0] = new cube(
                centre, cote, m
            );

            nb_elements = 1;
        }
        //sinon cet objet est constitué de 20 autres sous cubes
        else {
            sous_eponges_ = new objet*[27];
            nb_elements = 0;
            //pour chacun des 27 sous cubes
            for(int x = -1; x < 2; x++){
                for(int y = -1; y < 2; y++){
                    for(int z = -1; z < 2; z++){
                        float tc = cote / 3.0; //chaque cote du sous cube vaut 1/3 du cube de ce niveau
                        vec3 pos = vec3(
                            centre.x() + x * tc,
                            centre.y() + y * tc,
                            centre.z() + z * tc
                        );

                        int somme = abs(x) + abs(y) + abs(z);
                        //si ce n'est pas un des 7 cubes centraux
                        if(somme > 1) {

                            //on créer le sous cube
                            /*
                            sous_eponges_[nb_elements++] = new cube(
                                pos, tc, mat
                            );
                            */
                            sous_eponges_[nb_elements++] = new eponge_menger(
                                pos, tc, niveau-1, mat
                            );
                        }
                        //on remplace les cubes centraux par des sphères réfléchissantes
                        else {
                            sous_eponges_[nb_elements++] = new sphere(
                                pos, tc/5, new metal_mat(
                                    vec3(0.5,0.5,0.5),0
                                )
                            );
                        }
                    }
                }
            }
        }

        sous_eponges = new liste_objets(sous_eponges_, nb_elements);

        //calcul bornes boite collision
        vec3 c = centre;
        float r = cote /2; 
        coin_min = vec3( c.x() - r, c.y() - r, c.z() - r);  
        coin_max = vec3( c.x() + r, c.y() + r, c.z() + r); 

    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool eponge_menger::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection
   return sous_eponges->collision(ray, dist_min, dist_max, inter_data);
}

#endif //CUBE_H

