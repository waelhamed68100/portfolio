//@file Lumineux mat = materiau qui émet de la lumière

#ifndef LUMINEUX_MATERIAL_H
#define LUMINEUX_MATERIAL_H

#include "material.hpp"
#include "texture.hpp"


class lumineux_material : public material {
public : 

    texture* couleur; //couleur(s) de la surface lumineuse
    float luminosite; // 1 := lumineux à 100% de sa couleur
    //note : une luminosité > 1 fera tendre la couleur vers le blanc (sauf pour les composantes rgb == 0)
    
    //constructeur
    lumineux_material(texture* couleur_, float luminosite_) { 
        couleur = couleur_; 
        luminosite = luminosite_; 
    }

    //voir material::calcul_impacte pour plus de détails
    virtual bool calcul_impacte(
        const rayon& rayon_entrant, const intersec_data& inter_data, 
        vec3& attenuation, rayon& rayon_sortant) const {
            //simulation simplifiée : les rayons de rendus ne rebondissent pas sur les lumières
            return false;
    }

    //voir material::emission_lumiere pour plus de détails
    virtual vec3 emission_lumiere(const intersec_data& inter_data) const {
        //on emet la couleur de la texture au point d'intersection
        vec3 c = luminosite * couleur->valeur(inter_data);
        /* ne sert à rien
        if(c.x() > 1) c.c[0] = 1;
        if(c.y() > 1) c.c[1] = 1;
        if(c.z() > 1) c.c[2] = 1;
        */
       //note : luminosite = hack, on est sensé "envoyer" plus de photons plus un objet est lumineux
        return c; 
    }
};

#endif // LUMINEUX_MATERIAL_H
