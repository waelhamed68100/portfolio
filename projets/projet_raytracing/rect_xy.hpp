///@file Rectangle d'un plan parallèle au plan xy
#ifndef RECT_XY_H
#define RECT_XY_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"

///le rectangle (morceau de plan) est un objet de la scene
class rect_xy: public objet {
public:
    
    //coordonnées du plan
    float z; //position du plan sur l'axe Z 

    float x0, x1, y0, y1; //bords du plan
    //note : pour éviter d'inverser l'orientation du plan x0 < x1 et y0 < y1

    material * mat; //material du rectangle

    //constructeur basique
    rect_xy() {};

    /** @brief Constructeur complet
     * @param x0_ bords gauche
     * @param x1_ bords droit
     * @param y0_ bords gauche
     * @param y1_ bords droit
     * @note x0 et x1 ainsi que y0 et y1 peuvent être inversés si x0 > x1 ou y0 > y1
     */ 
    rect_xy(float x0_, float x1_, float y0_, float y1_, float z_, material* mat_) {
        /*
        if(x0_ == y0_ && x1_ == y1_){
            printf("ERREUR: rect_xy : les 2 points du plans doivent être différents !\n");
            exit(EXIT_FAILURE);
        }
        */
        
        /*
        x0 = x0_; x1 = x1_;
        y0 = y0_; y1 = y1_;
        */
        
        //obligatoire pour le bounding box ?
        x0 = x0_ < x1_ ? x0_ : x1_;
        x1 = x0_ < x1_ ? x1_ : x0_;

        y0 = y0_ < y1_ ? y0_ : y1_;
        y1 = y0_ < y1_ ? y1_ : y0_;

        z = z_;

        centre = vec3((x0 + x1)/2.0, (y0 + y1)/2.0, z);
        
        float offset = 0.05; // marge coins bounding volume
        coin_min = vec3(x0 - offset, y0 - offset, z - offset);
        coin_max = vec3(x1 + offset, y1 + offset, z + offset);

        mat = mat_;
    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool rect_xy::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection entre le rayon et le plan

    //si la direction du rayon est parallèle au plan alors aucune intersection
    if(ray.direction().z() == 0){
        return false;
    }

    //on calcule le paramètre t pour le point d'intersection
    float t = (z - ray.origine().z()) / ray.direction().z();
    
    //si la distance n'est pas dans les bornes acceptables 
    if( t < dist_min || t > dist_max){
        return false;
    }

    //on calcule les coordonnées x et y du point dans ce plan
    float x = ray.origine().x() + t * ray.direction().x();
    float y = ray.origine().y() + t * ray.direction().y();

    //on regarde si les coordonnées sont dans les bornes du rectangle du plan
    if(x0 <= x && x <= x1 && y0 <= y && y <= y1){
        inter_data.dist = t;
        inter_data.position = vec3(x,y,z);
        inter_data.normale = vec3(0,0,1);
        inter_data.ptr_material = mat;
        return true;
    }   
    else {
        return false;
    }
   
}

#endif //RECT_XY_H

