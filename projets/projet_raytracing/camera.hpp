///@file camera représentant le point de vue
#ifndef CAMERA_H
#define CAMERA_H

#include "vec3.hpp"
#include "rayon.hpp"

class camera {
public:
    //position de la caméra
    vec3 origine;   
    //direction et angle de vue de l'écran
    vec3 coin_bas_gauche; //position du coin bas gauche de l'écran
    vec3 delta_horizontal; //distance entre les bords horizontaux (haut et bas) de l'écran
    vec3 delta_vertical; //distance entre les bords verticaux (gauche et droite) de l'écran

    /** @brief constructeur
     * @pram fov_horizontal champs de vision horizontal en degrés
     * @param ratio Ratio largeur/hauteur de l'écran
     * @param position La position de la caméra dans l'espace
     * @param cible La position de ce vers quoi la caméra est dirigée (point central du champs de vision)
     * @param dir_haut La direction diquant la rotation de la caméra sur elle même
     */
    camera(float fov_horizontal, float ratio, vec3 position, vec3 cible, vec3 dir_haut) {
        //bug : lorsque les 3 composante de cible valent pile 0, 
        //le ray tracing risque de calculer la mauvaise couleur des pixels dans des cas très particuliers d'orthogonalité
        //piste : produit vectoriel donnant 0 ?
        //TODO: trouver la cause de ce bug. Dirty fix en attendant
        //note : trouvé : causé par des erreurs dans plan_**.hpp
        if(cible.x() == 0 && cible.y() == 0 && cible.z() == 0) {
            cible.c[2] = 0.00000001;
        }

        float radian = fov_horizontal * M_PI / 180.0; //conversion dégrés -> radians
        float moitie_largeur = tan(radian/2.0); // moitie de la hauteur de l'écran
        float moitie_hauteur = moitie_largeur / ratio; //moitie de la largeur de l'écran

        origine = position; //position dans l'espace de la caméra 

        //repère de la caméra
        vec3 w = vect_unit( position - cible); 
        vec3 u = vect_unit( prod_vect( dir_haut, w) );
        vec3 v = vect_unit( prod_vect( w, u) );

        //position du coin bas gauche de l'écran
        coin_bas_gauche = origine - moitie_largeur * u - moitie_hauteur * v - w; // écran placé à z = -1 
        delta_horizontal = 2 * moitie_largeur * u; //distance entre les bords horizontaux (haut et bas) de l'écran
        delta_vertical = 2 * moitie_hauteur * v; //distance entre les bords verticaux (gauche et droite) de l'écran
    }

    /** @brief Renvoi le rayon depuis les coordonnées du pixel en pourcentage
     * @param u Distance en pourcentage depuis le bords gauche de l'écran
     * @param v Distance en pourcentage depuis le bords bas de l'écran
     */ 
    rayon rayon_depuis_pourcentage( float u, float v) {
        return rayon(
            origine,
            vect_unit(coin_bas_gauche + u * delta_horizontal + v * delta_vertical - origine)
        );
    }

};

#endif //CAMERA_H
