///@file Objet de la scène : un cylindre fini orienté sur l'axe Y

#ifndef CYLINDRE_H
#define CYLINDRE_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"


class cylindre: public objet {
public:
    float r; // rayon du disque de la base du cylindre 
    float y0, y1; // bores du cylindre sur l'axe Y
    
    /** @brief Constructeur complet
     * @param centre_ Centre du cube
     * @param r_ rayon de la base
     * @param hauteur la hauteur : distance entre les 2 disques
     * @param mat_ Material de la surface du cube
     */ 
    cylindre(vec3 centre_, float r_, float hauteur, material* m) {
        
        if(r < 0) {
            printf("ERREUR constructeur cylindre : rayon négatif.\n");
            exit(EXIT_FAILURE);
        }
        if(hauteur < 0) {
            printf("ERREUR constructeur cylindre : hauteur négative.\n");
            exit(EXIT_FAILURE);
        }
        centre = centre_;
        r = r_;
        y0 = centre_.y() - hauteur / 2;
        y1 = centre_.y() + hauteur / 2;
        
        mat = m;

        //calcul bornes boite collision
        vec3 c = centre;
        float lp = r; // largeur = profondeur = rayon (depuis centre bounding box)
        float h = hauteur / 2; // hauteur
        coin_min = vec3( c.x() - lp, c.y() - h, c.z() - lp);  
        coin_max = vec3( c.x() + lp, c.y() + h, c.z() + lp);  
    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool cylindre::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //translation du rayon car calcul avec le cylindre infini centré sur Y
    rayon t_ray = rayon(ray.origine() - centre, ray.direction());
    //printf("début\n");
    //pour raccourcir les lignes de code :
    float o_x = t_ray.origine().x();
    float o_y = t_ray.origine().y();
    float o_z = t_ray.origine().z();

    float d_x = t_ray.direction().x();    
    float d_y = t_ray.direction().y();
    float d_z = t_ray.direction().z();
    //opti
    float r2 = r*r;

    //cas où l'origine du rayon est dans le cylindre infini
    // A.x^2 + A.z^2 < R^2
    float eq_disque = o_x * o_x + o_z * o_z;
    if(eq_disque < r2 && d_y != 0.0 ){ 
        float Y = FLT_MAX; // composante y du disque dont il faut vérifier l'intersection
        //cas sous le cylindre
        if(o_y < y0) { Y = y0; }
        else if ( o_y > y1) { Y = y1; }
        //si rayon à l'intérieur du cylindre
        if(Y == FLT_MAX) { return false; }
        //on calcul la valeur de t pour le point d'intersection
        float t = (Y - o_y) / d_y;
        //on vérifie que la distance du rayon à son origine est accepté
        if(t < dist_min || t > dist_max) { return false; }
        //calcul du point d'intersection avec le plan du disque
        vec3 it = t_ray.lancer(t);
        //on vérifie que l'intersection est dans le disque
        eq_disque = it.x() * it.x() + it.z() * it.z();
        if(eq_disque < r2) { 
            inter_data.dist = t;
            inter_data.normale = Y == y0 ? vec3(0,-1,0) : vec3(0,1,0);
            inter_data.position = it + centre;
            inter_data.ptr_material = mat;
            return true;
        }
        else { return false; }
    }

    //calcul du déterminant : voir note.txt pour le détail de la formule
    float a,b,c; // valeurs de l'eq du disque : t^2 * a + t * b + c = 0
    //calcul paramètres de l'equation 
    a = (d_x * d_x) + (d_z * d_z);
    b = 2 * (o_x * d_x + o_z * d_z);
    c = (o_x * o_x + o_z * o_z - r2 );
    //calcul discriminant
    float delta = b*b - 4 * a * c;
    //calcul valeurs de t pour les intersections 
    if(delta < 0) { // pas d'intersection
        //printf("delta < 0\n");
        return false;
    }
    float t0, t1;
    float sqrtd = sqrt(delta); //opti
    t0 = (-b - sqrtd ) / (2*a);
    t1 = (-b + sqrtd ) / (2*a);
    //test limite distance rayon
    if (t0>t1) {float tmp = t0;t0=t1;t1=tmp;}
    if(t0 < dist_min || t0 > dist_max) return false;
    //on calcule les points d'intersections avec le cylindre infini
    vec3 i0, i1;
    i0 = t_ray.lancer(t0);
    i1 = t_ray.lancer(t1);
    //printf("y0 : %f, y1 : %f\n", y0,y1);
    
    //on compare les valeurs de y des points d'intersection
    if(y0 <= i0.y() && i0.y() <= y1) { //rayon touche côté cylindre
        //printf("inter côté\n");
        inter_data.dist = t0;
        //vec3 normale = i0 - centre;
        //normale.c[1] = 0; //vecteur parallèle au plan xz
        vec3 normale = vec3(i0.x(), 0, i0.z());
        //normale = vect_unit (normale); //normalisé
        inter_data.normale = normale;
        inter_data.position = i0 + centre;
        inter_data.ptr_material = mat;
        return true;

    }
    if(i0.y() <= y0 && i1.y() >= y0) { //rayon touche bas cylindre
        //printf("inter bas\n");
        //on calcule la position du point d'intersection avec le disque
        float t3 = (y0 - o_y) / d_y;
        vec3 i3 = t_ray.lancer(t3);
        inter_data.position = i3 + centre;
        inter_data.dist = t3;
        inter_data.normale = vec3(0, 1, 0);
        inter_data.ptr_material = mat;
        return true;
    }
    if(i0.y() >= y1 && i1.y() <= y1) { //rayon touche haut cylindre
        //printf("inter haut\n");
        //on calcule la position du point d'intersection avec le disque
        float t3 = (y1 - o_y) / d_y;
        vec3 i3 = t_ray.lancer(t3);
        inter_data.position = i3 + centre;
        inter_data.dist = t3;
        inter_data.normale = vec3(0, 1, 0);
        inter_data.ptr_material = mat;
        return true;
    }
    
    //printf("hors limite\n");
    return false;

}

#endif // CYLINDRE_H

