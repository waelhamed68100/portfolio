echo " ------ cleaning ------"
rm raytracing.out

rm output.ppm

echo " ------ compil ------"
g++ -fopenmp -O3 main.cpp -o raytracing.out -g -lm

echo " ------ exec ------"
#./raytracing.out 2000 1000 1 output.ppm
./raytracing.out 1000 500 50 output.ppm

echo " ------ ouverture image ------ "
xdg-open output.ppm
