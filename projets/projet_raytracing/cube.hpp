///@file Objet de la scène : un cube

#ifndef CUBE_H
#define CUBE_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"

#include "rect_xy.hpp"
#include "rect_yz.hpp"
#include "rect_xz.hpp"

#include "liste_objets.hpp"

#include "inverse_normales_op.hpp"

class cube: public objet {
public:
    float cote; 
    
    // 6 faces
    rect_xz *bas, *haut; 
    rect_yz *gauche, *droite;
    rect_xy *avant, *arriere;

    //liste des 6 faces
    liste_objets *faces;    


    /** @brief Constructeur complet
     * @param centre_ Centre du cube
     * @param cote_ longueur = hauteur = profondeur de chacune des faces
     * @param mat_ Material de la surface du cube
     */ 
    cube(vec3 centre_, float cote_, material* m) {
        centre = centre_;
        cote = cote_; 
        mat = m;

        float mc = cote / 2; //moitié d'un côté
        
        bas = new rect_xz(
            centre.x() - mc, centre.x() + mc,
            centre.z() - mc, centre.z() + mc,
            centre.y() - mc,
            m
        ); 

        haut = new rect_xz(
            centre.x() - mc, centre.x() + mc,
            centre.z() - mc, centre.z() + mc,
            centre.y() + mc,
            m
        ); 

        gauche = new rect_yz(
            centre.y() - mc, centre.y() + mc,
            centre.z() - mc, centre.z() + mc,
            centre.x() - mc,
            m
        ); 

        droite = new rect_yz(
            centre.y() - mc, centre.y() + mc,
            centre.z() - mc, centre.z() + mc,
            centre.x() + mc,
            m
        ); 

        arriere = new rect_xy(
            centre.x() - mc, centre.x() + mc,
            centre.y() - mc, centre.y() + mc,
            centre.z() - mc,
            m
        ); 

        avant = new rect_xy(
            centre.x() - mc, centre.x() + mc,
            centre.y() - mc, centre.y() + mc,
            centre.z() + mc,
            m
        ); 

        objet **faces_ = new objet*[6];
        
        faces_[0] = new inverse_normales_op( bas );
        faces_[1] = haut;
        faces_[2] = new inverse_normales_op( gauche);
        faces_[3] = droite;
        faces_[4] = new inverse_normales_op( arriere);
        faces_[5] = avant ;

        faces = new liste_objets(faces_, 6);

        //calcul bornes boite collision
        vec3 c = centre;
        float r = cote /2; 
        coin_min = vec3( c.x() - r, c.y() - r, c.z() - r);  
        coin_max = vec3( c.x() + r, c.y() + r, c.z() + r);  


    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool cube::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection
    /*
    if(bas.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(haut.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(gauche.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(droite.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(avant.collision(ray, dist_min, dist_max, inter_data)) { return true; }     
    if(arriere.collision(ray, dist_min, dist_max, inter_data)) { return true; }
    
    return false;     
    */
   return faces->collision(ray, dist_min, dist_max, inter_data);
}

#endif //CUBE_H

