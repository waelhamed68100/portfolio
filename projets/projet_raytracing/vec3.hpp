///@file Définition et opérations sur vecteurs de R^3

#ifndef VEC3_H
#define VEC3_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h> //drand48

class vec3 {

public: 
    //composantes enregistrées dans un tableau
    float c[3];
    //constructeur basique
    vec3() { c[0] = c[1] = c[2] = 0; }
    //constructeur depuis les composantes
    vec3(float x, float y, float z) { c[0] = x; c[1] = y; c[2] = z; }
    
    //inline = optimisation 
    inline float x() const { return c[0]; }
    inline float y() const { return c[1]; }
    inline float z() const { return c[2]; }

    //opérations unaires
    inline vec3 operator-() const { return vec3(-c[0], -c[1], -c[2]); }

    //norme du vecteur
    inline float norme() const { return sqrt(c[0]*c[0] + c[1]*c[1] + c[2]*c[2]); }

    //racine de la norme d'un vecteur
    //note : utilisé pour check si un vecteur a une norme < 1
    inline float racine_norme() const { return c[0]*c[0] + c[1]*c[1] + c[2]*c[2]; }

    inline void print(const char* s) const { printf("%s: %f, %f, %f.\n",s, c[0], c[1], c[2]); }

};

//addition entre 2 vecteurs
inline vec3 operator+(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.c[0] + v2.c[0], v1.c[1] + v2.c[1], v1.c[2] + v2.c[2]);
}

//multiplication avec un scalaire
inline vec3 operator*(float t, const vec3 &v) {
    return vec3(t*v.c[0], t*v.c[1], t*v.c[2]);
}

//division avec un scalaire
inline vec3 operator/(vec3 v, float t) {
    return vec3(v.c[0]/t, v.c[1]/t, v.c[2]/t);
}

//soustraction entre 2 vecteurs
inline vec3 operator-(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.c[0] - v2.c[0], v1.c[1] - v2.c[1], v1.c[2] - v2.c[2]);
}

//produit scalaire entre 2 vecteurs
inline float prod_scal(const vec3 &v1, const vec3 &v2) {
    return v1.c[0] *v2.c[0] + v1.c[1] *v2.c[1]  + v1.c[2] *v2.c[2];
}

//vecteur directeur normalisé
inline vec3 vect_unit(vec3 v) {
    return v / v.norme();
}

//multipliation entre 2 veteurs
// ATTENTION, différent du produit scalaire / vectoriel
inline vec3 operator*(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.c[0] * v2.c[0], v1.c[1] * v2.c[1], v1.c[2] * v2.c[2]);
}

/** @brief Renvoi un vecteur dans la sphère unitaire (vecteur aléatoire de norme <= 1)
 */
inline vec3 vect_alea_dans_sphere_unitaire(){
    vec3 v;
    do {
        //valeur de chaque composante aléatoirement entre 0 et 2
        v = 2.0 * vec3(drand48(), drand48(), drand48()) - vec3(1,1,1);
    } while( v.racine_norme() >= 1 ); // tant qu'on est pas dans la sphère unitaire
    //note: on utilise la racine de la norme car plus rapide à calculer

    return v;
}

/** @brief Produit vectoriel entre 2 vecteurs
 */
inline vec3 prod_vect(const vec3 &v1, const vec3 &v2) {
    return vec3( (v1.c[1]*v2.c[2] - v1.c[2]*v2.c[1]),
                (-(v1.c[0]*v2.c[2] - v1.c[2]*v2.c[0])),
                (v1.c[0]*v2.c[1] - v1.c[1]*v2.c[0]));
}

#endif //VEC3_H
