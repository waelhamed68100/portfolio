///@file Opération : applique une rotation sur l'axe Y à un objet (n'affecte que le calcul du rendu !)

#ifndef ROTATION_Y_OP_H
#define ROTATION_Y_OP_H

#include "objet.hpp"

class rotation_y_op: public objet {
public: 

    //l'objet qu'il faut tourner
    objet * obj; 
    // l'angle en radians dans le sens courant
    float angle_rad;
    //paramètres constant de l'opération de rotation 
    float cos_a, sin_a; //cos(angle_rad) et sin(angle_rad)

    /** @brief constructeur
     * @param obj_ L'objet dont il faut appliquer une rotation
     * @param angle_deg L'angle en degrés dans le sens courant
     */
    rotation_y_op(objet* obj_, float angle_deg) { 
        obj = obj_; 
        angle_rad = angle_deg * (M_PI/180.0); // conversion degrés -> radians
        cos_a = cos(angle_rad);
        sin_a = sin(angle_rad);
        
        //TODO: recalculer la bounding box après rotation de l'objet
        coin_min = obj->coin_min;
        coin_max = obj->coin_max;
    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool rotation_y_op::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //note: formules optimisées pour éviter d'avoir à calculer la rotation invers cos(-T) et sin(-T).
    //voir notes.txt.

    //on fait pivoter le rayon de rendu dans le sens inverse de la rotation de l'objet
    //copie du rayon
    vec3 o = ray.origine(); 
    vec3 d = ray.direction();
    //calcul de la rotation de l'origine
    o.c[0] = ray.origine().x() * cos_a + ray.origine().z() * -sin_a; // calcul x'
    o.c[2] = ray.origine().x() * sin_a + ray.origine().z() * cos_a; // calcul z'
    //calcul de la rotation de la direction
    d.c[0] = ray.direction().x() * cos_a + ray.direction().z() * -sin_a; // calcul x'
    d.c[2] = ray.direction().x() * sin_a + ray.direction().z() * cos_a; // calcul z'
    rayon rotation_ray = rayon(o,d);

    //test d'intersection entre l'objet et le rayon pivoté
    if(obj->collision(rotation_ray, dist_min, dist_max, inter_data)){
        //on pivote le point d'intersection et la normale dans le sens de la rotation de l'objet
        
        //rotation point d'intersection
        vec3 p = inter_data.position; //copie des données
        p.c[0] = inter_data.position.x() * cos_a + inter_data.position.z() * sin_a;
        p.c[2] = inter_data.position.x() * -sin_a + inter_data.position.z() * cos_a;
        //rotation normale au point d'intersection
        vec3 n = inter_data.normale; //copie des données
        n.c[0] = inter_data.normale.x() * cos_a + inter_data.normale.z() * sin_a;
        n.c[2] = inter_data.normale.x() * -sin_a + inter_data.normale.z() * cos_a;
        //enregistrement
        inter_data.position = p;
        inter_data.normale = n;
        return true;
    }
    else {
        return false;
    }
};

#endif //ROTATION_Y_OP_H
