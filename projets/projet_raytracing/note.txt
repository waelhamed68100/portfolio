ressources d'apprentissage : https://devblogs.nvidia.com/accelerated-ray-tracing-cuda/ 

1) intersection d'une sphère avec le rayon lancé :

Soient une sphère S de rayon r et de centre c ( cx, cy, cz).

et un rayon R de rendu défini par p(t) = A + t * B.

soit p un point (x,y,z).
l'équation du cercle est : 
    ( x - cx )^2 + ( y - cy )^2 + ( z - cz)^2 = r^2
<-> prod_scal( ( p - c ), ( p - c) ) = r^2

C et R en intersection 
<-> prod_scal( (p(t) - c), (p(t) - c) ) = r^2
<-> prod_scal( (A + t * B - c), (A + t * B - c) ) = r^2
<-> t² * prod_scal(B,B) + 2 * t * prod_scal(B, A-c) + prod_scal(A-c, A-c) - r² = 0.    (eq1)

en calculant le discrimant d de eq1 on peut savoir si le lancer de rayon intersecte le cercle C :
    - d < 0 : pas d'intersection
    - d = 0 : une intersection (rayon frole le cercle)
    - d > 0 : 2 intersections (rayon transperse le cercle)

2) intersection entre un plan P parallèle au plan xy, positionné pour z = k, et un rayon R :

R défini par p(t) = A + t * B;

on cherche à résoudre :
p(t).z = A.z + t * B.z = k
<-> t = ( k - A.z ) / B.z

Attention, si B.z = 0 alors l'équatio n'a pas de sens puisque le rayon est parallèle au plan p

On calcule t, puis à partir de t on détermine p(t).x et p(t).y

Puis on compare les valeurs de p(t).x et p(t).y avec les bornes du plan P :
p(t) appartient à P ssi
    x0 <= p(t).x <= x1
    et y0 <= p(t).y <= y1
 
3) Rotation d'un point sur l'axe Y, d'angle T  :

x' = x * cos(T) + z * sin(T);
z' = -sin(T) * x + z * cos(T);

optimisations : 
    cos(T) = cos(-T)
    sin(T) = -sin(-T)

4) Equation d'un cylindre das l'axe Y : 
    -> on se place dans un repère 2D en ignorant y :
        -> equation du disque D de centre I (Ix, Iz) et de rayon R :  
                (x - Ix)^2 + (z - Iz)^2 = R^2
            <-> (x - Ix)^2 + (z - Iz)^2 - R^2 = 0
        -> on remplace par les coordonnées du point du rayon :
            p(t) = A + t * B;
            donc ( p(t).x - Ix )^2 + ( p(t).z - Iz)^2 - R^2 = 0
            <->  t^2 [ (B.x)^2 + (B.z)^2 ]
                + t  [ 2 ( A.x B.x - I.x B.x + A.z B.z - I.z B.z ) ]
                +    [ (A.x)^2 - 2 ( I.x A.x + I.z A.z) + (A.z)^2 - R^2 ]
                = 0 (Eq1)
        -> on calcule le discrimant de (Eq1) : Delta = b^2 - 4 a c 
        -> on détermine le plus petit t, s'il existe :
            si delta >= 0, alors t_min = [- b - sqrt(delta) ] / 2 a
        -> si t existe, on détermine la valeur de p(t).y :
            -> Si p(t).y est compris dans les bornes du cylindre alors il y a intersection !  


4) Equation d'un cylindre das l'axe Y : test 2èm méthode

    - equation cylindre centré sur l'axe Y de bornes y0 et y1 et de rayon R: 
        x^2 + z^2 = R^2
    - test d'intersection avec le rayon p(t) = A + t * B 
            x^2 + z^2 = R^2
        <-> (p(t).x)^2 + (p(t).y)^2 - R^2 = 0
        <-> (A.x + t * B.x )^2 + (A.y + t * B.y)^2 - R^2 = 0
        <-> t^2 [ B.x^2 + B.z^2 ]
            + t [ 2 (A.x B.x + A.z B.z) ]
            +   [ A.x^2 + A.z^2 - R^2 ]
            = 0
    - on calcule le déterminant de cette equation
    - si déterminant < 0 : pas d'intersection
    - sinon, on calcule les racines t0 et t1 de cette equation  
    - on calcule les points d'intersection i0 et i1 et on les compare
    - si y0 < i0.y < y1 on a touché le côté du cylindre
    - si i0.y < y0 mais i1.y > y0 alors on a touché le bas du cylindre
    - si i0.y > y1 mais i1 < y1 alors on a touché le haut du cylindre
    - sinon pas d'intersection (hors des limites du cylindre)

    note : cas extreme : rayon dans le cylindre infini 
        -> à gérer comme une interection de disque simple

5) Equation d'un cone axé sur Y : 
http://lousodrome.net/blog/light/2017/01/03/intersection-of-a-ray-and-a-cone/
    V = vecteur unitaire de la direction de la pointe vers le centre du disque
    T = angle en rad entre l'axe du cone Y et la limite du disque de la base
    S = sommet du cône
    AS = S - A = vecteur distance entre origine du rayon et sommet du cone
    équation :
        t^2 [ prod_scal(B, V)^2 - cos(T)^2 ]
        + t [ 2 * ( prod_scal(B,V) * prod_scal(AS, V) - prod_scal(B, AS) * cos(T)^2 ) ]
        +   [ prod_scal(AS,B)^2 - prod_scal(AS,AS) * cos(T)^2 ]
    http://www.ctralie.com/PrincetonUGRAD/Projects/COS426/Assignment3/part1.html#raycone 

    k = rayon_base / hauteur;
    yk = A.y - S.y;

    t^2 [ B.x^2 + B.z^2 - (k^2 * B.y^2) ]
    + t [ 2 * ( (B.x * A.x) + (B.z * A.z) - (k^2 * yk * B.y) ) ] 
    +   [ A.x^2 + A.z^2 - k^2 * yk^2 ] = 0

    https://stackoverflow.com/questions/13792861/surface-normal-to-a-cone/13792998 