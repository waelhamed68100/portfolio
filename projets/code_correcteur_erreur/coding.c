/**
 * @file coding.c
 * @author Arash Habibi
 * @author Julien Montavont
 * @version 2.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Generate code words from the initial data flow
 */

#include "codingdecoding.h"
#include <stdio.h>

CodeWord_t G [G_len] = {
    56, 28, 142, 71, 27, 181, 226, 113,  128, 64, 32, 16, 8, 4, 2, 1
};


void copyDataBitsCoding(char *message, CodeWord_t *cw, int size)
{
  int i = 0;
  
  for(i=0; i<size; i++)
    {
      setNthBitCW(&(cw[i]), 1, getNthBit(message[i], 1));
      setNthBitCW(&(cw[i]), 2, getNthBit(message[i], 2));
      setNthBitCW(&(cw[i]), 3, getNthBit(message[i], 3));
      setNthBitCW(&(cw[i]), 4, getNthBit(message[i], 4));
      setNthBitCW(&(cw[i]), 5, getNthBit(message[i], 5));
      setNthBitCW(&(cw[i]), 6, getNthBit(message[i], 6));
      setNthBitCW(&(cw[i]), 7, getNthBit(message[i], 7));
      setNthBitCW(&(cw[i]), 8, getNthBit(message[i], 8));
    }

  return;
}



/*
// Calcul des bits de contrôle
// Version 1 : bit de parité
void computeCtrlBits(CodeWord_t *message, int size)
{
  int sum;
  for(int m = 0; m < size; m++){
  
    sum = 0;
    for(int b = 1; b <= 8; b++){
      sum += getNthBit(message[m], b);
    }
    printf("sum(message[%d]) = sum('%c') = %d -> bit de parité = %d\n", m, (char)message[m], sum, sum%2);
  
    setNthBitCW(&(message[m]), 16, sum%2);
   
  }
  return;
}
*/

/*
// Calcul des bits de contrôle 
// Version 2 : polynôme générateur
void computeCtrlBits(CodeWord_t *message, int size)
{
  for(int m = 0; m < size; m++){
    CodeWord_t reste = division_entiere(message[m] << 8, POLY_GEN);
    for(int i = 1; i <= 8; i++){
      setNthBitCW(&(message[m]), i + 8, getNthBit(reste,i));
    }
  }
  return;
}
*/

// Calcul des bits de contrôle 
// Version 3 : Matrice génératrice basée sur le polynome G
void computeCtrlBits(CodeWord_t *message, int size)
{
  printf("\n------- Matrice d'encodage G ( . = 0 ) :\n\n");
  print_G();
  printf("\n");

  //pour chaque mot à coder
  for(int m = 0; m < size; m++){
    CodeWord_t code = 0;
    //pour chaque colonne du vecteur du mot codé
    for(int colonne = 0; colonne < G_len; colonne++){
      CodeWord_t col = G[colonne];
      int parite = 0;
      for(int ligne = 0; ligne < 8; ligne++){

        if(getNthBit(message[m], ligne+1) == 1 
          && getNthBit(col, ligne+1) == 1)
            parite++;
        
      }
      parite = parite % 2;
      setNthBitCW(&code, 16-colonne, parite);
    }
    message[m] = code;
  }
  return;
}


void coding(char *message, int data_size, char *cw, int *cw_size)
{
  *cw_size = data_size * sizeof(CodeWord_t);
  
  
  // copie des bits des lettres du message dans leur conteneur codeword
  // (le codage se fera directement sur les codeword)
  //message[0] = 162;
  //message[1] = 61;
  copyDataBitsCoding(message, (CodeWord_t*)cw, data_size);
  
  //affichage des lettres sans encodage
  printf("CODING: Lettres sans encodage : \n\n");
  char s[20];
  for(int i = 0; i < data_size; i++){
    sprintf (s, "cw[%d] : %c", i, message[i]);
    printBits(((CodeWord_t*)cw)[i], s);
  }

  // calcul du bit de contrôle
  printf("\nCODING: Codage de %d lettres dans des mots de longueur %d : \n\n", data_size, *cw_size);
  computeCtrlBits((CodeWord_t*)cw, data_size);

  //affichage des lettres encodées dans la suite de CodeWord 
  printf("\nCODING: Lettres encodées : \n\n");
  
  for(int i = 0; i < data_size; i++){
    sprintf (s, "cw[%d] : %c", i, message[i]);
    printBits(((CodeWord_t*)cw)[i], s);
  }

  return;
}


//affiche la matrice G
void print_G() {
  /*
  for(int i = 0 ; i < G_len; i++){
    printBits(G[i],"");
  }
  */
  for(int ligne = 0; ligne < 8; ligne++){
    for (int colonne = 0; colonne < G_len; colonne++){
      printf("%c ", getNthBit(G[colonne], 8-ligne) == 1 ? '1' : '.');
    }
    printf("\n");
  }
  
}
