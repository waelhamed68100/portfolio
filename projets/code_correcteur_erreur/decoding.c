/**
 * @file decoding.c
 * @author Arash Habibi
 * @author Julien Montavont
 * @version 2.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Functions to decipher the code words
 */

#include <stdio.h>
#include "codingdecoding.h"

CodeWord_t H [H_len] = {
  32824, 16412, 8334, 4167, 2075, 1205, 738, 369
};


void copyDataBitsDecoding(CodeWord_t *cw, char *message, int data_size)
{
  int i = 0;
  printf("copy, data_size = %d\n",data_size);
  for(i=0; i<data_size; i++)
    {
      setNthBitW(&(message[i]), 1, getNthBit(cw[i], 1));
      setNthBitW(&(message[i]), 2, getNthBit(cw[i], 2));
      setNthBitW(&(message[i]), 3, getNthBit(cw[i], 3));
      setNthBitW(&(message[i]), 4, getNthBit(cw[i], 4));
      setNthBitW(&(message[i]), 5, getNthBit(cw[i], 5));
      setNthBitW(&(message[i]), 6, getNthBit(cw[i], 6));
      setNthBitW(&(message[i]), 7, getNthBit(cw[i], 7));
      setNthBitW(&(message[i]), 8, getNthBit(cw[i], 8));
    }
}

/*
// Détecte les erreurs dans les mots codés reçus.
// Note : version 1 : le bit de parité est utilisé.
int thereIsError(CodeWord_t *cw, int data_size)
{
  printf("DECODING: vérification bits de parité :\n\n");
  int thereIsErr = 0;
  int sum;
  for(int m = 0; m < data_size; m++){
  
    sum = 0;
    for(int b = 1; b <= 8; b++){
      sum += getNthBit(cw[m], b);
    }
    printf("sum(cw[%d]) = sum('%c') = %d -> bit de parité = %d\n", m, (char)cw[m], sum, sum%2);
  
    int parity_bit = getNthBit(cw[m], 16);

    if(parity_bit != sum%2){
      printf("Bit de parité reçu != parité des données calculé. CodeWord cw[%d] erroné !\n\n",m);
      thereIsErr = 1;
    }

  }
  return thereIsErr;
}
*/
/*
// Détecte les erreurs dans les mots codés reçus.
// Note : version 2 : le polynome générateur est utilisé.
int thereIsError(CodeWord_t *cw, int data_size)
{
  printf("\nDECODING: vérification par division polynomiale :\n\n");
  int thereIsErr = 0;
  CodeWord_t reste;
  for(int m = 0; m < data_size; m++){
    CodeWord_t dividende = cw[m] << 8;
    for(int j = 1; j <= 8; j++){
      setNthBitCW( &dividende, j, getNthBit(cw[m], j+8) );
    }
    reste = division_entiere(dividende, POLY_GEN);
    
    if(reste != 0){
      thereIsErr = 1;
      printf("DECODING: cw[%d] : le reste de la division polynomiale != 0. CodeWord cw[%d] erroné !\n", m, m);
      printBits(cw[m], "mot erroné");
      printf("\n");
    }
  }
  return thereIsErr;
}
*/

// Détecte les erreurs dans les mots codés reçus.
// Note : version 3 : la matrice de décodage H est utilisée
int thereIsError(CodeWord_t *cw, int data_size)
{

  printf("\n------- Matrice de décodage H ( . = 0 ) :\n\n");
  print_H();
  printf("\n");

  printf("\nDECODING: détection d'erreurs par calcul matriciel (H) :\n\n");
  int thereIsErr;
  
  for(int m = 0; m < data_size; m++){
    CodeWord_t Y = 0;
    for(int ligne = 0 ; ligne < H_len; ligne++){
      CodeWord_t line = H[ligne];
      int parite = 0;
      for(int col = 0; col < 16; col ++){
        if(getNthBit(line, 16-col) == 1
          && getNthBit(cw[m], 16-col) == 1)
            parite++;
      }
      parite = parite % 2;
      setNthBitCW(&Y, 8-ligne, parite);
      
    }
    if(Y != 0){
      printf("DECODING: cw[%d] : vecteur décodé != 0. CodeWord cw[%d] erroné !\n", m, m);
      printf("DECODING: vecteur décodé : ");
      printBits(Y,"Y"); printf("\n");
      thereIsErr = Y;
    }
    
  }
  return thereIsErr;
}


void errorCorrection(CodeWord_t *cw, int data_size)
{
  printf("\nDECODING: correction des erreurs (si possible) :\n\n");
  
  for(int m = 0; m < data_size; m++){
    CodeWord_t Y = 0;
    for(int ligne = 0 ; ligne < H_len; ligne++){
      CodeWord_t line = H[ligne];
      int parite = 0;
      for(int col = 0; col < 16; col ++){
        if(getNthBit(line, 16-col) == 1
          && getNthBit(cw[m], 16-col) == 1)
            parite++;
      }
      parite = parite % 2;
      setNthBitCW(&Y, 8-ligne, parite);
      
    }
    if(Y != 0){

      printf("DECODING: tentative de correction de cw[%d]...\n", m);
      

      //on cherche la colonne que H qui correspond à Y
      int col = -1;
      for(int colonne = 0; colonne < 16 && col == -1;  colonne++ ){
        int check = 0;
        for(int ligne = 0; ligne < H_len; ligne++){
          if(getNthBit(H[ligne], 16-colonne) != getNthBit(Y, 8-ligne)){
            check = 1;
          }
        }
        if(check == 0) col = 16-colonne;
      }
      if(col == -1){
        printf("DECODING: correction impossible !\n\n");
      }
      else {
        printf("Erreur dans la colonne %d du vecteur codé reçu (en comptant de droite à gauche)\n", col);
        int bit = getNthBit(cw[m], col);
        bit = 1 - bit;
        setNthBitCW(cw + m, col, bit);
        printf("Erreur corrigée ! \n\n");
      }
      
      
    }
    
  }
  return;
}

void decoding(char *cw, int cw_size, char *message, int *data_size)
{
  /*
  printf("===================== TESTS:\n");
  division_entiere(162 << 8, POLY_GEN);
  division_entiere(61 << 8, POLY_GEN); 
  division_entiere(43605, POLY_GEN);
  division_entiere(41627, POLY_GEN);
  division_entiere(15819, POLY_GEN);
  printf("===================== FIN TESTS\n");
  */
 
  *data_size = cw_size / 2;
  printf("cw_size = %d, *data_size = %d\n",cw_size,*data_size);

  

  //-- For decoding
  copyDataBitsDecoding((CodeWord_t*)cw, message, *data_size);
  printf("\nDECODING : bits recus :\n\n");
  char s[20];
  for(int i = 0; i < cw_size; i+=2){
    sprintf (s, "cw[%d] : %c", i/2, cw[i]);
    printBits(((CodeWord_t*)(cw + i))[0], s);
  }

  //-- For error correction
  //-- to uncomment when complete and needed
  //errorCorrection((CodeWord_t*)cw, *data_size);

  //-- For error detection
  if(thereIsError((CodeWord_t*)cw, *data_size))
  {
    printf("\nPARITY ERROR: \"%s\"\n", message);
    //correction seulement si on utilise le code de hamming (exercice 3) !
    errorCorrection((CodeWord_t*)cw, *data_size);

    printf("DECODING: codewords après correction :\n\n");
    for(int i = 0; i < cw_size; i+=2){
      sprintf (s, "cw[%d] : %c", i/2, cw[i]);
      printBits(((CodeWord_t*)(cw + i))[0], s);
    }
    printf("\n");
  }

  

  return;
}

//affiche la matrice de décodage H
void print_H(){
  /*
  for(int i = 0 ; i < G_len; i++){
    printBits(G[i],"");
  }
  */
  for(int ligne = 0; ligne < H_len; ligne++){
    for (int colonne = 0; colonne < 16; colonne++){
      printf("%c ", getNthBit(H[ligne], 16 - colonne) == 1 ? '1' : '.');
    }
    printf("\n");
  }
}