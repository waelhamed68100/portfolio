# Code correcteur d'erreurs

Année : 2019

Programme C qui a pour but de détecter des erreurs de transmission sur un réseau (quelques bits inversés par octet transmis) et de les corriger si possible, grâce à calculs polynomiaux.

Points particuliers : gestion de l'encodage et du décodage.

Le programme est une simulation : les données ne sont pas réellement transmises sur le réseau car les réseaux sont fiables et il est donc difficiles d'avoir beaucoup d'erreurs générées naturellement.

Le programme est composé de 3 parties principales : 
 * **CODING** : encodage des données : des données de 8 bits sont encodées sur 16 bits puis elles sont "transmises" sur le réseau (en réalité elles sont envoyées au programme MEDIUM, simulant des interférences sur le réseau)
 * **MEDIUM** : selon ses arguments, il introduit une erreur dans chaque octet "transmis" sur le réseau.
 * **DECODING** : réceptionne les données erronées. Les détecte et les corrige si possible.

L'encodage, la détection et la correction se font à l'aide de matrices d'encodage/décodage.

## Dépendances 
---
 * Compilateur C

## Compilation et Exécution
---
* version simple : éxecuter le fichier `run.sh`


## Exemple d'affichage possible pendant l'éxecution :
---

    SENDING: chaine à envoyer = 'test'

    CODING: Lettres sans encodage : 

    'cw[0] : t' -> 00000000 01110100 (b) <-> 116 (d)
    'cw[1] : e' -> 00000000 01100101 (b) <-> 101 (d)
    'cw[2] : s' -> 00000000 01110011 (b) <-> 115 (d)
    'cw[3] : t' -> 00000000 01110100 (b) <-> 116 (d)

    CODING: Codage de 4 lettres dans des mots de longueur 8 : 


    ------- Matrice d'encodage G ( . = 0 ) :

    . . 1 . . 1 1 . 1 . . . . . . . 
    . . . 1 . . 1 1 . 1 . . . . . . 
    1 . . . . 1 1 1 . . 1 . . . . . 
    1 1 . . 1 1 . 1 . . . 1 . . . . 
    1 1 1 . 1 . . . . . . . 1 . . . 
    . 1 1 1 . 1 . . . . . . . 1 . . 
    . . 1 1 1 . 1 . . . . . . . 1 . 
    . . . 1 1 1 . 1 . . . . . . . 1 


    CODING: Lettres encodées : 

    'cw[0] : t' -> 00101101 01110100 (b) <-> 11636 (d)
    'cw[1] : e' -> 11111101 01100101 (b) <-> 4294966629 (d)
    'cw[2] : s' -> 01111110 01110011 (b) <-> 32371 (d)
    'cw[3] : t' -> 00101101 01110100 (b) <-> 11636 (d)
    Transmission complete

    MEDIUM : bits recus :

    'cw[0] : t' -> 00101101 01110100 (b) <-> 11636 (d)
    'cw[1] : e' -> 11111101 01100101 (b) <-> 4294966629 (d)
    'cw[2] : s' -> 01111110 01110011 (b) <-> 32371 (d)
    'cw[3] : t' -> 00101101 01110100 (b) <-> 11636 (d)

    MEDIUM: ajout d'erreur(s) (nb_rec = 8, error_type = 1) :

    MEDIUM a: erreur ajoutée (bit 1) dans les données de cw[0]: 't' (116) <- 'v' (118)
    MEDIUM a: erreur ajoutée (bit 1) dans les données de cw[1]: 'e' (101) <- 'g' (103)
    MEDIUM b: erreur ajoutée (bit 2) dans le ctrl de cw[2]: '~' (126) <- 'z' (122)
    MEDIUM a: erreur ajoutée (bit 7) dans les données de cw[3]: 't' (116) <- '�' (244)

    MEDIUM : bits à envoyer :

    'cw[0] : v' -> 00101101 01110110 (b) <-> 11638 (d)
    'cw[1] : g' -> 11111101 01100111 (b) <-> 4294966631 (d)
    'cw[2] : s' -> 01111010 01110011 (b) <-> 31347 (d)
    'cw[3] : �' -> 00101101 11110100 (b) <-> 11764 (d)
    Transfer complete.

    DECODING : bits recus :

    'cw[0] : v' -> 00101101 01110110 (b) <-> 11638 (d)
    'cw[1] : g' -> 11111101 01100111 (b) <-> 4294966631 (d)
    'cw[2] : s' -> 01111010 01110011 (b) <-> 31347 (d)
    'cw[3] : �' -> 00101101 11110100 (b) <-> 11764 (d)

    ------- Matrice de décodage H ( . = 0 ) :

    1 . . . . . . . . . 1 1 1 . . . 
    . 1 . . . . . . . . . 1 1 1 . . 
    . . 1 . . . . . 1 . . . 1 1 1 . 
    . . . 1 . . . . . 1 . . . 1 1 1 
    . . . . 1 . . . . . . 1 1 . 1 1 
    . . . . . 1 . . 1 . 1 1 . 1 . 1 
    . . . . . . 1 . 1 1 1 . . . 1 . 
    . . . . . . . 1 . 1 1 1 . . . 1 


    DECODING: détection d'erreurs par calcul matriciel (H) :

    DECODING: cw[0] : vecteur décodé != 0. CodeWord cw[0] erroné !
    DECODING: vecteur décodé : 'Y' -> 00000000 00111010 (b) <-> 58 (d)

    DECODING: cw[1] : vecteur décodé != 0. CodeWord cw[1] erroné !
    DECODING: vecteur décodé : 'Y' -> 00000000 00111010 (b) <-> 58 (d)

    DECODING: cw[2] : vecteur décodé != 0. CodeWord cw[2] erroné !
    DECODING: vecteur décodé : 'Y' -> 00000000 00000100 (b) <-> 4 (d)

    DECODING: cw[3] : vecteur décodé != 0. CodeWord cw[3] erroné !
    DECODING: vecteur décodé : 'Y' -> 00000000 00100110 (b) <-> 38 (d)


    DECODING: correction des erreurs (si possible) :

    DECODING: tentative de correction de cw[0]...
    Erreur dans la colonne 2 du vecteur codé reçu (en comptant de droite à gauche)
    Erreur corrigée ! 

    DECODING: tentative de correction de cw[1]...
    Erreur dans la colonne 2 du vecteur codé reçu (en comptant de droite à gauche)
    Erreur corrigée ! 

    DECODING: tentative de correction de cw[2]...
    Erreur dans la colonne 11 du vecteur codé reçu (en comptant de droite à gauche)
    Erreur corrigée ! 

    DECODING: tentative de correction de cw[3]...
    Erreur dans la colonne 8 du vecteur codé reçu (en comptant de droite à gauche)
    Erreur corrigée ! 

    DECODING: codewords après correction :

    'cw[0] : t' -> 00101101 01110100 (b) <-> 11636 (d)
    'cw[1] : e' -> 11111101 01100101 (b) <-> 4294966629 (d)
    'cw[2] : s' -> 01111110 01110011 (b) <-> 32371 (d)
    'cw[3] : t' -> 00101101 01110100 (b) <-> 11636 (d)
