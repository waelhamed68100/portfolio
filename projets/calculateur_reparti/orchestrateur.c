/** @file orchestrateur.c
 * @brief contient le programme de gestion des noeuds de calcul
 */
#include "orchestrateur.h"

/** @brief Thread A qui traite les commande données sur stdin
 * @param osef Variable inutile mais obligatoire pour respecter la convention
 */
void * thread_read_stdin(void * osef){
    (void)osef; //var inutilisée, pour le compilateur
    /* lecture de la commande */
    int len = 1024;
    char *buffer = malloc(sizeof(char)*len); //buff de lecture de la chaine envoyée sur stdin
    char* read;
    
    print_info("A : Attente de commande sur stdin...\n");
    print_info("A : orchestrateur>\n");
    read = fgets(buffer, len, stdin);
    if(read == NULL){
        perror("read thread_read_stdin ");
        exit(EXIT_FAILURE);
    }
    buffer[strlen(buffer)-1] = '\0'; //on supprime le retour à la ligne
    print_debug("A : Chaîne reçue stdin : '%s'\n", buffer);
    /* traitement de la commande */
    //cas requete de calcul : <operation>(x1,x2,...)
    if(strchr(buffer,'(') == NULL || strchr(buffer,')') == NULL){
        printf("A : ERREUR : Mauvaise requete de calcul. Format : <operation>(x1,x2,...)\n");
        free(buffer);
        return thread_read_stdin(NULL);
    }
    
    //on récupère l'opération
    char * copie_buffer = malloc(sizeof(char)*strlen(buffer)+1);
    strcpy(copie_buffer,buffer);
    //print_debug("copie_buffer : %s\n", copie_buffer);
    char * operation = strtok(copie_buffer,"(");
    print_debug("A : requete :\n", operation);
    print_debug("A : --> operation = '%s'\n", operation);
    
    /* on regarde si un noeud ipv4 permet cette opération */
    int slot = -1; 
    int protocole;
    //mutex lock car le tableau est une variable critique 
    int err;
	//Debut de la zone protegee. 
    if((err = pthread_mutex_lock(&mutex_noeuds_ipv4)) != 0){
    	errno = err;
    	perror("mutex_lock thread_read_stdin ipv4"); 
    }
    //recherche d'un slot disponible 
    for(int i = 0; i < 100; i++){
        //printf("i=%d status=%d op%s\n",i,noeuds_ipv4[i].status,noeuds_ipv4[i].operation);
        if(noeuds_ipv4[i].status == NOEUD_LIBRE
            && strcmp(noeuds_ipv4[i].operation,operation) == 0 ){
            print_debug("A : ==> Noeud ipv4 i=%d pouvant accepter la requete de calcul\n",i);
            slot = i;
            protocole = IPV4;
            break;
        }
    }
    //mutex unlock 
    if((err = pthread_mutex_unlock(&mutex_noeuds_ipv4)) != 0){
    	errno = err;
    	perror("mutex_unlock pthread_mutex_unlock"); 
    }

    //si aucun noeud ipv4 n'a été trouvé, on cherche dans les noeuds ipv6
    if(slot == -1){
        //Debut de la zone protegee. 
        if((err = pthread_mutex_lock(&mutex_noeuds_ipv6)) != 0){
            errno = err;
            perror("mutex_lock thread_read_stdin ipv6"); 
        }
        //recherche d'un slot disponible 
        for(int i = 0; i < 100; i++){
            if(noeuds_ipv6[i].status == NOEUD_LIBRE
                && strcmp(noeuds_ipv6[i].operation,operation) == 0 ){
                print_debug("A : ==> Noeud ipv6 i=%d pouvant accepter la requete de calcul\n",i);
                slot = i;
                protocole = IPV6;
                break;
            }
        }
        //mutex unlock 
        if((err = pthread_mutex_unlock(&mutex_noeuds_ipv6)) != 0){
            errno = err;
            perror("mutex_unlock pthread_mutex_unlock"); 
        }
    }
    
    /* noeud trouvé : extraction des paramètres */
    char * copie_buffer2;
    //noeud ipv4
    if(slot != -1 && protocole == IPV4){
        /* extraction des paramètres */
        copie_buffer2 = malloc(sizeof(char)*strlen(buffer));
        memcpy(copie_buffer2, 
            buffer + strlen(operation) + 1,
            strlen(buffer)-strlen(operation)-2
        );
        char *token = strtok(copie_buffer2, ",");
        const int nb_param = noeuds_ipv4[slot].nb_param;
        TYPEOF_PARAMS *params = malloc(sizeof(TYPEOF_PARAMS)* nb_param);
        for(int i = 0; i < nb_param; i++){
            print_debug("A : param %d : %s\n",i+1,token);
            double p = atof(token);
            params[i] = p;
            token = strtok(NULL,",");
        }
        //lancement du thread qui enverra la requete ipv4 de calcul, et receptionnera son résultat
        print_info("A : lancement du thread E* d'envoi des requetes de calcul ipv4.\n");
        param_thread_requete_calcul_ipv4 params_requete = malloc(sizeof(struct u_param_thread_requete_calcul_ipv4));
        params_requete->noeud_ipv4 = noeuds_ipv4[slot];
        params_requete->params_fct_calcul = params;
        params_requete->commande = malloc(sizeof(char)*strlen(buffer));
        strcpy(params_requete->commande ,buffer);
        noeuds_ipv4[slot].status = NOEUD_CALCUL;
        pthread_t tid_osef;
        int err = pthread_create(&tid_osef, NULL, thread_requete_calcul_ipv4, params_requete);
        if(err != 0){
            errno = err;
            perror("pthread_create thread_read_stdin");
            exit(EXIT_FAILURE);
        }
        free(copie_buffer2);
    }
    //noeud ipv6
    else if(slot != -1 && protocole == IPV6){
        /* extraction des paramètres */
        copie_buffer2 = malloc(sizeof(char)*strlen(buffer));
        memcpy(copie_buffer2, 
            buffer + strlen(operation) + 1,
            strlen(buffer)-strlen(operation)-2
        );
        char *token = strtok(copie_buffer2, ",");
        const int nb_param = noeuds_ipv6[slot].nb_param;
        TYPEOF_PARAMS *params = malloc(sizeof(TYPEOF_PARAMS)* nb_param);
        for(int i = 0; i < nb_param; i++){
            print_debug("A : param %d : %s\n",i+1,token);
            double p = atof(token);
            params[i] = p;
            token = strtok(NULL,",");
        }
        //lancement du thread qui enverra la requete ipv4 de calcul, et receptionnera son résultat
        print_info("A : lancement du thread G* d'envoi des requetes de calcul ipv6.\n");
        param_thread_requete_calcul_ipv6 params_requete = malloc(sizeof(struct u_param_thread_requete_calcul_ipv6));
        params_requete->noeud_ipv6 = noeuds_ipv6[slot];
        params_requete->params_fct_calcul = params;
        params_requete->commande = malloc(sizeof(char)*strlen(buffer));
        strcpy(params_requete->commande ,buffer);
        noeuds_ipv6[slot].status = NOEUD_CALCUL;
        pthread_t tid_osef;
        int err = pthread_create(&tid_osef, NULL, thread_requete_calcul_ipv6, params_requete);
        if(err != 0){
            errno = err;
            perror("pthread_create thread_read_stdin");
            exit(EXIT_FAILURE);
        }
        free(copie_buffer2);
    }
    //si aucun noeud n'a été trouvé, on ne fait rien
    else{
        print_info("A : Aucun noeud de libre pouvant accepté la requête :/\n");
    }
    
    /* on recommence : passage à la commande suivante */
    free(buffer);
    free(copie_buffer);
    
    thread_read_stdin(NULL);
    return NULL;
}

/** @brief Thread E* qui envoie une requete de calcul ipv4 et attend sa réponse
 * @param param pointeur param_thread_requete_calcul_ipv4 contenant les paramètres necessaires
 */
void * thread_requete_calcul_ipv4(void * param){
    /* création du socket UDP ipv4 de reception du résultat du calcul */
    int socket_reception_calcul;
    if((socket_reception_calcul = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("erreur création socket");
	    exit(EXIT_FAILURE);
    }

    //init des infos sur notre adresse
    struct sockaddr_in adr_reception;
    adr_reception.sin_family = AF_INET; //type (ipv4 || ipv6)
    adr_reception.sin_port = 0;//htons(atoi(argv[1])); //port
    adr_reception.sin_addr.s_addr = htonl(INADDR_ANY); //adresse ip

    char buf_reception_calc[UDP_IPv4_PAYLOAD_SIZE];
    (void)buf_reception_calc;

    //liaison entre le socket et notre adresse
    if (bind(socket_reception_calcul, (struct sockaddr *)&adr_reception, addrlen_ipv4) == -1){
        perror("bind thread_requete_calcul_ipv4");
        close(socket_reception_calcul);
        exit(EXIT_FAILURE);
    }

    //récupération du port choisi par l'os
    if (getsockname(socket_reception_calcul, (struct sockaddr *)&adr_reception, &addrlen_ipv4) == -1){
        perror("getsockname");
        close(socket_reception_calcul);
        exit(EXIT_FAILURE);
    }
    uint16_t port_reception_res_calc = ntohs(adr_reception.sin_port);
    print_debug("E* : Port de reception du resultat du calcul : %d\n", port_reception_res_calc);
    
    /* construction de la requete de calcul */
    print_debug("E* : requete de calcul ipv4 en construction.\n");
    
    param_thread_requete_calcul_ipv4 p = (param_thread_requete_calcul_ipv4)param;
    
    requete_calcul requete = malloc(sizeof(struct u_requete_calcul));
    //requete->params = malloc(sizeof(TYPEOF_PARAMS)*p->noeud_ipv4.nb_param);
    uint16_t port_noeud = p->noeud_ipv4.port;
    requete->params = p->params_fct_calcul;
    requete->nb_param = p->noeud_ipv4.nb_param;
    requete->port = port_reception_res_calc;
    for(int i = 0; i < p->noeud_ipv4.nb_param; i++){
        print_debug("E* : param %d = %f\n", i+1, requete->params[i]);
    }
    /* création du wrapper UDP */ 
    udp_message  udp_msg = malloc(sizeof( struct u_udp_message));
    udp_msg->message_type = malloc(sizeof(char)*MSG_SIZE);
    strcpy(udp_msg->message_type, MSG_REQUETE_CALCUL);
    udp_msg->payload = requete;
    /* envoi de la requete */
    //construction de l'adresse
    struct sockaddr_in adr_noeud;
    adr_noeud.sin_family = AF_INET;
    adr_noeud.sin_addr.s_addr =  p->noeud_ipv4.adresse_ip;
    adr_noeud.sin_port = htons(port_noeud);
    print_debug("E* : port = %d\n",port_noeud);
    //sérialisation
    int total_size;
    void *buf = serialize_udp_message(udp_msg, &total_size);
    //envoi
    print_debug("E* : envoi de la requete de calcul au noeud ipv4 (%d octets)\n",total_size);
    print_info("E* : attente du résultat de la requete de calcul...\n",total_size);

    if(sendto(socket_annonces_ipv4, buf, total_size, 0,
	(struct sockaddr *) &adr_noeud, addrlen_ipv4) == -1){
        perror("sendto thread_requete_calcul_ipv4");
	    close(socket_annonces_ipv4);
	    exit(EXIT_FAILURE);
    }
    /* maj de l'état du noeud dans la liste pour indiquer qu'il est occupé */
    /* attente de la réponse */
    print_debug("E* : attente de la réponse sur le port...%d\n",port_reception_res_calc);
    if (recvfrom(socket_reception_calcul, buf, UDP_IPv4_PAYLOAD_SIZE, 0, (struct sockaddr *)&adr_noeud, 
        &addrlen_ipv4) == -1){
        printf("errno = %d\n", errno);
        perror("recvfrom thread_requete_calcul_ipv4");
        exit(EXIT_FAILURE);
    }
    /* désérialisation de la réponse */
    udp_message udp_msg_res = deserialize_udp_message(buf);
    requete_calcul reponse = ((requete_calcul)udp_msg_res->payload);
    TYPEOF_CALC_RES resultat = reponse->result;
    print_info("E* : résultat du calcul :\n");
    char adr[130];
    inet_ntop(AF_INET,&(p->noeud_ipv4.adresse_ip), adr, (socklen_t)130);
    print_info("%s = %f par <%s:%d>\n", 
        p->commande, resultat, adr, port_noeud);
    /* màj de l'état du noeud (évite d'avoir à attendre son annonce) */
    int i = get_index_noeud_ipv4(p->noeud_ipv4.adresse_ip, port_noeud);
    if(i != -1){
        print_debug("E* : màj de l'état du noeud ipv4 (i=%d) (NOEUD_LIBRE)\n",i);
        noeuds_ipv4[i].status = NOEUD_LIBRE;
        
    }
    else{
        print_debug("E* : màj noeud impossible. Timeout'd ?\n");
    }
   
    free(p->commande);
    free(p);
    free(reponse->params);
    free(reponse);
    free(udp_msg_res->message_type);
    free(udp_msg_res);
    free(buf);
    return NULL;
}


/** @brief Thread G* qui envoie une requete de calcul ipv6 et attend sa réponse
 * @param param pointeur param_thread_requete_calcul_ipv6 contenant les paramètres necessaires
 */
void * thread_requete_calcul_ipv6(void * param){
    /* création du socket UDP ipv6 de reception du résultat du calcul */
    int socket_reception_calcul;
    if((socket_reception_calcul = socket(AF_INET6, SOCK_DGRAM, 0)) == -1){
        perror("erreur création socket");
	    exit(EXIT_FAILURE);
    }

    //init des infos sur notre adresse
    struct sockaddr_in6 adr_reception;
    adr_reception.sin6_family = AF_INET6; //type (ipv4 || ipv6)
    adr_reception.sin6_port = 0;//htons(atoi(argv[1])); //port
    adr_reception.sin6_addr= in6addr_any; //adresse ip
    adr_reception.sin6_flowinfo = 0;
    adr_reception.sin6_scope_id = 0;

    char buf_reception_calc[UDP_IPv6_PAYLOAD_SIZE];
    (void)buf_reception_calc;

    //liaison entre le socket et notre adresse
    if (bind(socket_reception_calcul, (struct sockaddr *)&adr_reception, addrlen_ipv6) == -1){
        perror("bind thread_requete_calcul_ipv6");
        close(socket_reception_calcul);
        exit(EXIT_FAILURE);
    }

    //récupération du port choisi par l'os
    if (getsockname(socket_reception_calcul, (struct sockaddr *)&adr_reception, &addrlen_ipv6) == -1){
        perror("getsockname");
        close(socket_reception_calcul);
        exit(EXIT_FAILURE);
    }
    uint16_t port_reception_res_calc = ntohs(adr_reception.sin6_port);
    print_debug("G* : Port de reception du resultat du calcul : %d\n", 
        port_reception_res_calc
    );
    
    /* construction de la requete de calcul */
    print_debug("G* : requete de calcul ipv6 en construction.\n");
    
    param_thread_requete_calcul_ipv6 p = (param_thread_requete_calcul_ipv6)param;
    
    requete_calcul requete = malloc(sizeof(struct u_requete_calcul));
    //requete->params = malloc(sizeof(TYPEOF_PARAMS)*p->noeud_ipv6.nb_param);
    uint16_t port_noeud = p->noeud_ipv6.port;
    requete->params = p->params_fct_calcul;
    requete->nb_param = p->noeud_ipv6.nb_param;
    requete->port = port_reception_res_calc;
    for(int i = 0; i < p->noeud_ipv6.nb_param; i++){
        print_debug("G* : param %d = %f\n", i+1, requete->params[i]);
    }
    /* création du wrapper UDP */ 
    udp_message  udp_msg = malloc(sizeof( struct u_udp_message));
    udp_msg->message_type = malloc(sizeof(char)*MSG_SIZE);
    strcpy(udp_msg->message_type, MSG_REQUETE_CALCUL);
    udp_msg->payload = requete;
    /* envoi de la requete */
    //construction de l'adresse
    struct sockaddr_in6 adr_noeud;
    adr_noeud.sin6_family = AF_INET6;
    adr_noeud.sin6_addr =  p->noeud_ipv6.adresse_ip;
    adr_noeud.sin6_port = htons(port_noeud);
    print_debug("G* : port = %d\n",port_noeud);
    //sérialisation
    int total_size;
    void *buf2 = serialize_udp_message(udp_msg, &total_size);
    //envoi
    print_debug("G* : envoi de la requete de calcul au noeud ipv6 (%d octets)\n",total_size);
    print_info("G* : attente du résultat de la requete de calcul...\n",total_size);

    if(sendto(socket_annonces_ipv6, buf2, total_size, 0,
	(struct sockaddr *) &adr_noeud, addrlen_ipv6) == -1){
        perror("sendto thread_requete_calcul_ipv6");
	    close(socket_annonces_ipv4);
	    exit(EXIT_FAILURE);
    }
    /* maj de l'état du noeud dans la liste pour indiquer qu'il est occupé */
    /* attente de la réponse */
    print_debug("G* : attente de la réponse sur le port...%d\n",port_reception_res_calc);
    if (recvfrom(socket_reception_calcul, buf2, UDP_IPv6_PAYLOAD_SIZE, 0, (struct sockaddr *)&adr_noeud, 
        &addrlen_ipv6) == -1){
        printf("errno = %d\n", errno);
        perror("recvfrom thread_requete_calcul_ipv6");
        exit(EXIT_FAILURE);
    }
    /* désérialisation de la réponse */
    udp_message udp_msg_res = deserialize_udp_message(buf2);
    requete_calcul reponse = ((requete_calcul)udp_msg_res->payload);
    TYPEOF_CALC_RES resultat = reponse->result;
    char adr[130];
    inet_ntop(AF_INET6,&(p->noeud_ipv6.adresse_ip), adr, (socklen_t)130);
    print_info("%s = %f par <%s:%d>\n", 
        p->commande, resultat, adr, port_noeud);
    /* màj de l'état du noeud (évite d'avoir à attendre son annonce) */
    int i = get_index_noeud_ipv6(p->noeud_ipv6.adresse_ip, port_noeud);
    if(i != -1){
        print_debug("G* : màj de l'état du noeud ipv4 (i=%d) (NOEUD_LIBRE)\n",i);
        noeuds_ipv6[i].status = NOEUD_LIBRE;
        
    }
    else{
        print_debug("G* : màj noeud impossible. Timeout'd ?\n");
    }
    /* free */
    free(p->commande);
    free(p);
    free(reponse->params);
    free(reponse);
    free(udp_msg_res->message_type);
    free(udp_msg_res);
    free(buf2);
    return NULL;
}


/** @brief Thread B* qui traite une requete udp 
 * @param param Struct param_thread_traitement_annonce contenant les paramètres necessaires
 */
void * thread_traitement_annonce(void * param){
    param_thread_traitement_annonce p = (param_thread_traitement_annonce)param;
    void * udp_msg_buf = p->udp_msg_buf;
    /* déserialisation du buf udp */
    udp_message msg = deserialize_udp_message(udp_msg_buf);
    
    /* vérification que c'est bien une annonce */
    if(strcmp(msg->message_type, MSG_ANNONCE) != 0){
        printf(
            "ERREUR : mauvais message attendu (%s au lieu de MSG_ANNONCE=%s)\n",
            msg->message_type,
            MSG_ANNONCE
        );
        exit(EXIT_FAILURE);
    }
    /* cas ipv4 */
    if(p->protocole == IPV4){
        /* lecture de la charge utile : l'annonce */
        annonce_noeud annonce = (annonce_noeud)msg->payload;

        /* check si le noeud est déjà présent dans la liste */
        char adr[130];
        inet_ntop(AF_INET,&(p->adresse_ipv4), adr, (socklen_t)130);

        //print_debug(const char* msg, ...)
        int i = get_index_noeud_ipv4(p->adresse_ipv4, annonce->port);
        /* ajout du noeud si noeud absent de la liste */
        if(i == -1){
            print_info("B* : annonce d'un nouveau noeud reçue : \n");
            print_info("B* : opération : %s\n", annonce->operation);
            print_info("B* : port : %d\n", annonce->port);
            print_info("B* : nb_param : %d\n", annonce->nb_param);
            print_info("B* : status : %d\n", annonce->status);
            print_info("B* : adr IPv4 : %s\n", adr);
            
            ajouter_noeud_ipv4(
                annonce->operation, 
                p->adresse_ipv4, 
                annonce->port, 
                annonce->nb_param
            );
            free(annonce->operation);
            
        }
        /* sinon màj de son état (pour éviter le timeout en particulier)*/
        else{
            print_debug("B* : Annonce noeud ipv4 :\n");
            print_debug("B* : opération : %s\n", annonce->operation);
            print_debug("B* : port : %d\n", annonce->port);
            print_debug("B* : nb_param : %d\n", annonce->nb_param);
            print_debug("B* : status : %d\n", annonce->status);
            print_debug("B* : adr IPv4 : %s\n", adr);
            //màj de l'état du noeud
            print_debug("B* : Noeud déjà présent dans la liste. Màj du noeud.\n");
            strcpy(noeuds_ipv4[i].operation,annonce->operation);
            noeuds_ipv4[i].nb_param = annonce->nb_param;
            noeuds_ipv4[i].adresse_ip = p->adresse_ipv4;
            noeuds_ipv4[i].port = annonce->port;
            noeuds_ipv4[i].status = annonce->status;
            noeuds_ipv4[i].date_dernier_msg = time(NULL);


            free(annonce->operation);
        }
    }
    /* cas ipv6 */
    else if(p->protocole == IPV6){
        /* lecture de la charge utile : l'annonce */
        annonce_noeud annonce = (annonce_noeud)msg->payload;
        
        /* check si le noeud est déjà présent dans la liste */
        char adr[130];
        inet_ntop(AF_INET6,&(p->adresse_ipv6), adr, (socklen_t)130);

        //print_debug(const char* msg, ...)
        int i = get_index_noeud_ipv6(p->adresse_ipv6, annonce->port);
        /* ajout du noeud si noeud absent de la liste */
        if(i == -1){
            print_info("B* : annonce d'un nouveau noeud reçue : \n");
            print_info("B* : opération : %s\n", annonce->operation);
            print_info("B* : port : %d\n", annonce->port);
            print_info("B* : nb_param : %d\n", annonce->nb_param);
            print_info("B* : status : %d\n", annonce->status);
            print_info("B* : adr IPv6 : %s\n", adr);
            
            ajouter_noeud_ipv6(
                annonce->operation, 
                p->adresse_ipv6, 
                annonce->port, 
                annonce->nb_param
            );
            free(annonce->operation);
        }
        /* sinon màj de son état (pour éviter le timeout en particulier)*/
        else{
            print_debug("B* : Annonce noeud ipv6 :\n");
            print_debug("B* : opération : %s\n", annonce->operation);
            print_debug("B* : port : %d\n", annonce->port);
            print_debug("B* : nb_param : %d\n", annonce->nb_param);
            print_debug("B* : status : %d\n", annonce->status);
            print_debug("B* : adr IPv6 : %s\n", adr);
            //màj de l'état du noeud
            print_debug("B* : Noeud déjà présent dans la liste. Màj du noeud.\n");

            strcpy(noeuds_ipv6[i].operation, annonce->operation);
            noeuds_ipv6[i].nb_param = annonce->nb_param;
            noeuds_ipv6[i].adresse_ip = p->adresse_ipv6;
            noeuds_ipv6[i].port = annonce->port;
            noeuds_ipv6[i].status = annonce->status;
            noeuds_ipv6[i].date_dernier_msg = time(NULL);


            free(annonce->operation);
        }
    }
    else{
        printf("B* : ERREUR : protocole inconnu (%d ?). Annonce ignorée.\n", 
            p->protocole
        );
    }
    free(msg->message_type);
    free(msg->payload);
    free(msg);
    free(p->udp_msg_buf);
    free(p);
    return NULL;
}

/** @brief Thread C qui receptionne les annonces UDP ipv4
 * @param osef Variable inutile mais obligatoire pour respecter la convention
 */
void * thread_get_annonce_ipv4(void * osef){
    (void)osef;
    /* reception des annonces */
    struct sockaddr_in addr_noeud; //adresse du noeud
    print_debug("C : En attente de reception d'une annonce ipv4 d'un noeud...\n");
    if (recvfrom(socket_annonces_ipv4, buf_socket_annonces_ipv4, UDP_IPv4_PAYLOAD_SIZE, 0, 
        (struct sockaddr *)&addr_noeud, &addrlen_ipv4) == -1){

        perror("err recvfrom thread_get_annonce");
        exit(EXIT_FAILURE);
    }
    print_debug("C : -------------------------------------------\n");
    print_debug("C : Annonce ipv4 reçue. Traitement...\n");
    /* création d'un thread pour traiter l'annonce */
	pthread_t thread_id;
    //copie du buffer recu par le socket
    void * udp_buf = malloc(UDP_IPv4_PAYLOAD_SIZE);
    //packaging des paramètres du thread de traitement de l'annonce
    param_thread_traitement_annonce param = malloc(sizeof (struct u_param_thread_traitement_annonce));
    param->protocole = IPV4;
    param->udp_msg_buf = udp_buf;
    param->adresse_ipv4 = addr_noeud.sin_addr.s_addr;
   
    memcpy(udp_buf, buf_socket_annonces_ipv4, UDP_IPv4_PAYLOAD_SIZE);
	/* création du thread de traitement de l'annonce */
    int err = pthread_create(&thread_id, NULL, thread_traitement_annonce, param);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_get_annonce_ipv4");
        exit(EXIT_FAILURE);
    }
    /* on recommence : reception du message suivant */
    thread_get_annonce_ipv4(NULL);
    return NULL;
}


/** @brief Thread F qui receptionne les annonces UDP ipv6
 * @param osef Variable inutile mais obligatoire pour respecter la convention
 */
void * thread_get_annonce_ipv6(void * osef){
    (void)osef;
    /* reception des annonces */
    struct sockaddr_in6 addr_noeud; //adresse du noeud
    print_debug("F : En attente de reception d'une annonce ipv6 d'un noeud...\n");
    if (recvfrom(socket_annonces_ipv6, buf_socket_annonces_ipv6, UDP_IPv6_PAYLOAD_SIZE, 0, 
        (struct sockaddr *)&addr_noeud, &addrlen_ipv6) == -1){

        perror("err recvfrom thread_get_annonce_ipv6");
        exit(EXIT_FAILURE);
    }
    print_debug("F : -------------------------------------------\n");
    print_debug("F : Annonce ipv6 reçue. Traitement...\n");
    /* création d'un thread pour traiter l'annonce */
	pthread_t thread_id;
    //copie du buffer recu par le socket
    void * udp_buf = malloc(UDP_IPv6_PAYLOAD_SIZE);
    //packaging des paramètres du thread de traitement de l'annonce
    param_thread_traitement_annonce param = malloc(sizeof (struct u_param_thread_traitement_annonce));
    param->protocole = IPV6;
    param->udp_msg_buf = udp_buf;
    param->adresse_ipv6 = addr_noeud.sin6_addr;
   
    memcpy(udp_buf, buf_socket_annonces_ipv6, UDP_IPv6_PAYLOAD_SIZE);
	/* création du thread de traitement de l'annonce */
    int err = pthread_create(&thread_id, NULL, thread_traitement_annonce, param);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_get_annonce_ipv6");
        exit(EXIT_FAILURE);
    }
    /* on recommence : reception du message suivant */
    thread_get_annonce_ipv6(NULL);
    return NULL;
}

/** @brief Ajoute les infos d'un nouveau noeud ipv4 
 * @param op Opération du noeud
 * @param adr_ip Adresse ip du noeud
 * @param port Port du socket de reception de la requete de calcul du noeud
 * @param nb_param Nombre de paramètres que la fonction de calcul prend en entrée
 */
int ajouter_noeud_ipv4(char* op, in_addr_t adr_ip, uint16_t port, uint16_t nb_param){
    /* mutex lock car le tableau est une variable critique */
    int err;
	/* Debut de la zone protegee. */
    if((err = pthread_mutex_lock(&mutex_noeuds_ipv4)) != 0){
    	errno = err;
    	perror("mutex_lock ajouter_noeud_ipv4"); 
    }
    /* recherche d'un slot disponible */
    int slot = -666;
    for(int i = 0; i < 100; i++){
        if(noeuds_ipv4[i].status == UNDEFINED_SLOT){
            //slot trouvé, on ajoute les infos du noeud
            strcpy(noeuds_ipv4[i].operation,op);
            noeuds_ipv4[i].nb_param = nb_param;
            noeuds_ipv4[i].adresse_ip = adr_ip;
            noeuds_ipv4[i].port = port;
            noeuds_ipv4[i].status = NOEUD_LIBRE;
            noeuds_ipv4[i].date_dernier_msg = time(NULL);
            print_debug("nouveau noeud ipv4 ajouté (i = %d)\n",i);
            slot = i;
            break;
        }
    }
    /* mutex unlock */
    if((err = pthread_mutex_unlock(&mutex_noeuds_ipv4)) != 0){
    	errno = err;
    	perror("mutex_unlock pthread_mutex_unlock"); 
    }
    /* check si un slot était dispo */
    if(slot == -666){
        print_debug("ERREUR : plus de 100 noeuds ipv4. Nouveau noeud ignoré\n");
    }
    return slot;
}

/** @brief Ajoute les infos d'un nouveau noeud ipv6
 * @param op Opération du noeud
 * @param adr_ip Adresse ip du noeud
 * @param port Port du socket de reception de la requete de calcul du noeud
 * @param nb_param Nombre de paramètres que la fonction de calcul prend en entrée
 */
int ajouter_noeud_ipv6(char* op, struct in6_addr adr_ip, uint16_t port, uint16_t nb_param){
    /* mutex lock car le tableau est une variable critique */
    int err;
	/* Debut de la zone protegee. */
    if((err = pthread_mutex_lock(&mutex_noeuds_ipv6)) != 0){
    	errno = err;
    	perror("mutex_lock ajouter_noeud_ipv6"); 
    }
    /* recherche d'un slot disponible */
    int slot = -666;
    for(int i = 0; i < 100; i++){
        if(noeuds_ipv6[i].status == UNDEFINED_SLOT){
            //slot trouvé, on ajoute les infos du noeud
            strcpy(noeuds_ipv6[i].operation,op);
            noeuds_ipv6[i].nb_param = nb_param;
            noeuds_ipv6[i].adresse_ip = adr_ip;
            noeuds_ipv6[i].port = port;
            noeuds_ipv6[i].status = NOEUD_LIBRE;
            noeuds_ipv6[i].date_dernier_msg = time(NULL);
            print_debug("nouveau noeud ipv6 ajouté (i = %d)\n",i);
            slot = i;
            break;
        }
    }
    /* mutex unlock */
    if((err = pthread_mutex_unlock(&mutex_noeuds_ipv6)) != 0){
    	errno = err;
    	perror("mutex_unlock pthread_mutex_unlock"); 
    }
    /* check si un slot était dispo */
    if(slot == -666){
        print_debug("ERREUR : plus de 100 noeuds ipv6. Nouveau noeud ignoré\n");
    }
    return slot;
}

/** @brief Renvoi l'index du noeud ipv4 correspondant à la signature 
 * @param adr_ip Adresse ip du noeud
 * @param port Port du socket de reception de la requete de calcul du noeud
 * @return l'index du noeud si trouvé, -1 sinon
*/
int get_index_noeud_ipv4(in_addr_t adr_ip, uint16_t port){
    (void)adr_ip;
    for(int i = 0; i < 100; i++){
        if(noeuds_ipv4[i].status != UNDEFINED_SLOT 
        //&& noeuds_ipv4[i].adresse_ip == adr_ip 
            && noeuds_ipv4[i].port == port 
        ){
            return i;
        }
    }
    //print_debug("WARNING: aucun noeud trouvé\n");
    return -1;
}

/** @brief Renvoi l'index du noeud ipv6 correspondant à la signature 
 * @param adr_ip Adresse ip du noeud
 * @param port Port du socket de reception de la requete de calcul du noeud
 * @return l'index du noeud si trouvé, -1 sinon
*/
int get_index_noeud_ipv6(struct in6_addr adr_ip, uint16_t port){
    (void)adr_ip;
    for(int i = 0; i < 100; i++){
        if(noeuds_ipv6[i].status != UNDEFINED_SLOT 
        //&& noeuds_ipv6[i].adresse_ip == adr_ip 
            && noeuds_ipv6[i].port == port 
        ){
            return i;
        }
    }
    //print_debug("WARNING: aucun noeud trouvé\n");
    return -1;
}

/** @brief Thread D : supprime tous les noeuds qui n'ont pas répondu depuis TIMEOUT_NOEUD secondes
 * @param osef Variable inutile mais obligatoire pour respecter la convention
*/
void * thread_timeout_noeud(void *osef){
    (void)osef;
    sleep(TIMEOUT_NOEUD);
    print_debug("D : mise à jour des noeuds actifs. TIMEOUT_NOEUD = %d sec.\n", TIMEOUT_NOEUD);
    int err;

    /* noeuds IPv4 */

    //mutex lock car le tableau est une variable critique 
	// Debut de la zone protegee. 
    if((err = pthread_mutex_lock(&mutex_noeuds_ipv4)) != 0){
    	errno = err;
    	perror("mutex_lock thread_timeout_noeud"); 
    }
    // vérification de la date du dernier message du noeud 
    time_t current_time = time(NULL);
    for(int i = 0; i < 100; i++){
        if(noeuds_ipv4[i].status != UNDEFINED_SLOT){
            //printf("i=%d ", i);
            double temps_ecoule = ((double)current_time - (double)noeuds_ipv4[i].date_dernier_msg);
            //printf("current_time=%d noeuds_ipv4[i].date_dernier_msg=%d temps_ecoule=%f TIMEOUT_NOEUD=%d\n",(int)current_time, (int)noeuds_ipv4[i].date_dernier_msg, temps_ecoule, TIMEOUT_NOEUD);
            if(temps_ecoule > TIMEOUT_NOEUD){
                print_debug(
                    "D : Noeud ipv4 i=%d a dépassé le TIMEOUT_NOEUD=%d (temps ecoulé : %f)\n", 
                    i, TIMEOUT_NOEUD, temps_ecoule
                );
                noeuds_ipv4[i].status = UNDEFINED_SLOT;
                print_info("D : Timeout Noeud ipv4 i=%d. Suppression.\n", i);
            }
        }
    }
    // mutex unlock 
    if((err = pthread_mutex_unlock(&mutex_noeuds_ipv4)) != 0){
    	errno = err;
    	perror("mutex_unlock thread_timeout_noeud"); 
    }

    /* noeuds IPv6 */

    //mutex lock car le tableau est une variable critique 
	// Debut de la zone protegee. 
    if((err = pthread_mutex_lock(&mutex_noeuds_ipv6)) != 0){
    	errno = err;
    	perror("mutex_lock thread_timeout_noeud"); 
    }
    // vérification de la date du dernier message du noeud 
    current_time = time(NULL);
    for(int i = 0; i < 100; i++){
        if(noeuds_ipv6[i].status != UNDEFINED_SLOT){
            //printf("i=%d ", i);
            double temps_ecoule = ((double)current_time - (double)noeuds_ipv6[i].date_dernier_msg);
            //printf("current_time=%d noeuds_ipv6[i].date_dernier_msg=%d temps_ecoule=%f TIMEOUT_NOEUD=%d\n",(int)current_time, (int)noeuds_ipv6[i].date_dernier_msg, temps_ecoule, TIMEOUT_NOEUD);
            if(temps_ecoule > TIMEOUT_NOEUD){
                print_debug(
                    "D : Noeud ipv6 i=%d a dépassé le TIMEOUT_NOEUD=%d (temps ecoulé : %f)\n", 
                    i, TIMEOUT_NOEUD, temps_ecoule
                );
                noeuds_ipv6[i].status = UNDEFINED_SLOT;
                print_info("D : Timeout Noeud ipv6 i=%d. Suppression.\n", i);
            }
        }
    }
    // mutex unlock 
    if((err = pthread_mutex_unlock(&mutex_noeuds_ipv6)) != 0){
    	errno = err;
    	perror("mutex_unlock thread_timeout_noeud"); 
    }
    /* on recommence */
    thread_timeout_noeud(NULL);
    return NULL;
}
/** @brief Créer le socket UDP ipv4 de reception des annonces
 * @param port_udp Port du socket
 */
void init_socket_annonces_ipv4(int port_udp){
    //socket factory
    if ((socket_annonces_ipv4 = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("erreur création socket");
        exit(EXIT_FAILURE);
    }
    //on indique qu'on veut utiliser le socket pour plusieurs adresses (non-obligatoire)
    int opt = true;
    if( setsockopt(socket_annonces_ipv4, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 )
    {
        perror("erreur setsockopt");
        exit(EXIT_FAILURE);
    }
    //init des infos sur notre adresse
    my_addr_annonces_ipv4.sin_family = AF_INET; //IPV4
    my_addr_annonces_ipv4.sin_port = htons(port_udp); //port
    my_addr_annonces_ipv4.sin_addr.s_addr = htonl(INADDR_ANY); //adresse ip locale

    addrlen_ipv4 = sizeof(struct sockaddr_in); //on récupère la taille de l'adr
    memset(buf_socket_annonces_ipv4, '\0', 1024); //on prépare le buffer
    //print_debug("Taille du buff = %d\n", (int)strlen(buf_socket_annonces_ipv4));

    //liaison entre le socket et notre adresse
    if (bind(socket_annonces_ipv4, (struct sockaddr *)&my_addr_annonces_ipv4, addrlen_ipv4) == -1){
        perror("bind socket reception annonce");
        close(socket_annonces_ipv4);
        exit(EXIT_FAILURE);
    }

    //récupération du port 
    if (getsockname(socket_annonces_ipv4, (struct sockaddr *)&my_addr_annonces_ipv4, &addrlen_ipv4) == -1){
        perror("getsockname");
        close(socket_annonces_ipv4);
        exit(EXIT_FAILURE);
    }

    print_debug("Port de reception des annonces IPV4 : %d\n", ntohs(my_addr_annonces_ipv4.sin_port));
}


/** @brief Créer le socket UDP ipv6 de reception des annonces
 * @param port_udp Port du socket
 */
void init_socket_annonces_ipv6(int port_udp){
    //socket factory
    if ((socket_annonces_ipv6 = socket(AF_INET6, SOCK_DGRAM, 0)) == -1){
        perror("erreur création socket init_socket_annonces_ipv6");
        exit(EXIT_FAILURE);
    }
    //on indique qu'on veut utiliser le socket pour plusieurs adresses (non-obligatoire)
    int opt = true;
    if( setsockopt(socket_annonces_ipv6, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 )
    {
        perror("erreur setsockopt");
        exit(EXIT_FAILURE);
    }
    //init des infos sur notre adresse
    my_addr_annonces_ipv6.sin6_family = AF_INET6; //IPV6
    my_addr_annonces_ipv6.sin6_port = htons(port_udp); //port
    my_addr_annonces_ipv6.sin6_addr = in6addr_any;
    my_addr_annonces_ipv6.sin6_flowinfo = 0;
    my_addr_annonces_ipv6.sin6_scope_id = 0;

    addrlen_ipv6 = sizeof(struct sockaddr_in6); //on récupère la taille de l'adr
    memset(buf_socket_annonces_ipv6, '\0', 1024); //on prépare le buffer
    //print_debug("Taille du buff = %d\n", (int)strlen(buf_socket_annonces_ipv4));

    //liaison entre le socket et notre adresse
    if (bind(socket_annonces_ipv6, (struct sockaddr *)&my_addr_annonces_ipv6, addrlen_ipv6) == -1){
        perror("bind socket reception annonce");
        close(socket_annonces_ipv6);
        exit(EXIT_FAILURE);
    }

    //récupération du port 
    if (getsockname(socket_annonces_ipv6, (struct sockaddr *)&my_addr_annonces_ipv6, &addrlen_ipv6) == -1){
        perror("getsockname");
        close(socket_annonces_ipv6);
        exit(EXIT_FAILURE);
    }

    print_debug("Port de reception des annonces IPV6 : %d\n", ntohs(my_addr_annonces_ipv6.sin6_port));
}


int main(int argc, char **argv){

    

    //vérif bons arguments
    if (argc != 3){
        printf("Usage: %s local_port debug\n", argv[0]);
        exit(-1);
    }

    DEBUG = atoi(argv[2]);

    /* on initialise les slots des noeuds ipv4 */
    for(int i = 0; i < 100; i++){
        noeuds_ipv4[i].operation = malloc(sizeof(char)* OP_SIZE); 
        noeuds_ipv4[i].status = UNDEFINED_SLOT;
    }
    /* on initialise les slots des noeuds ipv6 */
    for(int i = 0; i < 100; i++){
        noeuds_ipv6[i].operation = malloc(sizeof(char)* OP_SIZE); 
        noeuds_ipv6[i].status = UNDEFINED_SLOT;
    }

    /* création du socket UDP ipv4 de reception des annonces */
    init_socket_annonces_ipv4(atoi(argv[1]));
    /* création du socket UDP ipv6 de reception des annonces */
    init_socket_annonces_ipv6(atoi(argv[1])+1);

    /* création des threads */

    const int nb_threads = 4; //nb de threads crées
	pthread_t thread_ids[nb_threads]; //tableau des id de tous les threads
    int err; //retour de pthread_create = code d'erreur eventuel

    //lancement du thread qui receptionne les annonces en ipv4
    print_info("main : Lancement du thread C de reception des annonces ipv4.\n");
    err = pthread_create(&(thread_ids[1]), NULL, thread_get_annonce_ipv4, NULL);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_get_annonce_ipv4");
        exit(EXIT_FAILURE);
    }
    //lancement du thread qui receptionne les annonces en ipv6
    print_info("main : Lancement du thread F de reception des annonces ipv6.\n");
    err = pthread_create(&(thread_ids[3]), NULL, thread_get_annonce_ipv6, NULL);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_get_annonce_ipv6");
        exit(EXIT_FAILURE);
    }

    //lancement du thread qui check les timeout des noeuds
     print_info("main : Lancement du thread D de check des timeout des noeuds.\n");
    err = pthread_create(&(thread_ids[2]), NULL, thread_timeout_noeud, NULL);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_timeout_noeud");
        exit(EXIT_FAILURE);
    }

    //lancement du thread qui écoute sur stdin
    print_info("main : Lancement du thread A de lecture de commandes sur stdin.\n");
	err = pthread_create(&(thread_ids[0]), NULL, thread_read_stdin, NULL);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_read_stdin");
        exit(EXIT_FAILURE);
    }

    /* attente de la fin de tous les threads */
    for(int i = 0; i < nb_threads; i++){
		//printf("Attente de la fin du thread n° %d\n", i);
		if((err = pthread_join(thread_ids[i], NULL)) != 0){
            errno = err;
            perror("pthred_join\n");	
		}
	}

    printf("fin prog\n");
    return EXIT_SUCCESS;
}