/** @file utils.h
 */
#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdarg.h>

///booléen de type int
#define bool int
///valeur vrai du booléen
#define true 1
///valeur fausse 
#define false 0

///booléen indiquant s'il faut afficher les messages de déboggage
bool DEBUG; 

void update_random_interval(int min, int max);

void reset_random_interval();

int getRandValue();

void print_error(char* msg, bool checkErrno);

void print_debug(const char* msg, ...);

void print_info(const char* msg, ...);

bool charIs(char c, int nb_char, ...);

int absValue(int n);

int maxValue(int a, int b);

void getRandChar(char**);

char * get_current_hour();

#endif //UTILS_H