/** @file noeud_division.h
*/
#ifndef NOEUD_DIVISION_H
#define NOEUD_DIVISION_H

const char *OPERATION = "/";

double fct_calcul_noeud(double * params, int nb_param);


#include "noeud_ipv6.h"

#endif //NOEUD_DIVISION_H