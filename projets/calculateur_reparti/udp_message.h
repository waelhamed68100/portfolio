/** @file udp_message.h
*/
#ifndef UDP_MESSAGE_H
#define UDP_MESSAGE_H

#include <stdint.h> //uint16_t
#include <stdio.h> //printf
#include <stdlib.h> //malloc
#include <string.h> //memcpy

#include "utils.h"

///Taille de la chaîne u_annonce_noeud.operation
#define OP_SIZE 20

///Taille de la chaîne u_udp_message.message_type
#define MSG_SIZE 100 
///Type de message : annonce à l'orchestrateur par le noeud
#define MSG_ANNONCE "hi" 
///Type de message : requete de calcul au noeud par l'orchestrateur
#define MSG_REQUETE_CALCUL "calcul" 



///Type de variable des paramètres de calcul
#define TYPEOF_PARAMS double   
///Type de retour de la fonction de calcul
#define TYPEOF_CALC_RES double

//flags d'états des noeud de calcul

///FLAG : indique que le noeud du slot est indéfini
#define UNDEFINED_SLOT -666
///FLAG : le noeud est en cours de calcul
#define NOEUD_CALCUL 26 
///FLAG : le noeud est libre et peut recevoir une requete de calcul
#define NOEUD_LIBRE 27 

///Structure englobant tous les messages echangés entre orchestrateur <-> noeuds
typedef struct u_udp_message{
    ///Taille totale des données de cette struct une fois sérialisée, en octets
    unsigned int total_size; 
    ///@brief Type de message (voir #define MSG_*). 
    ///(taille constante de MSG_SIZE)
    char *message_type; 
    ///Charge utile du message, sérialisée
    void *payload;
}  *udp_message;

///Structure décrivant une annonce du noeud à orchestrateur
typedef struct u_annonce_noeud{
    ///Taille totale des données de cette struct une fois sérialisée, en octets
    unsigned int total_size;
    ///@brief Type de l'operation du noeud de calcul.
    ///(taille constante de OP_SIZE)
    char *operation; 
    ///Port du socket UDP d'écoute du noeud
    uint16_t port; 
    ///Nombre de paramètres requis pour le calcul
    uint16_t nb_param;
    ///FLAG d'état du noeud
    int status;
}  *annonce_noeud;

///Structure décrivant une requete de calcul envoyée echangée entre l'orchestrateur et un noeud
typedef struct u_requete_calcul {
    ///Taille totale des données de cette struct une fois sérialisée, en octets
    unsigned int total_size;
    ///Port du socket UDP de l'orchestrateur 
    uint16_t port; 
    ///Nombre de paramètres requis pour le calcul
    uint16_t nb_param;
    ///Chaîne des paramètres de la fonction de calcul 
    TYPEOF_PARAMS *params;
    ///Valeur de retour de la fonction de calcul
    TYPEOF_CALC_RES result;
} * requete_calcul;

/* sérialisation */

void * serialize_udp_message(udp_message udp_msg, int* total_size);

void * serialize_annonce_noeud(annonce_noeud moi, int* total_size);

void * serialize_requete_calcul(requete_calcul requete, int* total_size);

/* désérialisation*/

udp_message deserialize_udp_message(void *buf);

annonce_noeud deserialize_annonce_noeud(void *buf);

requete_calcul deserialize_requete_calcul(void *buf);

#endif //UDP_MESSAGE_H



