/** @file noeud_multiplication.c
 * @brief contient le prog du noeud ipv4 de calcul addition
 */
#include "noeud_multiplication.h"

/** @brief Fonction qui effectue le calcul
 * @param params Tableau des paramètres en entrée necessaires au calcul
 * @param nb_param Nombre d'éléments du tableau params
 */
double fct_calcul_noeud(double * params, int nb_param){
    (void)nb_param;
    int min = 2;
    int max = 10;
    srand(time(NULL)); 
    int temps_attente = (int)(min + (rand() % (max + 1 - min)));
    print_info("La fonction du noeud multiplication prendra %d secondes à calculer...\n", temps_attente);
    sleep(temps_attente);
    //sleep(1);(void)temps_attente;
    
    return params[1] * params[0];
}



