/** @file noeud_ipv4.h
*/
#ifndef NOEUD_IPV4_H
#define NOEUD_IPV4_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "udp_message.h"


///Période d'attente entre chaque annonce
#define PERIODE_ANNONCE 15

///état du noeud : < UNDEFINED_SLOT || NOEUD_CALCUL || NOEUD_LIBRE >
int status;

///Descripteur du socket UDP de communication avec l'orchestrateur
int sockfd; 
///Buffer pour acceuillir les données en transit sur le socket UDP
char buf[1024];   
///Adresse du socket de ce noeud 
struct sockaddr_in my_addr; 
///Adresse du socket de reception d'annonce de l'orchestrateur
struct sockaddr_in addr_annonce;
///taille de la struct sockaddr_in
socklen_t sockaddr_in_len; 

void * thread_annonce_orchestrateur(void * osef);

void * thread_attente_requete(void *osef);

extern TYPEOF_CALC_RES fct_calcul_noeud(TYPEOF_PARAMS * params, int nb_param);

extern const char * OPERATION;

#endif //NOEUD_IPV4_H