/** @file orchestrateur.h
*/
#ifndef ORCHESTRATEUR_H
#define ORCHESTRATEUR_H

#include "utils.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h> //pthread

#include "udp_message.h"


///Taille du buffer conntenant les données udp ipv4
#define UDP_IPv4_PAYLOAD_SIZE 1024
///Taille du buffer conntenant les données udp ipv6
#define UDP_IPv6_PAYLOAD_SIZE 1024
///Timeout (en secondes) avant la suppression d'un noeud down
#define TIMEOUT_NOEUD 30

/* IPV4 */

///Fd du socket IPv4 UDP de reception des annonces 
int socket_annonces_ipv4; 
///Buffer contenant les données recu par le socket IPv4 UDP de reception des annonces
char buf_socket_annonces_ipv4[UDP_IPv4_PAYLOAD_SIZE]; 
///adr. ip + port du socket ipv4 
struct sockaddr_in my_addr_annonces_ipv4; 
///Taille de l'adresse my_addr_ipv4
socklen_t addrlen_ipv4; 

/* IPV6 */

///Fd du socket IPv6 UDP de reception des annonces 
int socket_annonces_ipv6; 
///Buffer contenant les données recu par le socket IPv6 UDP de reception des annonces
char buf_socket_annonces_ipv6[UDP_IPv6_PAYLOAD_SIZE]; 
///adr. ip + port du socket ipv6 
struct sockaddr_in6 my_addr_annonces_ipv6; 
///Taille de l'adresse my_addr_ipv6
socklen_t addrlen_ipv6; 

//flags de protocole

///FLAG : protocole ipv4
#define IPV4 444
///FLAG : protocole ipv6
#define IPV6 666

///Structure décrivant un noeud de calcul ipv4
typedef struct u_info_noeud_ipv4{
    ///Opération de calcul du noeud
    char *operation; 
    ///Nombre de paramètres de la fonction de calcul
    int nb_param;
    ///Adresse ip du noeud
    in_addr_t adresse_ip;
    ///Port du noeud
    uint16_t port;
    ///Status (état) du noeud
    int status; //NOEUD_CALCUL, NOEUD_LIBRE
    ///timestamp du dernier message reçu par le noeud 
    time_t date_dernier_msg;
} info_noeud_ipv4;

///Structure décrivant un noeud de calcul ipv6
typedef struct u_info_noeud_ipv6{
    ///Opération de calcul du noeud
    char *operation; 
    ///Nombre de paramètres de la fonction de calcul
    int nb_param;
    ///Adresse ip du noeud
    struct in6_addr adresse_ip;
    ///Port du noeud
    uint16_t port;
    ///Status (état) du noeud
    int status; //NOEUD_CALCUL, NOEUD_LIBRE
    ///timestamp du dernier message reçu par le noeud 
    time_t date_dernier_msg;
} info_noeud_ipv6;

///Structure décrivant les données envoyées en paramètre au thread thread_traitement_annonce
typedef struct u_param_thread_traitement_annonce {
    ///Protocole utilisé
    int protocole;
    ///Adresse ipv4 du noeud (si ipv4)
    in_addr_t adresse_ipv4;
    ///Adresse ipv6 du noeud (si ipv6)
    struct in6_addr adresse_ipv6;
    ///Buffer des données envoyées dans la requete UDP
    void * udp_msg_buf;
} * param_thread_traitement_annonce;

///Structure décrivant les données envoyées en paramètre au thread thread_requete_calcul_ipv4
typedef struct u_param_thread_requete_calcul_ipv4 {
    ///Noeud ipv4 qui doit effectuer le calcul
    info_noeud_ipv4 noeud_ipv4;
    ///Paramètres d'entrée pour la fonction de calcul du noeud
    TYPEOF_PARAMS *params_fct_calcul;
    ///la commande que l'utilisateur a entrée
    char* commande;
} * param_thread_requete_calcul_ipv4;

///Structure décrivant les données envoyées en paramètre au thread thread_requete_calcul_ipv6
typedef struct u_param_thread_requete_calcul_ipv6 {
    ///Noeud ipv4 qui doit effectuer le calcul
    info_noeud_ipv6 noeud_ipv6;
    ///Paramètres d'entrée pour la fonction de calcul du noeud
    TYPEOF_PARAMS *params_fct_calcul;
    ///la commande que l'utilisateur a entrée
    char* commande;
} * param_thread_requete_calcul_ipv6;

///Tableau contenant tous les noeuds ipv4 enregistrés
info_noeud_ipv4 noeuds_ipv4[100];
///Tableau contenant tous les noeuds ipv4 enregistrés
info_noeud_ipv6 noeuds_ipv6[100];

///Mutex noeuds_ipv4
static pthread_mutex_t mutex_noeuds_ipv4 = PTHREAD_MUTEX_INITIALIZER;
///Mutex noeuds_ipv6
static pthread_mutex_t mutex_noeuds_ipv6 = PTHREAD_MUTEX_INITIALIZER;

//
void * thread_read_stdin(void * osef);

void * thread_requete_calcul_ipv4(void * param);
//
void * thread_requete_calcul_ipv6(void * param);
//
void * thread_traitement_annonce(void * udp_msg_buf);

void * thread_get_annonce_ipv4(void * osef);
//
void * thread_get_annonce_ipv6(void * osef);

int get_index_noeud_ipv4(in_addr_t adr_ip, uint16_t port);
//
int get_index_noeud_ipv6(struct in6_addr adr_ip, uint16_t port);

int ajouter_noeud_ipv4(char* op, in_addr_t adr_ip, uint16_t port, uint16_t nb_param);
//
int ajouter_noeud_ipv6(char* op, struct in6_addr adr_ip, uint16_t port, uint16_t nb_param);

void * thread_timeout_noeud(void *osef);

void init_socket_annonces_ipv4(int port_udp);
//
void init_socket_annonces_ipv6(int port_udp); 

#endif //ORCHESTRATEUR_H