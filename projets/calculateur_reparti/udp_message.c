/** @file udp_message.c
 * @brief contient les fonctions sur les messages echangés entre les programmes
 */
#include "udp_message.h"

/** @brief Sérialise un pointeur vers une instance de u_annonce_noeud
 * @param buf Pointeur vers le buffer contenant l'annonce à déconstruire
 * @param total_size Pointeur vers un entier dans lequel sera stocké la taille de la structure sérialisée
*/
void * serialize_annonce_noeud(annonce_noeud annonce, int* total_size){
    /* on détermine la taille ue fois sérialisée */
    annonce->total_size = sizeof(annonce->total_size) + OP_SIZE + sizeof(annonce->port) 
        +  sizeof(annonce->nb_param) + sizeof(annonce->status);
    if(total_size != NULL){
        *total_size = annonce->total_size; 
    }
    /* sérialisation */
    print_debug("Sérial. annonce : sérialization de l'annonce.\n");
    int decalage = 0;
    print_debug("Sérial. annonce : Taille totale de l'annonce_noeud à sérializer : %d\n",annonce->total_size);
    void *buff = malloc(annonce->total_size * sizeof(char));
    //1er membre : total size
    memcpy(buff, &(annonce->total_size), sizeof(annonce->total_size));
    //2ème membre : operation
    decalage += sizeof(annonce->total_size);
    memcpy(buff + decalage, annonce->operation, OP_SIZE);
    //3ème membre : port
    decalage += OP_SIZE;
    memcpy(buff + decalage, &(annonce->port),sizeof(annonce->port));
    //4ème membre : nb_param
    decalage += sizeof(annonce->port);
    memcpy(buff + decalage, &(annonce->nb_param),sizeof(annonce->nb_param));
    //5ème membre : status
    decalage += sizeof(annonce->nb_param);
    memcpy(buff + decalage, &(annonce->status),sizeof(annonce->status));
    /* free car plus besoin de l'instance */
    free(annonce);
    return buff;
}

/** @brief Sérialise un pointeur vers une instance de u_requete_calcul
 * @param buf Pointeur vers le buffer contenant la requête à déconstruire
 * @param total_size Pointeur vers un entier dans lequel sera stocké la taille de la structure sérialisée
*/
void * serialize_requete_calcul(requete_calcul requete, int* total_size){
    /* on détermine la taille ue fois sérialisée */
    requete->total_size = sizeof(requete->total_size) + sizeof(requete->port) 
        + sizeof(requete->nb_param) + sizeof(TYPEOF_PARAMS) * requete->nb_param
        + sizeof(requete->result);
    if(total_size != NULL){
        *total_size = requete->total_size; 
    }
    /* sérialisation */
    print_debug("Sérial. requete : sérialization de la requete.\n");
    int decalage = 0;
    print_debug("Sérial. requete : Taille totale de la u_requete_calcul à sérializer : %d\n",requete->total_size);
    void *buff = malloc(requete->total_size * sizeof(char));
    //1er membre : total size
    memcpy(buff, &(requete->total_size), sizeof(requete->total_size));
    //2ème membre : port
    decalage += sizeof(requete->total_size);
    memcpy(buff + decalage, &(requete->port),sizeof(requete->port));
    //3ème membre : nb_param
    decalage += sizeof(requete->port);
    memcpy(buff + decalage, &(requete->nb_param),sizeof(requete->nb_param));
    //4ème membre : params
    decalage += sizeof(requete->nb_param);
    memcpy(buff + decalage, requete->params, sizeof(TYPEOF_PARAMS) * requete->nb_param);
    //5ème membre : result
    decalage += sizeof(TYPEOF_PARAMS) * requete->nb_param;
    memcpy(buff + decalage, &(requete->result), sizeof(requete->result));
    
    /* free car plus besoin de l'instance */
    free(requete->params);
    free(requete);
    return buff;
}


/** @brief Sérialise un pointeur vers une instance de u_udp_message
 * @param buf Pointeur vers le buffer contenant le message udp à déconstruire
 * @param total_size Pointeur vers un entier dans lequel sera stocké la taille de la structure sérialisée
 */
void * serialize_udp_message(udp_message udp_msg, int* total_size){
    /* sérialisation du payload */
    void * payload_buf; //buf qui contiendra le payload sérialisé
    int taille_payload_buf; //taille du buf du payload
    //on sérialize en fonction du type du payload
    //cas annonce
    if(strcmp(udp_msg->message_type, MSG_ANNONCE) == 0 ){
        payload_buf = serialize_annonce_noeud(
            (annonce_noeud)udp_msg->payload,
            &taille_payload_buf
        ); 
    }
    //cas requete calcul
    else if(strcmp(udp_msg->message_type, MSG_REQUETE_CALCUL) == 0 ){
        payload_buf = serialize_requete_calcul(
            (requete_calcul)udp_msg->payload,
            &taille_payload_buf
        ); 
    }
    else{
        printf("ERREUR serialize_udp_message : type du message udp inconnu (%s ?)\n",udp_msg->message_type);
    }
    /* on détermine la taille de l'udp_message une fois sérialisée */
    print_debug("DEBUG : TAILLE payload->total_size = %d\n",taille_payload_buf);
    udp_msg->total_size =  sizeof(udp_msg->total_size) + MSG_SIZE 
        + taille_payload_buf;
    /* on enregistre la taille dans le pointeur eventuellement donné */
    if(total_size != NULL){
        *total_size = udp_msg->total_size; 
    }
    /* sérialisation */
    print_debug("Sérial. udp_msg : Sérialisation du message udp\n");
    int decalage = 0;
    print_debug("Sérial. udp_msg : Taille totale de l'udp_message à sérializer : %d\n"
        , udp_msg->total_size);
    void * buf = malloc(udp_msg->total_size * sizeof(char) );
    //1er membre : total_size
    memcpy(buf,&(udp_msg->total_size), sizeof(udp_msg->total_size));
    //2eme membre : message_type
    decalage += sizeof(udp_msg->total_size);
    memcpy(buf + decalage, udp_msg->message_type, MSG_SIZE);
    //3eme membre : payload
    decalage += MSG_SIZE;
    //copie du payload sérialisé dans la suite du buffer
    memcpy(
        buf + decalage, 
        payload_buf, 
        udp_msg->total_size - decalage
    );
    /* free */
    free(payload_buf);
    free(udp_msg->message_type);
    free(udp_msg);
    return buf;
}

/** @brief Désérialise un buffer d'un u_udp_message
 * @param buf Pointeur vers le buffer contenant le message udp à reconstruire
 */
udp_message deserialize_udp_message(void *buf){
    int decalage = 0;
    //alloc
    udp_message udp_msg = malloc(sizeof(struct u_udp_message));
    //1er membre : total_size
    unsigned int total_size;
    memcpy(&total_size,buf, sizeof(udp_msg->total_size));
    int * k = buf;
    print_debug("Dé-sérial. udp_msg : taille totale du message recu : %d\n",*k); //total_size);
    udp_msg->total_size = total_size;
    //2eme membre : message_type
    decalage += sizeof(udp_msg->total_size);
    udp_msg->message_type = malloc(sizeof(char)*MSG_SIZE);
    memcpy(udp_msg->message_type, buf + decalage, MSG_SIZE);
    print_debug("Dé-sérial. udp_msg : udp_msg->message_type = %s\n",udp_msg->message_type);
    //3ème membre : payload
    decalage += MSG_SIZE;
    int payload_size = (int)udp_msg->total_size - decalage;
    print_debug("Dé-sérial. udp_msg : taille du payload = %d\n", payload_size);
    //détermination du type du payload
    //cas annonce
    if(strcmp(udp_msg->message_type, MSG_ANNONCE) == 0){
        udp_msg->payload = deserialize_annonce_noeud(buf+decalage);
    }
    //cas requete calcul
    else if(strcmp(udp_msg->message_type, MSG_REQUETE_CALCUL) == 0){
        udp_msg->payload = deserialize_requete_calcul(buf+decalage);
    }
    else{
        printf("ERREUR: type du message udp inconnu (%s ?)\n",udp_msg->message_type);
        exit(EXIT_FAILURE);
    };
    //free
    //free(buf);
    return udp_msg;
}

/** @brief Désérialise un buffer d'un u_annonce_noeud
 * @param buf Pointeur vers le buffer contenant l'annonce à reconstruire
 */
annonce_noeud deserialize_annonce_noeud(void *buf){
    /* alloc. pour la structure déserialisée */
    annonce_noeud noeud = malloc(sizeof(struct u_annonce_noeud));
    /* déserialisation */
    print_debug("Dé-sérial. annonce : deserialization annonce...\n");
    int decalage = 0;
    //1er membre : total_size
    memcpy(&(noeud->total_size),buf, sizeof(noeud->total_size));
    print_debug("Dé-sérial. annonce : noeud->total_size = %d\n", noeud->total_size);
    //2ème membre : operation
    decalage += sizeof(noeud->total_size);
    noeud->operation = malloc(sizeof(char)* OP_SIZE);
    memcpy(noeud->operation, buf + decalage, OP_SIZE);
    //3ème membre : port
    decalage += OP_SIZE;
    memcpy(&(noeud->port),buf + decalage, sizeof(noeud->port));
    //4ème membre : nb_param
    decalage += sizeof(noeud->port);
    memcpy(&(noeud->nb_param),buf + decalage, sizeof(noeud->nb_param));
    //5ème membre : status
    decalage += sizeof(noeud->nb_param);
    memcpy(&(noeud->status),buf + decalage, sizeof(noeud->status));
    
    //free(buf);
    //^ pas besoin de free car le buf passé en param est déjà libéré lors de la désérialization du parent (udp_message)
    return noeud;
}


/** @brief Désérialise un buffer d'un u_requete_calcul
 * @param buf Pointeur vers le buffer contenant la requete à réconstruire
 */
requete_calcul deserialize_requete_calcul(void *buf){
    /* alloc. pour la structure déserialisée */
    requete_calcul requete = malloc(sizeof(struct u_requete_calcul));
    /* déserialisation */
    print_debug("Dé-sérial. requete : deserialization requete...\n");
    int decalage = 0;
    //1er membre : total_size
    memcpy(&(requete->total_size),buf, sizeof(requete->total_size));
    print_debug("Dé-sérial. requete : requete->total_size = %d\n", requete->total_size);
    //2ème membre : port
    decalage += sizeof(requete->total_size);
    memcpy(&(requete->port),buf + decalage, sizeof(requete->port));
    //3ème membre : nb_param
    decalage += sizeof(requete->port);
    memcpy(&(requete->nb_param),buf + decalage, sizeof(requete->nb_param));
    //4ème membre : params
    decalage += sizeof(requete->nb_param);
    int taille_tab_params = sizeof(TYPEOF_PARAMS) * requete->nb_param;
    requete->params = malloc(taille_tab_params);
    memcpy(requete->params, buf + decalage, taille_tab_params);
    //5ème membre : result
    decalage += taille_tab_params;
    memcpy(&(requete->result),buf + decalage, sizeof(requete->result));
    
    //free(buf);
    //^ pas besoin de free car le buf passé en param est déjà libéré lors de la désérialization du parent (udp_message)
    return requete;
}


