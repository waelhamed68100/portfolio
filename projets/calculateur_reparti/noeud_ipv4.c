/** @file noeud_ipv4.c
 * @brief contient le prog du noeud de calcul addition
 */
#include "noeud_ipv4.h"


/** @brief Thread B qui attent une requête de calcul du serveur sur le socket UDP  
 * @param osef Variable inutile mais obligatoire pour respecter la convention
*/
void * thread_attente_requete(void *osef){
    (void)osef;
    /* écoute sur le socket UDP */
    print_info("B : en attente d'une requete de calcul...\n");
    if (recvfrom(sockfd, buf, 1024, 0, (struct sockaddr *)&addr_annonce, 
        &sockaddr_in_len) == -1){
        printf("errno = %d\n", errno);
        perror("err recvfrom");
        exit(EXIT_FAILURE);
    }
    /* désérialisation de la requete */
    print_info("B : requete de calcul reçue. Déserialisation...\n");
    udp_message udp_msg = deserialize_udp_message(buf);
    if(strcmp(udp_msg->message_type, MSG_REQUETE_CALCUL) != 0){
        printf("ERREUR : udp_msg->message_type != %s\n", MSG_REQUETE_CALCUL);
        exit(EXIT_FAILURE);
    }
    requete_calcul requete = (requete_calcul)udp_msg->payload;
    for(int i = 0; i < requete->nb_param; i++){
        print_info("B : Param %d = %f\n", i+1, requete->params[i]);
    }
    print_debug("B : Port de renvoi du résultat : %d\n", requete->port);
    /* calcul */
    status = NOEUD_CALCUL;
    print_info("B : Calcul en cours...\n");
    TYPEOF_CALC_RES res = fct_calcul_noeud(requete->params, requete->nb_param);
    /* renvoi du resultat */
    uint16_t port_resultat = requete->port;
    requete->result = res;
    udp_msg->payload = requete;
    int taille_buf;
    void *buffer = serialize_udp_message(udp_msg, &taille_buf);
    /* renvoi du résultat */
    //print_debug("Taille buffer = %d\n", taille_buf);
    //construction de l'adresse
    struct sockaddr_in adr_reception_res;
    adr_reception_res.sin_family = AF_INET;
    adr_reception_res.sin_addr.s_addr = addr_annonce.sin_addr.s_addr;
    adr_reception_res.sin_port = htons(port_resultat);
    print_info("B : renvoi du résultat du calcul (%f) à l'orchestrateur.\n",res);
    
    if(sendto(sockfd, buffer, taille_buf, 0,
	(struct sockaddr *) &adr_reception_res, sockaddr_in_len) == -1){
        perror("sendto thread_attente_requete");
	    close(sockfd);
	    exit(EXIT_FAILURE);
    }
    status = NOEUD_LIBRE;
    return thread_attente_requete(NULL);
}

/** @brief Thread A qui envoi toutes les PERIODE_ANNONCE sec. une annonce au serveur
 * @param osef Variable inutile mais obligatoire pour respecter la convention
 */
void * thread_annonce_orchestrateur(void * osef){
    (void)osef;
    /* création de l'annonce */
    annonce_noeud annonce = malloc(sizeof(struct u_annonce_noeud));
    annonce->operation = (char*)OPERATION;
    annonce->port = ntohs(my_addr.sin_port);
    annonce->nb_param = 2;
    annonce->status = status;
    /* création du wrapper UDP */ 
    udp_message  udp_msg = malloc(sizeof( struct u_udp_message));
    udp_msg->message_type = malloc(sizeof(char)*MSG_SIZE);
    strcpy(udp_msg->message_type, MSG_ANNONCE);
    udp_msg->payload = annonce;
    /* sérialisation du wrapper */
    int udp_total_size;
    void *buf = serialize_udp_message(udp_msg, &udp_total_size);
    /* envoi du wrapper */
    print_debug("A : envoi d'une annonce au serveur (%d octets)\n", udp_total_size);
    if(sendto(sockfd, buf, udp_total_size, 0,
	(struct sockaddr *) &addr_annonce, sockaddr_in_len) == -1){
        perror("sendto thread_annonce_orchestrateur");
	    close(sockfd);
	    exit(EXIT_FAILURE);
    }
    /* attente */
    sleep(PERIODE_ANNONCE);
    /* envoi nouvelle annonce */
    thread_annonce_orchestrateur(NULL);
    return NULL;
}



int main(int argc, char **argv)
{
    /* vérif. du nb de params */
    if(argc != 4){
        printf("USAGE: %s @dest_ipv4 port_num \n", argv[0]);
        exit(-1);
    }
    DEBUG = atoi(argv[3]);
    print_info("main : protocole utilisé : ipv4\n");
    status = NOEUD_LIBRE;

    /* création du socket UDP */
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("erreur création socket");
	    exit(EXIT_FAILURE);
    }
    //définition de notre adresse
    //init des infos sur notre adresse
    my_addr.sin_family = AF_INET; //type (ipv4 || ipv6)
    my_addr.sin_port = 0;//htons(atoi(argv[1])); //port
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY); //adresse ip

    sockaddr_in_len = sizeof(struct sockaddr_in); //on récupère la taille de l'adr
    memset(buf, '\0', 1024); //on prépare le buffer
    //printf("Taille du buff = %d\n", (int)strlen(buf));

    //définition des infos de l'@ de l'orchestrateur
    addr_annonce.sin_family = AF_INET; //ipv4 || ipv6
    addr_annonce.sin_port   = htons(atoi(argv[2])); //récup du port depuis les params
    sockaddr_in_len         = sizeof(struct sockaddr_in); //définition taille de la struct de l'adr

    //on récupère l'@ de dest depuis les params.
    if(inet_pton(AF_INET, argv[1], &addr_annonce.sin_addr) != 1)
    {
        perror("inet_pton");
	    close(sockfd);
	    exit(EXIT_FAILURE);
    }

    //liaison entre le socket et notre adresse
    if (bind(sockfd, (struct sockaddr *)&my_addr, sockaddr_in_len) == -1){
        perror("bind");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    //récupération du port choisi par l'os
    if (getsockname(sockfd, (struct sockaddr *)&my_addr, &sockaddr_in_len) == -1){
        perror("getsockname");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    uint16_t port = ntohs(my_addr.sin_port);
    print_info("main : Port (choisi par l'os) pour la reception des requetes de l'orchestrateur : %d\n", port);

    /* création des threads */

    const int nb_threads = 2; //nb de threads crées
	pthread_t thread_ids[nb_threads]; //tableau des id de tous les threads
    int err; //retour de pthread_create = code d'erreur eventuel

    //lancement d'envoi des annonces
    print_info("main : Lancement du thread A d'envoi d'annonce périodique.\n");
	err = pthread_create(&(thread_ids[0]), NULL, thread_annonce_orchestrateur, NULL);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_annonce_orchestrateur");
        exit(EXIT_FAILURE);
    }

    //lancement du thread qui receptionne les requêtes
    print_info("main : Lancement du thread B de reception des requêtes.\n");
    err = pthread_create(&(thread_ids[1]), NULL, thread_attente_requete, NULL);
    if(err != 0){
        errno = err;
        perror("pthread_create thread_attente_requete");
        exit(EXIT_FAILURE);
    }

    /* attente de la fin de tous les threads */
    for(int i = 0; i < nb_threads; i++){
		//print_debug("main : Attente de la fin du thread n° %d\n", i);
		if((err = pthread_join(thread_ids[i], NULL)) != 0){
            errno = err;
            perror("pthred_join\n");	
		}
	}

    /* fermeture des sockets */
    close(sockfd);

    printf("fin prog\n");
    return EXIT_SUCCESS;
}


