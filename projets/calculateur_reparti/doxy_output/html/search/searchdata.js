var indexSectionsWithContent =
{
  0: "abcdfgimnoprstu",
  1: "u",
  2: "nou",
  3: "acdfgimprstu",
  4: "abcdmnoprstu",
  5: "aipru",
  6: "bfimnoptu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

