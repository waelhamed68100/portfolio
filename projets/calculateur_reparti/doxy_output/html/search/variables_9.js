var searchData=
[
  ['sockaddr_5fin_5flen',['sockaddr_in_len',['../noeud__ipv4_8h.html#a7226e39152ae80cf59a11eff1865ace9',1,'sockaddr_in_len():&#160;noeud_ipv4.h'],['../noeud__ipv6_8h.html#a7226e39152ae80cf59a11eff1865ace9',1,'sockaddr_in_len():&#160;noeud_ipv6.h']]],
  ['socket_5fannonces_5fipv4',['socket_annonces_ipv4',['../orchestrateur_8h.html#a5998ddd934580beab6f8566563b01103',1,'orchestrateur.h']]],
  ['socket_5fannonces_5fipv6',['socket_annonces_ipv6',['../orchestrateur_8h.html#a9c835d2be3e083d9beb05d2e885da23a',1,'orchestrateur.h']]],
  ['sockfd',['sockfd',['../noeud__ipv4_8h.html#ad2c8fb3df3a737e0685e902870a611d2',1,'sockfd():&#160;noeud_ipv4.h'],['../noeud__ipv6_8h.html#ad2c8fb3df3a737e0685e902870a611d2',1,'sockfd():&#160;noeud_ipv6.h']]],
  ['status',['status',['../structu__info__noeud__ipv4.html#a4e07facc8bdf706ddb8c7bbf090aa00f',1,'u_info_noeud_ipv4::status()'],['../structu__info__noeud__ipv6.html#af04d18e9e1c1edfed08348f8a1975a64',1,'u_info_noeud_ipv6::status()'],['../structu__annonce__noeud.html#a6c394522856e00520646833d51e997b6',1,'u_annonce_noeud::status()'],['../noeud__ipv4_8h.html#a6e27f49150e9a14580fb313cc2777e00',1,'status():&#160;noeud_ipv4.h'],['../noeud__ipv6_8h.html#a6e27f49150e9a14580fb313cc2777e00',1,'status():&#160;noeud_ipv6.h']]]
];
