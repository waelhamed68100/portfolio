var searchData=
[
  ['date_5fdernier_5fmsg',['date_dernier_msg',['../structu__info__noeud__ipv4.html#a4f46b3bd4ec350aeada4e9085f7943ec',1,'u_info_noeud_ipv4::date_dernier_msg()'],['../structu__info__noeud__ipv6.html#a10ae5b40f8203cdf6f1e5e8a495ce5c0',1,'u_info_noeud_ipv6::date_dernier_msg()']]],
  ['debug',['DEBUG',['../utils_8h.html#a48f76a8c0dc23567f303ed6aa221e078',1,'utils.h']]],
  ['deserialize_5fannonce_5fnoeud',['deserialize_annonce_noeud',['../udp__message_8c.html#a963c0751494873e9bed9bee62b27ae2b',1,'deserialize_annonce_noeud(void *buf):&#160;udp_message.c'],['../udp__message_8h.html#a963c0751494873e9bed9bee62b27ae2b',1,'deserialize_annonce_noeud(void *buf):&#160;udp_message.c']]],
  ['deserialize_5frequete_5fcalcul',['deserialize_requete_calcul',['../udp__message_8c.html#a4a12e549246a87a68287387d3602182b',1,'deserialize_requete_calcul(void *buf):&#160;udp_message.c'],['../udp__message_8h.html#a4a12e549246a87a68287387d3602182b',1,'deserialize_requete_calcul(void *buf):&#160;udp_message.c']]],
  ['deserialize_5fudp_5fmessage',['deserialize_udp_message',['../udp__message_8c.html#abe8b65dcf195f15c4ce908426c4a16b5',1,'deserialize_udp_message(void *buf):&#160;udp_message.c'],['../udp__message_8h.html#abe8b65dcf195f15c4ce908426c4a16b5',1,'deserialize_udp_message(void *buf):&#160;udp_message.c']]]
];
