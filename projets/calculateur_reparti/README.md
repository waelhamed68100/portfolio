# Calculateur réparti

Année : 2018

Programme C d'un calculateur réparti. 

Un programme (appelé orchestrateur) orchestre l'envoi de calcul de calculs (comme une simple division, par exemple) à d'autres programmes appelés noeuds de calculs.

A la réception des requêtes de l'orchestrateur, ces noeuds de calculs executent le calcul (temps arbitrairement long de plusieurs secondes) puis envoient le résultat à l'orchestrateur.

Les messages sont échangés par **ipv6** ou **ipv4** (au choix) par le biais de **sockets udp C unix**.

L'orchestrateur utilise des **threads unix** pour empêcher les requêtes d'être bloquantes.

Les données échangées sont sérialisées / désérialisées pour optimiser au meiux la taille des messages.

Si plusieurs noeuds de calculs proposent la même opération, l'orchestrateur enverra le calcul au premier noeud disponible.

L'orchestrateur est constamment à l'écoute de nouveaux noeuds de calculs ajoutés sur le réseau. Lorsqu'un noeud ne témoigne plus de sa présence après un moment trop long (timeout), l'orchestrateur le supprime automatiquement de sa la liste de noeuds de calculs à disposition.

Le document `Projet – Calculateur réparti - Protocle.pdf` décrit le format des données échangées.

## Compilation et Exécution :
---------------------------------

Pour compiler et éxecuter l'orchestrateur : `éxécuter le fichier run_orchestrateur.sh`

Pour compiler et éxecuter un noeud de calcul : `éxécuter le fichier run_noeud_<opération>.sh`,

Par exemple pour éxécuter le programme du noeud d'addition : `./run_noeud_addition.sh`

## Utilisation :
------------------------------
Une fois l'orchestrateur lancé et les noeuds de calculs synchronisés avec lui, il faut entrer une commande de calcul à l'orchestrateur en tapant dans son terminal `<symbole opération>(<paramètre 1>, <paramètre 2>)`

Symboles des opérations : 

| opération      | symbole |
|----------------|:-------:|
| addition       |    +    |
| soustraction   |    -    |
| multiplication |    *    |
| division       |    /    |

Par exemple pour envoyer une requete de division de 7 par 3 il faut taper dans le terminal de l'orchestrateur (après qu'un noeud de division se soit synchronisé) : `/(7,3)`

## Exemple d'exécution :
--------------------------------

(noter les timestamps indiqués)

L'orchestrateur :

    * Execution de orchestrateur: ./orchestrateur 1337 0

    [22:19:30] info : main : Lancement du thread C de reception des annonces ipv4.
    [22:19:30] info : main : Lancement du thread F de reception des annonces ipv6.
    [22:19:30] info : main : Lancement du thread D de check des timeout des noeuds.
    [22:19:30] info : main : Lancement du thread A de lecture de commandes sur stdin.
    [22:19:30] info : A : Attente de commande sur stdin...
    [22:19:30] info : A : orchestrateur>
    [22:19:47] info : B* : annonce d'un nouveau noeud reçue : 
    [22:19:47] info : B* : opération : +
    [22:19:47] info : B* : port : 50561
    [22:19:47] info : B* : nb_param : 2
    [22:19:47] info : B* : status : 27
    [22:19:47] info : B* : adr IPv4 : 127.0.0.1
    +(4,8)
    [22:20:4] info : A : lancement du thread E* d'envoi des requetes de calcul ipv4.
    [22:20:4] info : A : Attente de commande sur stdin...
    [22:20:4] info : A : orchestrateur>
    [22:20:4] info : E* : attente du résultat de la requete de calcul...
    [22:20:10] info : E* : résultat du calcul :
    [22:20:10] info : +(4,8) = 12.000000 par <127.0.0.1:50561>
    [22:20:43] info : B* : annonce d'un nouveau noeud reçue : 
    [22:20:43] info : B* : opération : /
    [22:20:43] info : B* : port : 33330
    [22:20:43] info : B* : nb_param : 2
    [22:20:43] info : B* : status : 27
    [22:20:43] info : B* : adr IPv6 : ::1
    /(4,3)
    [22:20:56] info : A : lancement du thread G* d'envoi des requetes de calcul ipv6.
    [22:20:56] info : A : Attente de commande sur stdin...
    [22:20:56] info : A : orchestrateur>
    [22:20:56] info : G* : attente du résultat de la requete de calcul...
    [22:20:59] info : /(4,3) = 1.333333 par <::1:33330>

    [22:22:04] info : D : Timeout Noeud ipv4 i=0. Suppression.


Un noeud de calcul ipv4 (opération d'addition) : 

    * Execution de noeud_addition: ./noeud_addition 127.0.0.1 1337 0

    [22:19:47] info : main : protocole utilisé : ipv4
    [22:19:47] info : main : Port (choisi par l'os) pour la reception des requetes de l'orchestrateur : 50561
    [22:19:47] info : main : Lancement du thread A d'envoi d'annonce périodique.
    [22:19:47] info : main : Lancement du thread B de reception des requêtes.
    [22:19:47] info : B : en attente d'une requete de calcul...
    [22:20:4] info : B : requete de calcul reçue. Déserialisation...
    [22:20:4] info : B : Param 1 = 4.000000
    [22:20:4] info : B : Param 2 = 8.000000
    [22:20:4] info : B : Calcul en cours...
    [22:20:4] info : La fonction du noeud addition prendra 6 secondes à calculer...
    [22:20:10] info : B : renvoi du résultat du calcul (12.000000) à l'orchestrateur.
    [22:20:10] info : B : en attente d'une requete de calcul...
    < CTRL + C >

Un noeud de calcul ipv6 (opération de division) : 

    * Execution de noeud_division: ./noeud_division ::1 1338 0

    [22:20:43] info : main : protocole utilisé : ipv6
    [22:20:43] info : main : Port (choisi par l'os) pour la reception des requetes de l'orchestrateur : 33330
    [22:20:43] info : main : Lancement du thread A d'envoi d'annonce périodique.
    [22:20:43] info : main : Lancement du thread B de reception des requêtes.
    [22:20:43] info : B : en attente d'une requete de calcul...
    [22:20:56] info : B : requete de calcul reçue. Déserialisation...
    [22:20:56] info : B : Param 1 = 4.000000
    [22:20:56] info : B : Param 2 = 3.000000
    [22:20:56] info : B : Calcul en cours...
    [22:20:56] info : La fonction du noeud division prendra 3 secondes à calculer...
    [22:20:59] info : B : renvoi du résultat du calcul (1.333333) à l'orchestrateur.
    [22:20:59] info : B : en attente d'une requete de calcul...
