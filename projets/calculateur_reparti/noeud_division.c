/** @file noeud_division.c
 * @brief contient le prog du noeud ipv6 de calcul soustraction
 */
#include "noeud_division.h"

/** @brief Fonction qui effectue le calcul
 * @param params Tableau des paramètres en entrée necessaires au calcul
 * @param nb_param Nombre d'éléments du tableau params
 */
double fct_calcul_noeud(double * params, int nb_param){
    (void)nb_param;
    int min = 2;
    int max = 10;
    srand(time(NULL)); 
    int temps_attente = (int)(min + (rand() % (max + 1 - min)));
    print_info("La fonction du noeud division prendra %d secondes à calculer...\n", temps_attente);
    sleep(temps_attente);
    //sleep(1);(void)temps_attente;
    TYPEOF_CALC_RES res = params[0] / params[1];
    return res;
}



