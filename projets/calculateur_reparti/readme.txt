- Auteur : Wael HAMED, L2S5P (projet fait seul)

- lancement : 

    a) orchestrateur :
        _ script ./run_orchestrateur pour la compilation et le lancement avec les paramètres de base
        _ utilisation : ./orchestrateur <port ipv4> <booléen de débuggage (0 ou 1)>
        _ note : le port ipv6 est le <port ipv4> + 1

    b)  noeud_<operation> :
        _ script ./run_noeud_<operation> pour la compilation et le lancement d'un noeud avec les paramètres de base
        _ utilisation : ./noeud_<operation> <adresse de l'orchestrateur> <port de l'orchestrateur> <booléen de débuggage (0 ou 1)>
        _ note : attention au type du noeud. Il peut être en ipv4 ou ipv6 il faut donc faire attention au format de l'adresse ainsi qu'au port de l'orchestrateur.

- notes : 

    * les commentaires du code sont compatibles avec doxygen (le script tente d'ailleurs de générer une doc). J'ai laissé une documentation générée au format html et latex : 
    * pour accéder à la documentation générée il faut ouvrir le fichier ./doxy_output/html/index.html dans un navigateur web.
