/** @file noeud_soustraction.h
*/
#ifndef NOEUD_SOUSTRACTION_H
#define NOEUD_SOUSTRACTION_H

const char *OPERATION = "-";

double fct_calcul_noeud(double * params, int nb_param);


#include "noeud_ipv6.h"

#endif //NOEUD_SOUSTRACTION_H