/** @file utils.c
 * @brief Contient des petites fonctions utilitaires
 */
#include "utils.h"

///borne inférieure de l'interval des entiers aléatoires possibles
int MIN_RAND_VAL_CELL = -100;
///borne supérieure de l'interval des entiers aléatoires possibles
int MAX_RAND_VAL_CELL = 100;


/** @brief Met à jour l'interval de valeurs aléatoires
 * @param min Borne inférieure
 * @param max Borne supérieure
 */
void update_random_interval(int min, int max){
    MIN_RAND_VAL_CELL = min;
    MAX_RAND_VAL_CELL = max;
}

/** @brief Redéfinit l'interval aléatoire de base
 * min = -100
 * max = 100
 */
void reset_random_interval(){
    MIN_RAND_VAL_CELL = -100;
    MAX_RAND_VAL_CELL = 100;
}
/** @brief Renvoit un entier aléatoire appartenant à [MIN_RAND_VAL_CELL, MAX_RAND_VAL_CELL]
*/
int getRandValue(){
    return (int)(MIN_RAND_VAL_CELL + (rand() % (MAX_RAND_VAL_CELL + 1 - MIN_RAND_VAL_CELL)));
}

/** @brief affiche un message d'erreur
 * @param msg La chaine à ajouter en début de message d'erreur
 * @checkErrno booléen indiquant s'il faut aussi renvoyer errno et une description de cette valeur de errno
 */
void print_error(char* msg, bool checkErrno){
    if(checkErrno == true){
        printf("ERROR: %s : Errno %d : %s\n",msg, errno, strerror(errno));
    }
    else{
        printf( "ERROR: %s \n",msg);
    }
}
/** @brief affiche un message de déboggage si DEBUG est vrai, sinon ne fait rien
 * @param S'utilise comme printf
 * */
void print_debug(const char* msg, ...){
    if(DEBUG == true){
        char * heure = get_current_hour();
        printf("[%s] DEBUG : ", heure);
        free(heure);
        fflush(stdout);
        va_list argptr;
        va_start(argptr, msg);
        vfprintf(stderr, msg, argptr);
        fflush(stdout);
        va_end(argptr);
    }
}

/** @brief affiche un message d'info
 * @param S'utilise comme printf
 * */
void print_info(const char* msg, ...){

        char * heure = get_current_hour();
        printf("[%s] info : ", heure);
        free(heure);
        fflush(stdout);
        va_list argptr;
        va_start(argptr, msg);
        vfprintf(stderr, msg, argptr);
        fflush(stdout);
        va_end(argptr);
    
}
/** @brief renvoit 1 (vrai) si c est un des autres caractères donnés en paramètres, 0 (faux) sinon.
 *  @param c caractère à comparer à l'ensemble de caractères C
 *  @param nb_char Nombre de caractères de l'ensemble C
 *  @param ... ensemble C : suite de caractères (char) séparés par une virgule
 */
bool charIs(char c, int nb_char, ...){
    va_list ap;
    va_start(ap, nb_char);
    char car;
    while(nb_char > 0){
        car = (char)va_arg(ap, int);
        if(c == car){
            return true;
        }
        nb_char--;
    }
    return false;
}

/** @brief Renvoit la valeur absolue de n */
int absValue(int n){
    return n >= 0 ? n : -n;
}

/** @brief Renvoit la plus grande valeur des 2 variables a et b */
int maxValue(int a, int b){
    return a > b ? a : b;
}

/** @brief Renmplit une chaine avec un caractère aléatoire
 * Compris dans [0,9] U [A,Z] U [a,z]
 */
void getRandChar(char** buffer){
    if(*buffer != NULL) free((*buffer));
    (*buffer) = (char*)malloc(sizeof(char)*2);

    int r = rand();
    char c;
    if(r%3 == 0){
        update_random_interval(48,57); //chiffre aléatoire
    }
    if(r%3 == 1){
        update_random_interval(65,90); //Majuscule aléatoire
    }
    if(r%3 == 2){
        update_random_interval(97,122); //minuscule aléatoire
    }
    c = (char)getRandValue();
    (*buffer)[0] = c;
    (*buffer)[1] = '\0';
    reset_random_interval();
}

/** @brief Renvoie l'heure courrante sous forme de chaîne hh:mm:ss (attention à bien penser à librérer cette chaîne)
 */
char * get_current_hour(){
    time_t t = time(NULL);
    struct tm *tmp = localtime(&t);
    char * s = malloc(sizeof(char) * 100);
    //tmp->tm_hour;
   
    sprintf(s,"%d:%d:%d", tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
    return s;
}

