# Petite application de chat

Année : 2018.

Simple application c++ avec interface graphique (Qt/qml) permettant de créer un serveur de chat local (websocket) et de communiquer sur celui-ci.

On peut créer une salle ou se connecter à une salle déjà crée, puis échanger des messages. Les cas de salle pleine et de déconnexions sont gérés.

## Dépendences
---

* Qt 5.11.2 (version seulement à titre indicatif, une version légèrement antérieur peut peut-être aussi convenir)
* Modules de Qt : quick, websockets

## Compilation et Exécution
---

* remplacer la variable `CHEMIN_QTQML` du fichier run.sh par le chemin absolu contenant les outils executables de Qt.
* éxecuter le fichier `run.sh`


## Screenshots
---

tentative connection salle pleine :

![screenshot introuvable](./screenshots/chatroom_full.png "tentative connection salle pleine")

un utilisateur (hors host) se déconnecte :

![screenshot introuvable](./screenshots/host_disconnect.png "utilisateur (hors host) se déconnecte")

le host se déconnecte :

![screenshot introuvable](./screenshots/message+disconnection.png "host se déconnecte")
