#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QDebug>
#include <QtWebSockets>

#define REQ_SEPARATOR " #&# "

class Backend : public QObject
{
    Q_OBJECT
    //property c++ <-> qml : nombre de joueur max qui peuvent se connecter
    Q_PROPERTY(int nb_joueurs_max READ nb_joueurs_max NOTIFY nb_joueurs_maxChanged)
    //property c++ <-> qml : nimbre de joeuurs connectés au lobby
    Q_PROPERTY(int nb_joueurs_co READ nb_joueurs_co NOTIFY nb_joueurs_coChanged)

public:
    //constructeur
    explicit Backend(QObject *parent = nullptr);

//signaux : liaison avec le qml ;) 
//De ce que j'ai compris, c++ -> qml
//note : c'est plus clean de le faire depuis le c++,
//et garder cette convention permet d'éviter les erreurs
signals:
    //lorsqu'on change de page depuis le backend (c++)
    void pageChanged(QString page);
    //lorsqu'un serveur est crée
    void serverListening(QString url);
    //lorsque le nombre de joueurs possible change
    void nb_joueurs_maxChanged();
    //lorsque le nombre de jouerus connectés change
    void nb_joueurs_coChanged();
    //lorsqu'un nouveau joueur se connecte à la partie
    void newPlayerConnected(QString identifier);
    //lorsqu'un nouveau joueur se déconnecte à la partie
    void playerDisconnected(QString identifier);
    //lorsque le serveur s'est déconnecté
    void serverDisconnection();
    //lorsque le lobby est plein
    void serverFull();
    //lorsque l'on recoit un message de chat
    void chatMessage(QString msg);

//qml -> c++
public slots: 
    //qml qui demande au c++ de changer la page
    void changePage(QString page);
    //qml qui demande de créer un serveur
    void createServer(int nb_joueurs_max, QString nickname);
    //renvoit le nombre de joueurs max
    int nb_joueurs_max() const {
        return m_nb_joueurs_max;
    }
    //renvoit le nombre de joueurs connectés au lobby
    int nb_joueurs_co() const {
        return m_nb_joueurs_co;
    }
    //un utilisateur se connecte à un serveur
    void connectToServer(QString url, QString nickname);
    //l'utilisateur envoit un message de chat
    void sendChatMsg(QString msg);
    

//events/functions  c++ -> c++
private slots: 
    //lorsqu'un nouveau client se connecte
    void onNewConnection();
    //lorsqu'un client envoi unmessage
    void serverProcessMessage(const QString &message);
    //lorsque le client se connecte à un serveur
    void onClientConnected();

    /* pas event : */
    //C : fonction appellée par le client lorsqu'il veut se connecter au lobby (envoyer son pseudo)
    void connectToLobby();
    //S : renvoit les infos du lobby au client
    void sendLobby(QWebSocket * client);
    //C : client recoit un message 
    void clientProcessMessage(const QString &message);
    //C : fonction qui est appellée lorsqu'on recoit les infos du lobby par le serveur
    void connectedToLobby(QString infos_lobby);
    //S : ajoute un client qui vient de se connecter au lobby à la liste des clients connectés au lobby
    void addToLobby(QWebSocket * client, QString nickname);
    //C : lorsqu'un nouveau joueur se connecte au lobby
    void newClientConnected(QString nickname);
    //S : lorsqu'un client se déconnecte du serveur
    void clientDisconnected_S();
    //C : lorsque la connection au serveur a été interrompu
    void serverDisconnected();
    //S : lorsque le serveur recoit un message de chat il le renvoit à tous les autres clients
    void serveChatMsg(QString msg);

private:
    //S : QWebSocketServer du serveur (défini si rôle du host)
    QWebSocketServer *m_pWebSocketServer = NULL;
    //C : QWebSocket du client (défini si rôle du client)
    QWebSocket *m_pWebSocketClient = NULL;
    //S : liste des clients connectés
    QList<QWebSocket *> m_clients;
    //N : nombre de joueurs max (host non inclu)
    int m_nb_joueurs_max = -1;
    //N : nombre de joueurs connectés
    int m_nb_joueurs_co = 1;
    //N : pseudo de l'utilisateur
    QString m_nickname;
    //S : liste des pseudos pour le serveur (associés aux sockets)
    std::map<QWebSocket*, QString> m_nicknames;
    //C : liste des pseudos pour le client
    QStringList m_client_nicknames;
};

#endif // BACKEND_H
