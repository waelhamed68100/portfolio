import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import io.qt.Backend 1.0
import QtQuick.Dialogs 1.3

Window {
    id: mainWindow
    visible: true
    width: 700
    minimumWidth: 500
    height: 450
    minimumHeight: 200
    title: "Chat"
    color: "#CED0D4"


    //boite de dialogue
    MessageDialog {
        id: msgDialog
        //seul le bouton ok est visible (TODO : ne fonctionne pas, à fix)
        standardButtons : StandardButton.Ok
        title: "NON-DEFINI"
        text: "NON-DEFINI"
        //lorsque l'utilisateur a cliqué sur le bouton
        onAccepted: {
            ta_lobby.text = "";
            //on retourne à la page d'accueil
            view.currentPage = "home"
        }
        //Component.onCompleted: visible = true
    }

    //fenetre de chat
    Window {
        id: chatWindow
        //invisible de base, s'affiche une fois connecté au lobby
        visible : false
        width: 300
        minimumWidth: 50
        height: 400
        minimumHeight: 200
        title: "Chat"
        color: "yellow"
        flags: Qt.Window | Qt.WindowTitleHint | Qt.CustomizeWindowHint
    
        Rectangle {
            width : parent.width
            height : parent.height 
            

            Rectangle {
                width : parent.width
                height : parent.height - txt_input_chat.height
                anchors.margins : 10 //TODO : fix
                color : "black"
                ScrollView {
                    anchors.fill : parent
                    clip: true 
                    ScrollBar.vertical.policy: ScrollBar.AlwaysOn

                    TextArea {
                        id: ta_chatMsgs
                        verticalAlignment : TextEdit.AlignTop
                        readOnly: true
                        selectByMouse : true
                        font.pixelSize: 14
                        text : ""
                        color : "white"
                    }
                }
            }
            
            Rectangle {
                height : 30
                width : parent.width
                anchors.bottom : parent.bottom
                color : "light grey"
                MouseArea {
                    anchors.fill: parent
                    onClicked: { txt_input_chat.focus = true; }
                }

                RowLayout {
                    Label { text : "Message: " }
                    //input du chat (pour écrire un msg)
                    TextInput {
                        id: txt_input_chat
                        //width : parent.width //- k.width
                        Layout.fillWidth : true
                        readOnly: false //à mettre en true
                        
                        text : ""
                        onAccepted : {
                            console.log("envoi d'un message au chat")
                            backend.sendChatMsg(txt_input_chat.text);
                            txt_input_chat.text = "";
                        }
                    }
                }
                
            }
            
        }
    }

    //permet de gérer plusieurs pages
    SwipeView {
        id : view
        //indique la page à charger
        property string currentPage : "home"

        //le code c++ (liaison bi-directionnelle)
        Backend {
            id: backend

            /**
            * on* = events : fonction appellée par le backend c++ !
            */

            //lorsque la page est changée
            onPageChanged : {
                //change la page courrante
                view.currentPage = page;
                btn_connectionToServer.enabled = true;
                if(page === "home"){ 
                    mainWindow.title = "Chat"; 
                    ta_chatMsgs.text = ""; 
                    chatWindow.close();// chatWindow.hide();//TODO : fix la fenetre qui ne veut pas se fermer
                }
            }
            //lorsqu'un serveur est crée
            onServerListening : {
                txt_server_addr.text = url;
                txt_server_addr.url = url;
            }
            //lorsqu'une nouvelle personne se connecte
            onNewPlayerConnected : {
                ta_lobby.text = ta_lobby.text + identifier + ' CONNECTED\n';
                chatWindow.visible = true;
                
            }
            //lorsqu'un nouveau joueur se déconnecte à la partie
            onPlayerDisconnected : {
                ta_lobby.text = ta_lobby.text + identifier + ' DISCONNECTED\n';
            }
            //lorsque le serveur s'est déconnecté
            onServerDisconnection : {
                mainWindow.title = "Chat";
                msgDialog.icon = StandardIcon.Critical;
                msgDialog.title = "Erreur de connection";
                msgDialog.text = "Le serveur a coupé la connection";
                msgDialog.open();
            }
            //lorsque le lobby est plein
            onServerFull : {
                mainWindow.title = "Chat";
                msgDialog.icon = StandardIcon.Warning;
                msgDialog.title = "Déconnexion";
                msgDialog.text = "Impossible de rejoindre la partie : le nombre de joueurs max est atteint";
                msgDialog.open();
            }
            
            //lorsque l'on recoit un message de chat
            onChatMessage : {
                ta_chatMsgs.text = ta_chatMsgs.text + msg + '\n';
            }
            
            
        }

        /**
        * Chaque Item représente une page différente
        */

        //page d'acceuil
        Item {
            //n'est visible que si la page correspond à celle indiquée dans le swipeview
            visible: view.currentPage === "home"
            
            //affichage des elements côte-à-côte
            RowLayout {
                //bouton pour afficher la page de création d'un serveur
                BetterButton {
                    //id: btn_show_create_lobby
                    text : "Créer un serveur"
                    onClicked: {
                        backend.changePage("create_lobby");
                    }
                }
                //bouton pour rejoindre un serveur
                BetterButton {
                    id: btn_join_lobby
                    text : "Rejoindre un serveur"
                     onClicked: {
                        backend.changePage("join_lobby");
                    }
                }
            }
        }

        //page pour créer un serveur
        Item {
            visible: view.currentPage === "create_lobby"
            //affiche le formulaire de création du serveur
            ColumnLayout {
                //ligne demandant le pseudo
                RowLayout {
                    Label {
                        text: "Pseudo : "
                    }
                    //input du nombre de joueurs voulu
                    TextInput {
                        id: txt_nickname
                        readOnly: false //à mettre en true
                        text: "ChuckNorris"
                    }
                }
                //ligne demandant le nombre de joueur
                RowLayout {
                    Label {
                        text: "Nombre de joueur : "
                    }
                    //input du nombre de joueurs voulu
                    TextInput {
                        id: txt_nb_players
                        readOnly: false //à mettre en true
                        text: "3"
                        inputMethodHints: Qt.ImhDigitsOnly
                        validator: IntValidator {
                            bottom: 1
                            top: 100
                        }
                    }
                    
                }
                //bouton pour lancer la création
                BetterButton {
                    //id: btn_create_lobby
                    text : "Lancer le serveur"
                    onClicked: {
                        //backend.changePage("show_lobby");
                        backend.createServer(parseInt(txt_nb_players.text),txt_nickname.text);
                        this.enabled = false;
                        chatWindow.title = "Chat ("+txt_nickname.text+")";
                        mainWindow.title = "Chat ("+txt_nickname.text+")";
                    }
                }
            }
            
        }

        //page du lobby crée pour l'admin (celui qui host la partie)
        Item {
            visible: view.currentPage === "show_lobby"
            ColumnLayout {
                //affichage des elements côte-à-côte
                RowLayout {
                    
                    Label {
                        text: "Serveur en écoute sur : "
                    }
                    //affiche le port du serveur
                    TextInput {
                        id: txt_server_addr
                        readOnly: true //à mettre en true
                        text: "BACKEND PAS ENCORE PRET"
                        property string url : "non-défini" //utilepour copier dans le presse-papier, pas implémenté
                    }

                    Button {
                        text: "copier l'url"
                        onClicked : {
                            console.log(txt_server_addr.url);
                        }
                    }
                }

                Label {
                    text: "Joueurs : "
                }

                TextArea {
                    id: ta_lobby
                    readOnly: true
                    selectByMouse : true
                    font.pixelSize: 14
                    //wrapMode: TextInput.WrapAnywhere
                    text : ""
                    background: Rectangle {
                        radius: 5
                        color: "white"
                    }
                }

                RowLayout {
                    
                    Label {
                        text: backend.nb_joueurs_co;
                    }
                    Label {
                        text: "/";
                    }
                    Label {
                        text: backend.nb_joueurs_max;
                    }
                    Label {
                        text: " joueurs connectés";
                    }
                }
                
            }
        }

        //page pour se connecter à un serveur
        Item {
            visible: view.currentPage === "join_lobby"
            //formulaire de connexion
            ColumnLayout {
                //ligne demandant l'adr du serveur
                 RowLayout {
                    //TODO : séparer les champs ip et port
                    Label {
                        text: "Adresse du serveur : "
                    }
                    //affiche le port du serveur
                    TextInput {
                        id: input_server_adr
                        readOnly: false 
                        text: "ws://127.0.0.1/"
                    }
                }
                //ligne demandant le pseudo
                 RowLayout {
                    //TODO : séparer les champs ip et port
                    Label {
                        text: "Pseudo : "
                    }
                    //affiche le port du serveur
                    TextInput {
                        id: input_join_lobby_nickname
                        readOnly: false 
                        text: "XxX pseudo XxX"
                    }
                }
                //bouton qui enclenche la connexion
                Button {
                    id : btn_connectionToServer
                    text: "Connexion"
                    onClicked : {
                        mainWindow.title = "Chat ("+input_join_lobby_nickname.text+")";
                        txt_server_addr.text = input_server_adr.text;
                        backend.connectToServer(input_server_adr.text, input_join_lobby_nickname.text);
                        this.enabled = false;
                        chatWindow.title = "Chat ("+input_join_lobby_nickname.text+")";
                    }
                }
            }
        }

    }

}