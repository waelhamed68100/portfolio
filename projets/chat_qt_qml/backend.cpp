#include "backend.h"

Backend::Backend(QObject *parent) : 
    //définit le QObject
    QObject(parent)
{
    
}

//petite fonction utilitaire qui formatte l'adresse du client
static QString getIdentifier(QWebSocket *peer)
{
    return QStringLiteral("%1:%2").arg(peer->peerAddress().toString(),
                                       QString::number(peer->peerPort()));
}
static QString getIdentifier(QString nickname)
{
    return QStringLiteral("%1 CONNECTED").arg(nickname);
}

//N : on change la page depuis le c++
void Backend::changePage(QString page)
{
    qDebug() << "On change de page : " << page;
    //on lance l'évènement de l'IHM <changer de page> (onPageChanged)
    emit pageChanged(page);
}

//N :un utilisateur créer un serveur
void Backend::createServer(int nb_joueurs_max, QString nickname)
{
    m_nickname = nickname;
    qDebug() << "Création d'un serveur. Nombre de slots : " << nb_joueurs_max;
    //on récupère le paramètre : nombre de joueurs qui peuvent se connecter
    m_nb_joueurs_max = nb_joueurs_max;
    m_nb_joueurs_co = 1;
    //on notifie l'ihm que la valeur a changé
    emit nb_joueurs_maxChanged();
    emit nb_joueurs_coChanged();
    
    //le webSocketServer du serveur (nous)
    m_pWebSocketServer = new QWebSocketServer(
            //message lors de la création
            QStringLiteral("Serveur crée"),
            //mode non sécurisé (on commence simple)
            QWebSocketServer::NonSecureMode,
            //référence vers le backend
            this
    );

    //on lance l'écoute du serveur sur l'adr locale et sur un port disponible (décidé par l'os)
    if (m_pWebSocketServer->listen(QHostAddress::Any, 0))
    {
        qDebug() << "Serveur en écoute sur " << m_pWebSocketServer->serverUrl().toString();
        emit serverListening(m_pWebSocketServer->serverUrl().toString());
        //liaison entre les évenements du socket et cette class
        //event : nouvelle connection au serveur
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection,
                this, &Backend::onNewConnection);
        
        //on affiche la page de l'interface du lobby
        emit pageChanged("show_lobby");
        emit newPlayerConnected(m_nickname);
    }
    
}

//S : nouveau client se connecte au serveur
void Backend::onNewConnection(){
    //qwebsocket du client
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();
    pSocket->setParent(this);
    qDebug() << getIdentifier(pSocket) << " CONNECTED\n";

    //on bind l'event "nouveau message (texte)"
    connect(pSocket, &QWebSocket::textMessageReceived,
            this, &Backend::serverProcessMessage);
    //on bind l'event "déconnection"
    
    connect(pSocket, &QWebSocket::disconnected,
            this, &Backend::clientDisconnected_S);
    
    //on ajoute le nouveau client à la liste des clients
    m_clients << pSocket;
}



//S : serveur recoit un message
void Backend::serverProcessMessage(const QString &message){
    //on récupère le client
    QWebSocket *pSender = qobject_cast<QWebSocket *>(sender());
    qDebug() << getIdentifier(pSender) << " a envoyé un message : \n" << message;
    //TODO : traitement du message (check protocole)
    QStringList parsed = message.split(REQ_SEPARATOR);
    QString commande = parsed.at(0);
    //cas : client se connecte au lobby
    if(commande.compare("connect_lobby") == 0){
        qDebug() << "quelqu'un vient de se connecter au lobby.";
        QString pseudo = message;
        pseudo.remove(QString("connect_lobby" REQ_SEPARATOR));
        addToLobby(pSender, pseudo);
    }
    //cas : un client envoi un message de chat
    else if(commande.compare("chat_send") == 0){
        qDebug() << "un client a nevoyé un message sur le chat.";
        QString msg = message;
        msg.remove(QString("chat_send" REQ_SEPARATOR));
        serveChatMsg(msg);
    }
    else {
        qDebug() << "commande " << commande << " inconnue";
    }
            
}

//C : un client se connecte à un serveur
void Backend::connectToServer(QString url, QString nickname){
    m_nickname = nickname;
    m_pWebSocketClient = new QWebSocket(
        //message lors de la création
        nickname,
        //mode non sécurisé (on commence simple)
        QWebSocketProtocol::VersionLatest,
        //référence vers le backend
        this
    );
    qDebug() << "Connection au serveur " << url << "...";
    //on lie l'event connecté
    connect(m_pWebSocketClient, &QWebSocket::connected, this, &Backend::onClientConnected);
    //on bind l'event "nouveau message (texte)"
    connect(m_pWebSocketClient, &QWebSocket::textMessageReceived,
            this, &Backend::clientProcessMessage);
    //on bind l'event "déconnection du serveur"
    connect(m_pWebSocketClient, &QWebSocket::disconnected, this, &Backend::serverDisconnected);
    //on lance la connection au serveur
    m_pWebSocketClient->open(QUrl(url));
}

//C : lorsque la connection au serveur a été interrompu
void Backend::serverDisconnected(){
    m_nb_joueurs_co = 0;
    emit serverDisconnection();
}

//C : lorsque le client se connecte
void Backend::onClientConnected(){
    qDebug() << "Connecté !";
    connectToLobby();
    
}

//C : fonction appellée par le client lorsqu'il veut se connecter au lobby (envoyer son pseudo)
void Backend::connectToLobby(){
    //on envoit le pseudo
    //construction de la requête
    QString req = QStringLiteral("connect_lobby" REQ_SEPARATOR "%1").arg(m_nickname);
    qDebug() << req; 
    //envoi de la requête
    m_pWebSocketClient->sendTextMessage(req);
}

//S : ajoute un client qui vient de se connecter au lobby à la liste des clients connectés au lobby
void Backend::addToLobby(QWebSocket * client, QString nickname){
    qDebug() << nickname << " s'est connecté au serveur";
    //on vérfie qu'il resete de la place 
    if(m_nb_joueurs_co == m_nb_joueurs_max){
        qDebug() << "Mais il ne reste plus aucune place disponible dans le lobby. On lui refuse la connexion.";
        client->sendTextMessage(QString("server_full"));
        return;
    }
    m_nb_joueurs_co++;
    //association de son pseudo à son socket
    m_nicknames[client] = nickname;
    //on notifie l'interface
    emit newPlayerConnected(nickname);
    emit nb_joueurs_coChanged();
    //on lui renvoit les infos du lobby
    sendLobby(client);
    //on notifie tous les autres clients de la nouvelle connection
    QString notif = QStringLiteral("new_player" REQ_SEPARATOR "%1").arg(nickname);
    for(QWebSocket *s : m_clients){
        //on envoi pas la notif au client qui vient de se connecter
        if(s != client)
            s->sendTextMessage(notif);
    }

}

//S : renvoit les infos du lobby au client
void Backend::sendLobby(QWebSocket * client){
    qDebug() << "envoi des infos du lobby à " << client;
    QString res = QStringLiteral("lobby" REQ_SEPARATOR "%1" REQ_SEPARATOR "%2" REQ_SEPARATOR "%3")
                    .arg(m_nb_joueurs_co) 
                    .arg(m_nb_joueurs_max)
                    .arg(m_nickname);
    std::map<QWebSocket*, QString>::iterator it;
    for ( it = m_nicknames.begin(); it != m_nicknames.end(); it++ ){
        res = QStringLiteral("%1" REQ_SEPARATOR "%2").arg(res).arg(it->second);
    }
    qDebug() << res;
    client->sendTextMessage(res);
}

//C : client recoit un message 
void Backend::clientProcessMessage(const QString &message){
    qDebug() << "Le serveur nous a envoyé un message : \n" << message;
    QWebSocket *pSender = qobject_cast<QWebSocket *>(sender());
    //traitement du message (check protocole)
    QStringList parsed = message.split(REQ_SEPARATOR);
    QString commande = parsed.at(0);
    //cas : le serveur renvoit les infos du lobby
    if(commande.compare("lobby") == 0){
        qDebug() << "Infos du lobby récupérées.";
        QString infos_lobby = message;
        infos_lobby.remove(QString("lobby" REQ_SEPARATOR));
        connectedToLobby(infos_lobby);
    }
    //cas : nouveau joeuur connecté au lobby
    else if(commande.compare("new_player") == 0){
        qDebug() << "Un nouveau joeur a rejoin le lobby.";
        QString pseudo = message;
        pseudo.remove(QString("new_player" REQ_SEPARATOR));
        newClientConnected(pseudo);
    }
    //cas : un joueur s'est déconnecté du lobby
    else if(commande.compare("player_disconnected") == 0){
        QString pseudo = message;
        pseudo.remove(QString("player_disconnected" REQ_SEPARATOR));
        qDebug() << "Le joueur " << pseudo <<  " a quitté le lobby.";
        m_nb_joueurs_co--;
        emit nb_joueurs_coChanged();
        emit playerDisconnected(pseudo);
    }
    //cas : un joueur s'est déconnecté du lobby
    else if(commande.compare("server_full") == 0){
        qDebug() << "Lobby plein...";
        emit serverFull();
    }
    //cas : un message de chat a été reçu
    else if(commande.compare("chat_receive") == 0){
        qDebug() << "Message de chat reçu.";
        QString msg = message;
        msg.remove("chat_receive" REQ_SEPARATOR);
        emit chatMessage(msg);
    }
    else {
        qDebug() << "commande " << commande << " inconnue";
    }
}

//C : fonction qui est appellée lorsqu'on recoit les infos du lobby par le serveur
void Backend::connectedToLobby(QString infos_lobby){
    changePage("show_lobby");
    qDebug() << "infos_lobby : \n" << infos_lobby;
    QStringList parsed = infos_lobby.split(REQ_SEPARATOR);
    //on récupère le nombre de joueurs connectés
    m_nb_joueurs_co = std::stoi(parsed.at(0).toStdString());
    qDebug() << "m_nb_joueurs_co = " << m_nb_joueurs_co;
    //on récupère le nombre de joueurs max dans la salle
    m_nb_joueurs_max = std::stoi(parsed.at(1).toStdString());
    qDebug() << "m_nb_joueurs_max = " << m_nb_joueurs_max;

    //on récupère la liste de pseudos connectés au lobby
    qDebug() << "utilisateurs connectés : ";
    for(int i = 2; i < parsed.size(); i++ ){
        qDebug() << parsed.at(i);
        m_client_nicknames.append(parsed.at(i));
        emit newPlayerConnected(parsed.at(i));
    }
    emit nb_joueurs_coChanged();
    emit nb_joueurs_maxChanged();
}

//C : lorsqu'un nouveau joueur se connecte au lobby
void Backend::newClientConnected(QString nickname){
    m_nb_joueurs_co++;
    emit newPlayerConnected(nickname);
    emit nb_joueurs_coChanged();
}


//S : lorsqu'un client se déconnecte du serveur
void Backend::clientDisconnected_S(){
    //on récupère son pseudo    
    QWebSocket *pSender = qobject_cast<QWebSocket *>(sender());
    QString pseudo = m_nicknames[pSender];
    //cas où un client s'est connecté mais s'est déconnecté pck le lobby était plein
    if(pseudo == NULL){
        return;
    }
    qDebug() << "Le client " << pSender << " (" << pseudo << ") s'est déconnecté du serveur";
    //on efface le client des listes (TODO : fusionner les 2 listes en une seule)
    //dictionnaire des pseudos
    std::map<QWebSocket*, QString>::iterator it;
    it = m_nicknames.find(pSender);
    m_nicknames.erase(it);
    //liste des sockets des clients
    if(m_clients.removeOne(pSender) == false) qDebug() << "ERREUR suppression socket client !";
    //on notifie tous les clients
    QString notif = QStringLiteral("player_disconnected" REQ_SEPARATOR "%1").arg(pseudo);
    for(QWebSocket *s : m_clients){
        s->sendTextMessage(notif);
    }
    //màj du nombre de clients connectés
    m_nb_joueurs_co--;
    emit nb_joueurs_coChanged();
    //on notifie l'interface du host
    emit playerDisconnected(pseudo);
}

//N : l'utilisateur envoit un message de chat
void Backend::sendChatMsg(QString msg){
    QString req = QStringLiteral("chat_send" REQ_SEPARATOR "%1").arg(msg);
    //si on est un client
    if(m_pWebSocketClient != NULL){
        m_pWebSocketClient->sendTextMessage(req);
    }
    //sinon on est le serveur
    else {
        serveChatMsg(msg);
    }
        
}

//S : lorsque le serveur recoit un message de chat il le renvoit à tous les autres clients
void Backend::serveChatMsg(QString msg){
    QWebSocket *pSender = qobject_cast<QWebSocket *>(sender());
    QString pseudo = m_nicknames[pSender];
    if(pseudo == NULL) pseudo = m_nickname;
    QString chat_msg = QStringLiteral("chat_receive" REQ_SEPARATOR "[%1] %2: %3")
        .arg(QTime().currentTime().toString("hh:mm:ss"))
        .arg(pseudo)
        .arg(msg);
    //on renvoi le message à tous les clients
    for(QWebSocket *s : m_clients){
        s->sendTextMessage(chat_msg);
    }
    //on l'affiche sur l'ihm du serveur
    emit chatMessage(chat_msg.remove(QString("chat_receive" REQ_SEPARATOR)));

}
