const { bdd } = require("./bdd.js");


const connexion = (identifiant, password) => {
    const sql = 'SELECT * FROM Users WHERE identifiant = ? AND password = ?';
    return new Promise((resolve, reject) => {
        bdd.query(sql, [identifiant, password], (err, result) =>{
            if(err) return reject(err);
            console.log(result);
            return resolve(result);
            
        })
    })
    
    
}

const creerUser = (identifiant, password, isAdmin) => {
    const user  = { identifiant: identifiant, password: password, isAdmin: isAdmin}
    const sql = `INSERT INTO Users SET ?`
    
    return new Promise((resolve, reject) =>{
        const query = bdd.query(sql, user, (err, result) =>{
            if(err) return reject(err)
            console.log(result);
            console.log("Un user ajouté en bdd.");
            return resolve(result);
        })

    })
    
}

const getUserById = (id) => {
    const sql = `SELECT * FROM Users WHERE id = ?`
    
    return new Promise((resolve, reject) =>{
        const query = bdd.query(sql, id, (err, result) =>{
            if(err) return reject(err)
            console.log(result);
            console.log("Un user récupéré en bdd.");
            return resolve(result[0]);
        })

    })
}


module.exports = {
    connexion,
    creerUser,
    getUserById
};