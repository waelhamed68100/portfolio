
const { bdd } = require("../models/bdd.js")


const addArticle = (titre, description, imageUrl, postedByUser) => {
    const article  = { 
        titre: titre, 
        description: description,
        image: imageUrl,
        postedByUser: postedByUser
    }
    const sql = `INSERT INTO Articles SET ?`
    return new Promise((resolve, reject) => {
        const query = bdd.query(sql, article, (err, result) =>{
            if(err) return reject( err);
            console.log(result);
            console.log("Un article ajouté en bdd...");
            return resolve(result);
        })
    })
    
}

const updateArticle = (id, titre, description, imageUrl, postedByUser) => {
    const article  = { 
        titre: titre, 
        description: description,
        image: imageUrl,
        postedByUser: postedByUser
    }
    const sql = `UPDATE Articles SET ? WHERE id = ?`
    return new Promise((resolve, reject) => {
        const query = bdd.query(sql, [article, id], (err, result) =>{
            if(err) return reject( err);
            console.log(result);
            console.log("Un article modifié en bdd...");
            return resolve(result);
        })
    })
    
}

const getArticles = () => {
    return new Promise( (resolve, reject) => {
        const sql = `SELECT * FROM Articles`
        const query = bdd.query(sql, (err, results) =>{
        if(err) throw err;
        console.log(results);
        console.log("Articles récupérés depuis la base de données.");
        return resolve(results);
    })
    })
    
}

const getArticle = (id) => {
    return new Promise( (resolve, reject) => {
        const sql = `SELECT * FROM Articles WHERE id = ?`
        const query = bdd.query(sql,[id], (err, results) =>{
        if(err) throw err;
        console.log(results);
        if(results.length === 0) {
            console.log("aucun article avec id = "+id+" trouvé en bdd.");
            return resolve(null);
        }
        else {
            console.log("1 article récupéré depuis la base de données.");
            return resolve(results[0]);    
        }
    })
    })
    
}

module.exports = {
    addArticle,
    getArticles,
    getArticle,
    updateArticle
}

