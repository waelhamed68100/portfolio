const mysql = require("mysql");


// infos de connexion à la base de données
const bdd = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "maBaseDeDonnees"
})

// connexion à la base de données
bdd.connect((err) => {
    if(err) throw err;

    console.log("mysql connected...")

})

const creerBdd = () => {
    const sql = "CREATE DATABASE IF NOT EXISTS maBaseDeDonnees";
    bdd.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);
        console.log("base de données crée.");
    })
}

const creerTableArticles = () => {
    const sql = `CREATE TABLE IF NOT EXISTS Articles(
        id INTEGER UNSIGNED AUTO_INCREMENT NOT NULL, 
        titre VARCHAR(255) NOT NULL,
        description TEXT , 
        image VARCHAR(255) NOT NULL,
        postedByUser INTEGER UNSIGNED,
        PRIMARY KEY(id),
        FOREIGN KEY (postedByUser) REFERENCES Users(id)

    )`;
    bdd.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);
        console.log("table Articles crée.");

    })
}

const supprimerTableArticles = () => {
    const sql = `DROP TABLE IF EXISTS Articles`;
    bdd.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);
        console.log("table Articles supprimée.");

    })
}

const creerTableUsers = () => {
    const sql = `CREATE TABLE IF NOT EXISTS Users(
                    id INTEGER UNSIGNED AUTO_INCREMENT NOT NULL, 
                    identifiant VARCHAR(255) NOT NULL,
                    password VARCHAR(255) NOT NULL,
                    isAdmin BOOLEAN NOT NULL,
                    PRIMARY KEY(id) 
                )`;
    bdd.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);
        console.log("table Users crée.");

    })
}

const supprimerTableUsers = () => {
    const sql = `DROP TABLE IF EXISTS Users`;
    bdd.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);
        console.log("table Users supprimée.");

    })
}







module.exports = {
    bdd,
    supprimerTableArticles,
    supprimerTableUsers,
    creerTableArticles,
    creerTableUsers,
    
}