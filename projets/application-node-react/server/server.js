const express = require("express");
const fileUpload = require('express-fileupload');

const cors = require("cors");
const App = express();
const port = 5002;

const jwt = require('jsonwebtoken');
const userController = require("./controllers/userController.js");
const articleController = require("./controllers/articleControler.js");
const bdd = require('./models/bdd.js');

App.use(cors());

App.use(express.json());
//App.use(express.urlencoded({ extended: true }));
App.use(fileUpload());

//check token
App.use((request, response, next) => {
    userController.isLoggedIn(request.body.token)
        .then((decodedToken)=> {
            console.log("Utilisateur connecté. id = "+decodedToken)
        })
        .catch((err)=>{
            console.log("Token invalide.(peut tout de même accéder à getArticles)");
        })
    next();
});



App.post("/creerUser", userController.creerUser)
// renvoit un json contenant les infos des articles à afficher
App.get('/getArticles', articleController.getArticles)

App.post("/getArticle", articleController.getArticle)

App.post("/connexion", userController.connexion)

App.post("/addArticle", articleController.creerArticle)

App.post("/updateArticle", articleController.updateArticle)

App.use(express.static(__dirname + '/images'));

App.listen(port, () => {
    console.log("serveur en écoute sur le port " + port);
    /*
    bdd.supprimerTableArticles();
    bdd.supprimerTableUsers();
    bdd.creerTableUsers();
    bdd.creerTableArticles();
    */
   // création de l'admin
   //bdd.creerUser("user","secret", true);
})