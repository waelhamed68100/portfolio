const jwt = require("jsonwebtoken");
const users = require("../models/users.js");


const connexion = (request, response)=>{
    console.log("tentative de connexion...");
    console.log(request.body);
    // s'il manque des données de connexion
    if(!request.body || !request.body.identifiant || !request.body.password){
        response.send({connected: false, msg: "missing credentials"})
    }
    else {
        //identification de l'utilisateur
        users.connexion(request.body.identifiant, request.body.password)
            .then((result) => {
                if(result.length === 0) {
                    console.log("Aucun utilisateur avec cet identifiant et ce mot de passe");
                    response.send({
                        connected: false,
                        msg: "Aucun utilisateur avec ces infos de connexion trouvé."
                    })
                }
                else if (result.length === 1) {
                    console.log("Utilisateur connecté avec succès.");
                    const  token = jwt.sign(result[0].id, 'secret-key-token');
    
                    response.send({
                        token: token,
                        connected: true,
                        msg: "Vous vous êtes connecté avec succès",
                        isAdmin: result[0].isAdmin
                    })
                }
                else if (result.length > 1) {
                    res.send({
                        connected: false,
                        msg: "Plusieurs utilisateurs avec ces mêmes infos de connexion..."
                    })
                    throw new Error("Plusieurs utilisateurs avec cet identifiant et ce mot de passe.");
      
                }
            })
    }
}

const creerUser = (request, response) =>{
    if(!request.body.identifiant || !request.body.password ) {
        response.send({
            connected: false,
            msg: "Missing credentials"
        })
    }
    else {
        users.creerUser(request.body.identifiant, request.body.password, false)
            .then((result)=> {
                console.log("un utilisateur ajouté en bdd.");
                
                const  token = jwt.sign(request.body.identifiant, 'secret-key-token');

                response.send({
                    token: token,
                    connected: true,
                    msg: "Vous avez crée un compte avec succès."
                })
            })
    }
}

const isLoggedIn = (token) => {
    return new Promise( (resolve, reject) => {
        // verify jwt
        if(!token) {
            console.log("isLoggedIn : Aucun token envoyé");
            return resolve(null);
        }

        jwt.verify(token, 'secret-key-token', function(err, decoded) {
            if(err) throw new Error("invalid token.")
            console.log("decoded token :", decoded);
            return resolve(decoded);
        });
    } )
    
    
}



module.exports = {
    isLoggedIn,
    connexion,
    creerUser,
};