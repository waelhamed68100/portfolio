const userController = require("./userController.js");
const articles = require("../models/articles.js");
const fs = require("fs");
const users = require("../models/users.js");



const creerArticle = (request, response) => {
    console.log("addArticle", request.body);
    console.log("request.files",request?.files?.image)

    // check user is authenticated
    userController.isLoggedIn(request.body.token).then( (decodedToken) => {
        if(decodedToken){
            // add article in db
            const article = JSON.parse(request.body.article);
            //vérification parametres
            if(!article.titre || !article.description || !request.files || !request.files.image || request.files.image === "undefined") {
                console.log("info(s) article manquante(s).");
                response.send({
                    articleAdded: false,
                    message: "infos article manquantes."
                })
                return;
            }
            //get user id
            users.getUserById(decodedToken)
                .then((user)=>{
                    console.log("user", user);
                    console.log("isAdmin", user.isAdmin);
                    if(user.isAdmin === 0) {
                        response.send({
                            articleAdded: false,
                            message: "Vous devez être un admin pour pouvoir ajouter des articles"
                        })
                        return;
                    }
                    // save image
                    fs.appendFile(__dirname + "/../images/" + request.files.image.name, request.files.image.data, 'binary', function(err){
                        if (err) throw err
                        console.log('image article sauvegardée.')
                        
                        articles.addArticle(
                            article.titre, 
                            article.description,
                            "http://localhost:5002/"+request.files.image.name,
                            decodedToken
                        ).then((result) => {
                            response.send({
                                articleAdded: true,
                                message : "Article correctement ajouté en base de données."
                            })
                        })

                    })
                })
            
            
        }
        else {
            console.log("user not authentified with jwt.");
            response.send({
                articleAdded: false,
                message: ""
            });
        }
    })
    
}

const updateArticle = (request, response) => {
    console.log("updateArticle", request.body);
    console.log("request.files",request?.files?.image)

    // check user is authenticated
    userController.isLoggedIn(request.body.token).then( (decodedToken) => {
        if(decodedToken){
            // add article in db
            const article = JSON.parse(request.body.article);
            //vérification parametres
            if(!article.titre || !article.description || !request.files || !request.files.image || request.files.image === "undefined") {
                console.log("info(s) article manquante(s).");
                response.send({
                    articleAdded: false,
                    message: "infos article manquantes."
                })
                return;
            }
            //get user id
            users.getUserById(decodedToken)
                .then((user)=>{
                    console.log("user", user);
                    console.log("isAdmin", user.isAdmin);
                    if(user.isAdmin === 0) {
                        response.send({
                            articleAdded: false,
                            message: "Vous devez être un admin pour pouvoir modifier des articles"
                        })
                        return;
                    }
                    // save image
                    fs.appendFile(__dirname + "/../images/" + request.files.image.name, request.files.image.data, 'binary', function(err){
                        if (err) throw err
                        console.log('image article sauvegardée.')
                        
                        articles.updateArticle(
                            article.id,
                            article.titre, 
                            article.description,
                            "http://localhost:5002/"+request.files.image.name,
                            decodedToken
                        ).then((result) => {
                            response.send({
                                articleAdded: true,
                                message : "Article correctement modifié en base de données."
                            })
                        })

                    })
                })
            
            
        }
        else {
            console.log("user not authentified with jwt.");
            response.send({
                articleAdded: false,
                message: "mauvais token"
            });
        }
    })
    .catch((err)=> {
        console.log("Problème de token.");
        response.send({
            articleAdded: false,
            message: "mauvais token"
        });
    })
}

const getArticles = (request, response) => {
    articles.getArticles().then( (results) => {
        response.send(
            JSON.stringify(results.map((article)=>{
                return {
                    id: article.id,
                    titre: article.titre,
                    description: article.description,
                    image: article.image
                }
            }))
        )
    })
}

const getArticle = (request, response) => {
    if(!request.body.id) {
        response.send({
            found: false,
            message: "Missing article id."
        })
        return;
    }
    articles.getArticle(request.body.id).then( (article) => {
        if(article === null) {
            response.send({
                found: false,
                message: "Aucun article avec id "+request.body.id+" trouvé en base de données."
            })
            return;
        }
        response.send(
            JSON.stringify({
                found: true,
                id: article.id,
                titre: article.titre,
                description: article.description,
                image: article.image,
                postedByUser: article.postedByUser
                
            })
        )
        return;
    })
}

module.exports = {
    creerArticle,
    getArticles,
    getArticle,
    updateArticle
}