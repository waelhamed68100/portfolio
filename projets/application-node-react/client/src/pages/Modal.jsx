import ModalSuccessCSS from "./ModalSuccess.module.css"
import ModalErrorCSS from "./ModalError.module.css"

export default function Modal ({modalMessage, setModalMessage}) {
    

    const clearMsg = (e) => {
        e.preventDefault();
        setModalMessage({});
    }

    console.log("modalMessage", modalMessage)
    if(modalMessage.message) {
        return <div className={modalMessage.isSuccess ? ModalSuccessCSS.modalSuccess : ModalErrorCSS.modalError}> 
                    <div className={modalMessage.isSuccess ? ModalSuccessCSS.modalMsg : ModalErrorCSS.modalMsg}>Message : { modalMessage.message }
                        
                    </div>
                    <button className={modalMessage.isSuccess ? ModalSuccessCSS.modalButton : ModalErrorCSS.modalButton}onClick={clearMsg}>fermer</button>
                </div>
    }
    
}