import { useRouteError } from "react-router-dom"

export function PageErreur() {

    const erreurPage = useRouteError();
    console.log(erreurPage);

    return <h1>Aucune page avec ce nom</h1>

}