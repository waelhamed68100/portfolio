import ArticleCSS from "./Article.module.css"
import { useNavigate, useParams } from "react-router-dom"

export function Article( {article} ) {

    const {id} = useParams()
    console.log("id:",id);
    console.log("article", article)

    const navigate = useNavigate();

    return <div onClick={(e) => {navigate('/article/'+ article.id ) } } className={ArticleCSS.card}>
        <img className={ArticleCSS.img} src={article.image} style={{width:"100%"}}></img>
        <div className={ArticleCSS.container}>

            <h4><b>{article.titre}</b></h4>
            <p>{article.description}</p>
        </div>
    </div>

}
