import { useOutletContext } from "react-router-dom";
import ConnexionCSS from "./Connexion.module.css"
import { useState } from "react";

export function CreerCompte() {

    const [user, setUser, setModalMessage] = useOutletContext();

    const HandleCreerCompte = (e) => {
        e.preventDefault();
        const postData = {
            identifiant: user.identifiant,
            password: user.password
        }
        fetch("http://localhost:5002/creerUser",{
            headers: {
                'Content-Type':'application/json',
            },
            method: 'POST',
            body: JSON.stringify(postData)
        }).then(res => res.json())
        .then(res => {
            console.log(res)
            if(res.connected === true) {
                setUser({
                    ...user,
                    connected: true
                })
                setModalMessage({
                    isSuccess: true,
                    message: res.msg
                })
                localStorage.setItem("token", res.token);
                console.log("token : ", localStorage.getItem("token"));
            }
            else {
                setUser({
                    ...user,
                    connected: false
                })
                setModalMessage({
                    isSuccess: false,
                    message: res.msg
                });
            }
        })
    }

    const HandleChange = (e) => {
        setUser(user => ({
            ...user,
            [e.target.name]: e.target.value
        }))
    }


    if(user.connected === true) {
        return <h1>Vous avez déjà un compte et vous êtes connecté.</h1>
    }
    else {
        return <>
            <h1>Créer un compte</h1>
            <form className={ConnexionCSS.form}>
            <   label>Nouvel identifiant:
                    <input
                        type="text" 
                        name="identifiant"
                        value={user.identifiant}
                        onChange={HandleChange}
                    />
                </label>
                <label>Nouveau mot de passe:
                    <input
                        type="text" 
                        name="password"
                        value={user.password}
                        onChange={HandleChange}
                    />
                </label>
                <button onClick={HandleCreerCompte}>Créer un compte</button>
            </form>
        </>
    }


    
}