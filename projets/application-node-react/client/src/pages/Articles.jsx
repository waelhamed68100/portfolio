import { useState, Fragment, useEffect} from "react";
import { Article } from "./Article";
import ArticlesCSS from "./Articles.module.css"
import ArticleCSS from "./Article.module.css"
import { useOutletContext } from "react-router-dom";


export function Articles ( props ) {
    //const listeArticles = props.listeArticles;
    const [ isLoading, setIsLoading ] = useState(true);
    const [listeArticles, setListeArticles ] = useState([]);
    const [ article, setArticle ] = useState({
        titre: "Titre de l'article",
        description: "Ici se trouve la description d'un article à acheter"
    });

    useEffect(()=>{
        //alert("liste des articles modifiée");
        fetch("http://localhost:5002/getArticles")
            .then((response) => {
                response.json().then((result)=> {
                    console.log(result);
                    setListeArticles(result);
                    setIsLoading(false);
                })
            })
    }, [])

    const handleChange = (e) => {

        setArticle(article => ({
            ...article, 
            [e.target.name]: e.target.value
        }))
    }

    const [preview, setPreview] = useState();
    const [fichierImage, setFichierImage] = useState();

    const handleImage = (e) => {
        setPreview(URL.createObjectURL(e.target.files[0]));
        setFichierImage(e.target.files[0]); 
    }
    

    const [user, setUser, setModalMessage] = useOutletContext();

    

    const envoyerFormulaireCreationArticle =  (e) => {
        console.log(e.target);
        console.log("preview", preview);
        console.log("fichierImage", fichierImage);
        alert("envoi de l'article à ajouter en base de données...");
        
        var data = new FormData()
        data.append('image', fichierImage)
        data.append('article', JSON.stringify(article))
        data.append("token", localStorage.getItem("token"))
        
        fetch( "http://localhost:5002/addArticle", {
            method: 'POST',

            body: data
        })
        .then((response) => response.json())
        .then((result) => {
            console.log("résultat upload article :", result)
            setModalMessage({
                isSuccess: result.articleAdded,
                message: result.message
            })
        })
        e.preventDefault();

    }
    if(isLoading === true) {
        return <h1>Chargement des articles...</h1>
    }
    else {
        return <>
            <h1>Liste des Articles</h1>
            <div className={ArticlesCSS.container}>
                { listeArticles.map((article) =>{
                    return <Fragment key={article.id} >
                        <Article  article={article} ></Article>
                    </Fragment>
                }) }

            </div>
           

            <h1>Ajouter un article</h1>
            
            <form className={ArticlesCSS.form}>
                
                    <input
                        type="text" 
                        placeholder="Titre de l'article..."
                        name="titre"
                        value={article.titre}
                        onChange={handleChange}
                        className={ArticlesCSS.formElement}
                    > 
                    </input>
                    <textarea 
                        placeholder="Description..." 
                        value={article.description}
                        name="description"
                        onChange={handleChange}
                        className={ArticlesCSS.formElement}
                    ></textarea>
                    <input id="image" onChange={handleImage} type="file" name="image" accept="image/png, image/jpg"  />
                    <button 
                        onClick={envoyerFormulaireCreationArticle} 
                        type="button"
                        className={ArticlesCSS.formElement}
                    >
                        Envoyer l'article
                    </button>


                
            </form>

            <div className={ArticleCSS.card}>
                <img className={ArticleCSS.img} src={preview} style={{width:"100%"}}></img>
                <div className={ArticleCSS.container}>

                    <h4><b>{article.titre}</b></h4>
                    <p>{article.description}</p>
                </div>

            </div>
        </>
    }
}
