import ArticleCSS from "./Article.module.css"
import ArticlesCSS from "./Articles.module.css"
import { useParams } from "react-router-dom"
import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";


export function PageUnArticle(  ) {

    const {id} = useParams();
    const [user, setUser, setModalMessage] = useOutletContext();
    const [article, setArticle] = useState(null);
    const [isModifyingArticle, setIsModifyingArticle] = useState(false);

    console.log("id:",id);
    useEffect(()=>{
        fetch("http://localhost:5002/getArticle",{
            headers: {
                'Content-Type':'application/json',
            },
            method: 'POST',
            body: JSON.stringify({id:id})
        }).then(res => res.json())
        .then(res => { 
            console.log("1 article :", res)
            if(res.found === false) {
                setModalMessage({
                    isSuccess:false,
                    message: res.message
                })
            }
            else {
                setArticle(res)
            }
        })
        .catch((err)=>{
            console.log("erreur :", err)
        })
    }, [])


    const envoyerFormulaireModificationArticle = (e) => {
        alert("modif article");
        const data = new FormData()
        data.append('image', fichierImage ? fichierImage : preview)
        data.append('article', JSON.stringify(article))
        data.append("token", JSON.parse(localStorage.getItem("user")).token)
        console.log("data :", data);

        fetch( "http://localhost:5002/updateArticle", {
            method: 'POST',

            body: data
        })
        .then((response) => response.json())
        .then((result) => {
            console.log("résultat modification article :", result)
            setModalMessage({
                isSuccess: result.articleAdded,
                message: result.message
            })
        })
    }

    const handleChange = (e) => {

        setArticle(article => ({
            ...article, 
            [e.target.name]: e.target.value
        }))
    }

    const [preview, setPreview] = useState();
    const [fichierImage, setFichierImage] = useState();

    const handleImage = (e) => {
        setPreview(URL.createObjectURL(e.target.files[0]));
        setFichierImage(e.target.files[0]); 
    }

    if(article === null) {
        return <h1>Chargement de l'article...</h1>
    }
    else if (isModifyingArticle === true && user.connected === true) {
        return <>
            <h1>Modification de l'article</h1>
            <form className={ArticlesCSS.form}>
                
                        <input
                            type="text" 
                            placeholder="Titre de l'article..."
                            name="titre"
                            value={article.titre}
                            onChange={handleChange}
                            className={ArticlesCSS.formElement}
                        > 
                        </input>
                        <textarea 
                            placeholder="Description..." 
                            value={article.description}
                            name="description"
                            onChange={handleChange}
                            className={ArticlesCSS.formElement}
                        ></textarea>
                        <input id="image" onChange={handleImage} type="file" name="image" accept="image/png, image/jpg"  />
                        <button 
                            onClick={envoyerFormulaireModificationArticle} 
                            type="button"
                            className={ArticlesCSS.formElement}
                        >
                            Modifier l'article
                        </button>

                        <button onClick={(e)=> {setIsModifyingArticle(false);}}>Annuler</button>


                    
                </form>
                <div className={ArticleCSS.card}>
                    <img className={ArticleCSS.img} src={preview ? preview : article.image} style={{width:"100%"}}></img>
                    <div className={ArticleCSS.container}>

                        <h4><b>{article.titre}</b></h4>
                        <p>{article.description}</p>
                    </div>

                </div>
        </>
    }
    else if (user.connected === true){
        return <div className={ArticleCSS.card}>
                    <img className={ArticleCSS.img} src={article.image} style={{width:"100%"}}></img>
                    <div className={ArticleCSS.container}>

                        <h4><b>{article.titre}</b></h4>
                        <p>{article.description}</p>
                        
                        <button onClick={(e) => {setIsModifyingArticle(true);}}>Modifier</button>
                        <button onClick={(e) => {alert("suppression article");}}>Supprimer</button>

                    </div>

                </div>
    }
    else {
        return <div className={ArticleCSS.card}>
                    <img className={ArticleCSS.img} src={article.image} style={{width:"100%"}}></img>
                    <div className={ArticleCSS.container}>

                        <h4><b>{article.titre}</b></h4>
                        <p>{article.description}</p>
                        
                    </div>

                </div>
    }
}