import { useOutletContext } from "react-router-dom";
import ConnexionCSS from "./Connexion.module.css"
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export function Connexion() {
    const [user, setUser, setModalMessage] = useOutletContext();
    console.log("user",user);

    const HandleChange = (e) => {
        setUser(user => ({
            ...user,
            [e.target.name]: e.target.value
        }))
    }

    const HandleConnexion = (e) => {
        e.preventDefault();
        const postData = {
            identifiant: user.identifiant,
            password: user.password
        }
        fetch("http://localhost:5002/connexion",{
            headers: {
                'Content-Type':'application/json',
            },
            method: 'POST',
            body: JSON.stringify(postData)
        }).then(res => res.json())
        .then(res => {
            console.log(res)
            if(res.connected === true) {
                setUser({
                    ...user,
                    connected: true
                })
                setIsConnexion(false);
                setModalMessage({
                    isSuccess: true,
                    message: res.msg
                })
                localStorage.clear("user");
                localStorage.setItem("user", JSON.stringify({token: res.token, isAdmin: res.isAdmin}));
            }
            else {
                setUser({
                    ...user,
                    connected: false
                })
                setIsConnexion(false);
                setModalMessage({
                    isSuccess: false,
                    message: res.msg
                });
            }
        });

        setIsConnexion(true);
    }

    const [isConnexion, setIsConnexion] = useState(false);

    if(isConnexion){
        return <h1>Autentification...</h1>
    }

    const deconnexion = (e) => {
        console.log("Déconnexion...");
        localStorage.clear("user");
        setUser({
            ...user,
            connected: false
        })
    }
    
    //const navigate = useNavigate();

    const redirectCreerCompte = (e) => {
        e.preventDefault();
        //navigate('/creer-compte');
    }
    


    if(user.connected === false) {
        return <>
        
            <h1>Vous n'êtes pas connecté</h1>
            <form className={ConnexionCSS.form}>
            <   label>Identifiant:
                    <input
                        type="text" 
                        name="identifiant"
                        value={user.identifiant}
                        onChange={HandleChange}
                    />
                </label>
                <label>Mot de passe:
                    <input
                        type="text" 
                        name="password"
                        value={user.password}
                        onChange={HandleChange}
                    />
                </label>
                <button onClick={HandleConnexion}>Se connecter</button>
                <a href="/creer-compte">Vous n'avez pas de compte ? Créer un compte</a>
            </form>
        </>
    }
    else {
        return <>
            <h1>Utilisateur { user.identifiant } Connecté avec succès</h1>
            <button onClick={deconnexion}> Se déconnecter</button>        
        </> 
    }
}