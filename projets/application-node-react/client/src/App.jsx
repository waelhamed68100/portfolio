import { RouterProvider, Link, createBrowserRouter, NavLink, Outlet } from "react-router-dom"
import { Article } from "./pages/Article.jsx"
import { PageUnArticle } from "./pages/PageUnArticle.jsx"
import { PageErreur } from "./pages/PageErreur.jsx";
import { Articles } from "./pages/Articles.jsx"
import AppCSS from "./App.module.css"
import { Connexion } from "./pages/Connexion.jsx";
import { useEffect, useState } from "react";
import Modal from "./pages/Modal.jsx";
import { CreerCompte } from "./pages/CreerCompte.jsx";


const router = createBrowserRouter([
    {
      path: '/',
      element: <PageAcceuil></PageAcceuil>,
      errorElement: <PageErreur></PageErreur>,
      children: [
        {
          path: 'articles',
          element: <Articles />
        },
        {
          path: 'article/:id',
          element: <PageUnArticle></PageUnArticle>
        },
        {
          path: 'espace-personnel',
          element: <Connexion ></Connexion>
        },
        {
          path: 'creer-compte',
          element: <CreerCompte ></CreerCompte>
        }
      ]

    },

]);

function PageAcceuil () {

  const [user, setUser] = useState({connected:false, identifiant:"", password:""});
  console.log("user page acceuil : ", user);

  const [errorMsg, setErrorMsg] = useState({});
  const [successMsg, setSuccessMsg] = useState({});

  const [modalMessage, setModalMessage] = useState({});

  const clearErrorMsg = (e) => {
    e.preventDefault();
    setErrorMsg('');
  }
  const clearSuccessMsg = (e) => {
    e.preventDefault();
    setSuccessMsg('');
  }

  useEffect( ()=> {
    console.log("localstorage:", localStorage.getItem("user"));
    if(localStorage.getItem("user")) {
      setUser({
        ...user,
        connected: true,
        token: JSON.parse(localStorage.getItem("user"))["token"],
        isAdmin: JSON.parse(localStorage.getItem("user"))["isAdmin"]
      })
    }
  }, [user.connected])



  return <>
    <div className={AppCSS.container}>
      <header className={AppCSS.header}>
        <nav className={AppCSS.nav}>
          <NavLink to="/"> Page d'acceuil </NavLink>
          <NavLink to="/articles"> ArticleS </NavLink>
          <NavLink to="/article/42"> Article </NavLink>
          <NavLink to="/espace-personnel"> Espace personnel </NavLink>
        </nav>
      </header>
      <div>
        <Modal modalMessage={modalMessage} setModalMessage={setModalMessage}></Modal>

        <div className={AppCSS.outlet}>
          <Outlet  context={[user, setUser, setModalMessage]} ></Outlet>

        </div>
      </div>


    </div>
  
  </>
}

function App() {

  return (
    <>
      <RouterProvider router={router} />
    </>
  )
}

export default App
