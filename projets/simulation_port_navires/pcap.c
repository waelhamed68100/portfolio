#include "pcap.h"

///Nombre de quais dans le port
int nb_quais;

///Indique le nombre de quais occupés par des navires
int nb_quais_occupes;

///tableau représentant tous les quais du port;
navire *quais;

///Booléen indiquant la fin de la simulation (plus aucun navire accepté, fin du prog lrosque tous les navires ont quitté le port)
bool *stop_simulation;

///Nombre de navires attendant qu'une place se libère
int nb_nav_en_attente;


///partage de fonction de la variable :
///FLAG indiquant le type de requete
///ou numéro du quai d'accostage lorsqu'un navire en fait la demande
int *shared_int;


///numéro du quai d'un navire lorsqu'un camion en fait la demande
c_demande_quai *camion_demande_navire;


int semid; //intercepteur des id des semaphores
union semun arg; //paramètre pour les semctl
int shmid; //intercepteur des id des segments de mémoire partagée
struct sembuf sops; //pour les P/V

///Pas le droit d'utiliser le param de compilation c99 mdr
int i; //pour les boucles, only 80s kids will remember



/** @brief Fonction qui supprime les objets IPC System V crées par la simulation */
void liberer_ressources(int exit_val){
    /* sémaphore de signalement de requete */
    
    //printf("\n");
    //print_debug("Suppression de la sémaphore de signalement de requete\n");
    //récupération de l'id
    if((semid = semget(_clefs.cle_requete_pcap, 0, droits)) == -1){
        perror("ERREUR :  main : semget cle_requete_pcap");
        //exit(EXIT_FAILURE);
    }
    //suppression de la sémaphore (mutex)
    if(semctl(semid, -1, IPC_RMID) == -1){
        perror("ERREUR :  main : semctl IPC_RMID requete_pcap,");
        //exit(EXIT_FAILURE);
    }
    else
        print_debug("liberer_ressources : sémaphore de signalement de requete supprimé\n");

    /* segment de mémoire partagée de l'entier multifonctions shared_int */
    
    //printf("\n");
    //print_debug("Suppression du sgmt. mém. partagée entier multifonctions shared_int\n");
    //récupération de l'id du segment de mémoire partagé
    shmid = shmget(_clefs.cle_shared_int, 0,  droits );
    if((shmid) == -1){
        perror("ERREUR :  main : shmget cle_shared_int");
        //exit(EXIT_FAILURE);
    }
    //suppression du segment de mémoire partagé associé
    if(shmctl(shmid, IPC_RMID, NULL) == -1){
        perror("ERREUR :  main : shmctl IPC_RMID shared_int");
        //exit(EXIT_FAILURE);
    }
    else
        print_debug("liberer_ressources : sgmt. mém. partagée shared_int supprimé\n");

    
    /* segment de mémoire partagée de la liste des quais */
    
    //printf("\n");
    //print_debug("Suppression du sgmt. mém. partagée quais\n");
    //récupération de l'id du segment de mémoire partagé
    shmid = shmget(_clefs.cle_quais, 0,  droits );
    if((shmid) == -1){
        perror("ERREUR :  main : shmget quais");
        //exit(EXIT_FAILURE);
    }
    //suppression du segment de mémoire partagé associé
    if(shmctl(shmid, IPC_RMID, NULL) == -1){
        perror("ERREUR :  main : shmctl IPC_RMID quais");
        //exit(EXIT_FAILURE);
    }
    else
        print_debug("liberer_ressources : sgmt. mém. partagée quais supprimé\n");

    
    /* sémaphore des chargements de conteneurs des différents navires*/
    
    //printf("\n");
    //print_debug("Suppression de la sémaphore des chargements de conteneurs\n");
    //récupération de l'id
    if((semid = semget(_clefs.cle_chargmts_navires, 0, droits)) == -1){
        perror("ERREUR :  main : semget cle_chargmts_navires");
        //exit(EXIT_FAILURE);
    }
    //suppression de la sémaphore 
    if(semctl(semid, -1, IPC_RMID) == -1){
        perror("ERREUR :  main : semctl IPC_RMID chargmts_navires");
        //exit(EXIT_FAILURE);
    }
    else
        print_debug("liberer_ressources : sémaphore sémaphore des chargements de conteneurs supprimée.\n");

    
    /* sémaphore de la liste des navires en attente */
    
    //printf("\n");
    //print_debug("Suppression de la sémaphore des navires en attente\n");
    //récupération de l'id
    if((semid = semget(_clefs.cle_navires_attente, 0, droits)) == -1){
        perror("ERREUR :  main : semget cle_navires_attente");
        //exit(EXIT_FAILURE);
    }
    //suppression de la sémaphore 
    if(semctl(semid, -1, IPC_RMID) == -1){
        perror("ERREUR :  main : semctl IPC_RMID navires_attente");
        //exit(EXIT_FAILURE);
    }
    else
        print_debug("liberer_ressources : sémaphore des navires en attente supprimée.\n");

    /* segment de mémoire partagée du booléen indiquant la fin de la simu */
    
    //printf("\n");
    //print_debug("Suppression du sgmt. mém. partagée fin_sim\n");
    //récupération de l'id du segment de mémoire partagé
    shmid = shmget(_clefs.cle_fin_sim, 0,  droits );
    if((shmid) == -1){
        perror("ERREUR :  main : shmget quais");
        //exit(EXIT_FAILURE);
    }
    //suppression du segment de mémoire partagé associé
    if(shmctl(shmid, IPC_RMID, NULL) == -1){
        perror("ERREUR :  main : shmctl IPC_RMID fin_sim");
        //exit(EXIT_FAILURE);
    }
    else
        print_debug("liberer_ressources : sgmt. mém. partagée bool fin_sim supprimé\n");


    exit(exit_val);
}

/** @brief Fonctio qui est appellée lorsque pstop signale la fin de la simulation
 */
void req_stop_simulation(){
    if(*stop_simulation == true){
        print_info("req_stop_simulation : fin de simulation déjà activée...\n");
    }
    else{
        *stop_simulation = true;
        print_info("req_stop_simulation : fin de simulation activée. Attente du départ de tous les navires du port.\n");
        //V(navires_attente)
        sops.sem_num = 0; sops.sem_op = nb_nav_en_attente; sops.sem_flg = 0;
        print_info("req_stop_simulation : on indique la fin de sim aux navires qui attendaient.\n");
        if(semop(_clefs.semid_navires_attente, &sops, 1) == -1){
            perror("attente_quai_libre : semop V(navires_attente)");
            exit(EXIT_FAILURE);
        }

    }

    if(nb_quais_occupes == 0){
        print_info("req_stop_simulation : aucun navire dans le port. Fin du prog. Bye.\n");
         liberer_ressources(EXIT_SUCCESS);
    }
    
}

/** @brief Fonction qui est appellée lorsqu'un navire quitte le port
 * (donc après avoir été déchargé)
 */
void navire_quitte_port(){
    print_info("navire_quitte_port : un navire quitte le port. Nb de quais occupés--\n");
    nb_quais_occupes--;
    //V(requete_pcap.d)
    sops.sem_num = 3; sops.sem_op = 1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("req_camion_quai : semop V(requete_pcap.d)");
        liberer_ressources(EXIT_FAILURE);
    }
    /* on attend que pcap soit libéré, sinon on peut exit avant même qu'il n'ait fait le P() (erreur sémaphore inexistante...) */
    //P(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = -1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("req_camion_quai : semop P(requete_pcap.c)");
        liberer_ressources(EXIT_FAILURE);
    }
    //si fin de simulation activée et que le dernier navire a quitté le port
    if(*stop_simulation == true && nb_quais_occupes == 0){
        print_debug("navire_quitte_port : fin de simulation et tous les navires ont quitté le port : fin du prog. Bye.\n");
        liberer_ressources(EXIT_SUCCESS);
    }
    //si simulation tjs en cours et que des navires sont en attente
    if(*stop_simulation == false && nb_nav_en_attente > 0){
        //on autorise un navire à entrer
        //V(navires_attente)
        sops.sem_num = 0; sops.sem_op = 1; sops.sem_flg = 0;
        if(semop(_clefs.semid_navires_attente, &sops, 1) == -1){
            perror("req_stop_simulation : semop V(navires_attente)");
            liberer_ressources(EXIT_FAILURE);
        }
        nb_nav_en_attente--;
    }
}

/** @brief Petite fonction qui s'occupe de vérifier qu'on recoit un accusé de réception 
 * après le traitement de certaines requetes (les requetes qui attendent un résultat)
 */
void accuse_reception(){
    /* on fait un V(requete_pcap.d) pour débloquer le prog qui attendait le résultat de la requete */
    sops.sem_num = 3; sops.sem_op = 1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("accuse_reception : semop V(requete_pcap.d)");
        liberer_ressources(EXIT_FAILURE);
    }
    /* on fait un P(requete_pcap.c) en attendant que le prog nous indique qu'il a bien récupéré le résultat */
    print_debug("accuse_reception : on attend que le prog récupère bien le résultat...\n");
    sops.sem_num = 2; sops.sem_op = -1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("accuse_reception : semop P(requete_pcap.c)");
        liberer_ressources(EXIT_FAILURE);
    }
}


/** @brief Fonction qui renvoit le nb de quais dans le port (utile pour pdump uniquement)
 */
void req_nbquais_pdump(){
    /* on indique le nombre de port dans la variable partagée */
    *shared_int = nb_quais;
    /* on débloque le pdump qui attendait et on attend qu'il récupère la valeur */
    return accuse_reception();
};


/** @brief Fonction qui gère l'accueil des navires dans le port 
 */
void req_navire_entree(){
    /* on vérifie qu'il reste de la place */
    if(*stop_simulation == true){
        print_info("req_navire_entree : fin de simulation active : aucun nouveau navire accepté.\n");
        *shared_int = -1;
    }
    else if(nb_quais_occupes == nb_quais){
        print_info("req_navire_entree : aucun quai disponible. Bye le navire.\n");
        *shared_int = -1;
        nb_nav_en_attente++;
    }
    //s'il reste de la place on cherche le quai disponible
    else{
        int idx = -1; //pas obligatoire, mais permet de vérifier qu'on ait pas fait d'erreur
        for(i = 0; i < nb_quais; i++){
            if(quais[i].quai_reserve == false){
                idx = i;
                break;
            }
        }
        if(idx == -1){
            printf("req_navire_entree : ERREUR : on aurait dû trouver un quai disponible.\n");
            liberer_ressources(EXIT_FAILURE);
        }
        print_info("req_navire_entree : quai disponible : quai n° %d.\n", idx);
        *shared_int = idx;
        nb_quais_occupes++; //un nouveau navire entre dans le port
    }
    /* on débloque le pnav qui attendait et on attend qu'il récupère la valeur */
    return accuse_reception();
};

/** @brief Fonction qui gère la demande de quai d'un navire par un camion
 */
void req_camion_quai(){
    /* on demande le paramètre */
    //avec un V(requete_pcap.d)
    sops.sem_num = 3; sops.sem_op = 1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("req_camion_quai : semop V(requete_pcap.d)");
        liberer_ressources(EXIT_FAILURE);
    }

    /* on attend le paramètre */
    //avec un P(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = -1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("req_camion_quai : semop P(requete_pcap.c)");
        liberer_ressources(EXIT_FAILURE);
    }

    /* paramètre reçu : traitement de la requete */
    //on cherche le quai du navire
    int idx = -1;
    for(i = 0; i < nb_quais; i++){
        if(quais[i].quai_reserve == true 
        && quais[i].nom == (char)*shared_int){
            idx = i;
            break;
        }
    }
    if(idx == -1){
        print_info("req_camion_quai : aucun navire '%c' dans le port.\n", (char)*shared_int);
    }
    else{
        print_info("req_camion_quai : le navire '%c' a été trouvé : quai n° %d.\n",(char)*shared_int, idx);
    }
    *shared_int = idx;

    /* on débloque le pfcam qui attendait et on attend qu'il récupère la valeur */
    return accuse_reception();
}

/** @brief Fonction qui réceptionne toutes les requetes faites à pcap
*/
void traitement_req(){

    /* on attend une requete */

    //P(requete_pcap.a)
    print_info("----------------------------------\n");
    print_info("traitement_req : attente d'une nouvelle requete...\n");
    sops.sem_num = 0; sops.sem_op = -1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("traitement_req : semop P(requete_pcap.a)");
        liberer_ressources(EXIT_FAILURE);
    }

    /* requete recue, on indique que le demandeur peut indiquer le type de sa requete */

    print_debug("traitement_req : requete acceptée. Attente du type de la requete...\n");
    //on indique qu'on veut le type en débloquant le demandeur 
    //avec un V(requete_pcap.b)
    sops.sem_num = 1; sops.sem_op = 1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("traitement_req : semop V(requete_pcap.b)");
        liberer_ressources(EXIT_FAILURE);
    }
    //on attend le type en se bloquant avec un P(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = -1; sops.sem_flg = 0;
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("traitement_req : semop P(requete_pcap.c)");
        liberer_ressources(EXIT_FAILURE);
    }

    /* analyse du type de la requete */
    print_debug("traitement_req : type de la requete reçu = %d.\n", *shared_int);
    switch(*shared_int){
        //cas d'un navire qui fait une demande de quai
        case REQ_NAVIRE_QUAI :
            print_info("traitement_req : un navire demande un quai.\n");
            req_navire_entree();
            break;
        //cas d'un camion qui demande le quai d'un navire
        case REQ_CAMION_QUAI_NAVIRE :
            print_info("traitement_req : un camion demande le quai d'un navire.\n");
            req_camion_quai();
            break;
        //cas d'un navire qui quitte le port
        case REQ_NAVIRE_QUITTE_PORT :
            print_info("traitement_req : un navire quitte le port.\n");
            navire_quitte_port();
            break;
        //cas de pstop qui signale la fin de simulation
        case REQ_STOP_SIMULATION :
            print_info("traitement_req : pstop signale la fin de la simulation.\n");
            req_stop_simulation();
            break;
        //cas de pdump qui veut récupérer le nombre total de quais
        case REQ_PDUMP :
            print_info("traitement_req : pdump souhaite le nombre total de quai.\n");
            req_nbquais_pdump();
            break;
        default :
            print_info("traitement_req : type de requete inconnu (%d ?), on pourrait l'ignorer, mais exit(erreur) quand même.\n", *shared_int);
            liberer_ressources(EXIT_FAILURE);
            
    }
    /* on passe à la requete suivante */
    *shared_int = -666; //pas obligatoire, juste pour être sûr qu'on a pas fait d'erreur
    return traitement_req();
    
    
}

/** @brief Fonction qui créer les objets IPC Systeme V necessaires à toute la simulation.
 * (même pour les objets utilisés dans les autres programmes de la simulation uniquement)
 */
void init_semid_shmid(){
    /* Initialisation des sémaphores / segments de mémoire partagés */

    /*sémaphore de signalement de requete */
    
    /** note : 3 éléments :
     * (1) sem. pour la demande de requete à pcap
     * (2) sem. pour l'attente avant le traitemet de la requete
     * (3) et (4) pour les echanges entre pcap et le prog qui a fait la requete (si besoin d'échanger des données)
     */
    if((semid = semget(_clefs.cle_requete_pcap, 4, IPC_CREAT | IPC_EXCL | droits)) == -1){
        perror("init_semid_shmid : semget cle_requete_pcap");
        liberer_ressources(EXIT_FAILURE);
    }
    _clefs.semid_requete_pcap = semid;
    //initialisation des élements de la sémaphore à 0
    for(i = 0; i < 4; i++){
        arg.val = 0;
        if(semctl(_clefs.semid_requete_pcap, i, SETVAL, arg) == -1){
            perror("init_semid_shmid : semctl semid_requete_pcap");
            liberer_ressources(EXIT_FAILURE);
        }
    }

    /* segment de mémoire partagée contenant le type de la requete faite à pcap */
    /* segment de mémoire partagée pour la reception du quai d'accostage lorsqu'un navire en fait la demande */

    if((shmid = shmget(_clefs.cle_shared_int, sizeof(int), IPC_CREAT | IPC_EXCL | droits)) == -1){
        perror("init_semid_shmid : shmget cle_flag_requete_pcap");
        liberer_ressources(EXIT_FAILURE);
    }
    _clefs.shmid_shared_int = shmid;
	shared_int = shmat(_clefs.shmid_shared_int, NULL, 0);
    *shared_int = -666; 

    /* segment de mémoire partagée contenant la liste des quais (et les navires qui y sont amarrés) */
    
    if((shmid = shmget(_clefs.cle_quais, sizeof(struct u_navire)*nb_quais, IPC_CREAT | IPC_EXCL | droits)) == -1){
        perror("init_semid_shmid : shmget cle_quais");
        liberer_ressources(EXIT_FAILURE);
    }
    _clefs.shmid_quais = shmid;
	quais = shmat(_clefs.shmid_quais, NULL, 0);
    //initialisation du port 
    for(i = 0; i < nb_quais; i++){
        quais[i].quai_reserve = false;
    }
    nb_quais_occupes = 0;

    /* sémaphore simulant les chargements de conteneurs des différents navires */

    if((semid = semget(_clefs.cle_chargmts_navires, nb_quais, IPC_CREAT | IPC_EXCL | droits)) == -1){
        perror("init_semid_shmid : semget cle_chargmts_navires");
        liberer_ressources(EXIT_FAILURE);
    }
    _clefs.semid_chargmts_navires = semid;
    //initialisation des élements de la sémaphore à 0
    for(i = 0; i < nb_quais; i++){
        arg.val = 0;
        if(semctl(_clefs.semid_chargmts_navires, i, SETVAL, arg) == -1){
            perror("init_semid_shmid : semctl semid_chargmts_navires");
            liberer_ressources(EXIT_FAILURE);
        }
    }


    /* sémaphore simulant les navires qui attendent au large qu'un quai se libère */

    if((semid = semget(_clefs.cle_navires_attente, 1, IPC_CREAT | IPC_EXCL | droits)) == -1){
        perror("init_semid_shmid : semget cle_navires_attente");
        liberer_ressources(EXIT_FAILURE);
    }
    _clefs.semid_navires_attente = semid;
    //initialisation de l'élément de la sémaphore à 0
    arg.val = 0;
    if(semctl(_clefs.semid_navires_attente, 0, SETVAL, arg) == -1){
        perror("init_semid_shmid : semctl semid_navires_attente");
        liberer_ressources(EXIT_FAILURE);
    }

    /* segment de mémoire partagée contenant le bool indiquant si la fin de la simulation est active */
    
    if((shmid = shmget(_clefs.cle_fin_sim, sizeof(int), IPC_CREAT | IPC_EXCL | droits)) == -1){
        perror("init_semid_shmid : shmget cle_fin_sim");
        liberer_ressources(EXIT_FAILURE);
    }
    _clefs.shmid_fin_sim = shmid;
	stop_simulation = shmat(_clefs.shmid_fin_sim, NULL, 0);
    //initialisation du port 
    for(i = 0; i < nb_quais; i++){
        quais[i].quai_reserve = false;
    }
    nb_quais_occupes = 0;
    
}

int main(int argc, char** argv){
    /* init DEBUG_PORT */
    init_DEBUG_PORT();

    /* vérification des arguments */
    if(argc != 2){
        printf("main : ERREUR : usage: %s <nb_quais>\n",argv[0]);
        liberer_ressources(EXIT_FAILURE);
    }
    /* extraction du nombre de quais */
    nb_quais = atoi(argv[1]);
    if(nb_quais <= 0){
        printf("main : ERREUR : nb_quais invalide\n");
        printf("main : ERREUR : usage: %s <nb_quais>\n",argv[0]);
        liberer_ressources(EXIT_FAILURE);
    }

    print_debug("main : création d'un port à %d quais.\n", nb_quais);

    /* init des clées des sémaphores/mémoires partagées */
    init_IDS();

    /* init des des sémaphores et segments de mémoire partagée nécessaires à toute la simulation */
    print_debug("main : init. des sémaphores et sgmt. de mém. partagée...\n");
    init_semid_shmid();
    print_debug("Ok.\n");
    //init des variable internes 
    nb_quais_occupes = 0;

    /* lancement du traitement des requetes */
    print_info("main : lancement du traitement des requetes...\n");
    *stop_simulation = false;
    nb_nav_en_attente = 0;
    traitement_req();
   
    printf("ERREUR : on ne devrait jamais arriver ici !\n");
    liberer_ressources(EXIT_FAILURE);

    return EXIT_FAILURE; //obligatoire sinon compile pas sur turing
}