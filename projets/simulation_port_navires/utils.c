/** @file utils.c
 * @brief Contient des petites fonctions utilitaires
 */
#include "utils.h"

/** @brief Renvoit le maximum de 2 entiers
 */
int max(int a, int b){
    return a > b ? a : b;
}


/** @brief Définit quels types de messages afficher selon la var. d'env. DEBUG_PORT
 */
void init_DEBUG_PORT(){
    /* init du mode débuggage selon la variable d'env. DEBUG_PORT */
    char * env = getenv("DEBUG_PORT");
    if(env == 0){
        DEBUG_PORT = 0;
    }
    else{
        DEBUG_PORT = atoi(env);
    }
    print_info("DEBUG_PORT = %d.\n", DEBUG_PORT);
    //exit(EXIT_FAILURE);
}

/** @brief affiche un message de déboggage si DEBUG est vrai, sinon ne fait rien
 * @param S'utilise comme printf
 * */
void print_debug(const char* msg, ...){
    if(DEBUG_PORT > 1){
        char * heure = get_current_hour();
        printf("[%s] DEBUG : ", heure);
        free(heure);
        fflush(stdout);
        va_list argptr;
        va_start(argptr, msg);
        vfprintf(stderr, msg, argptr);
        fflush(stdout);
        va_end(argptr);
    }
}

/** @brief affiche un message d'info
 * @param S'utilise comme printf
 * */
void print_info(const char* msg, ...){
    if(DEBUG_PORT >= 1){
        char * heure = get_current_hour();
        printf("[%s]  Info : ", heure);
        fflush(stdout);
        free(heure);
        va_list argptr;
        va_start(argptr, msg);
        vfprintf(stderr, msg, argptr);
        fflush(stdout);
        va_end(argptr);
    }
}

/** @brief Renvoie l'heure courrante sous forme de chaîne hh:mm:ss (attention à bien penser à librérer cette chaîne)
 */
char * get_current_hour(){
    time_t t = time(NULL);
    struct tm *tmp = localtime(&t);
    char * s = malloc(sizeof(char) * 100);
    //tmp->tm_hour;
   
    sprintf(s,"%d:%d:%s%d", tmp->tm_hour, tmp->tm_min, tmp->tm_sec >= 10 ? "" : "0", tmp->tm_sec);
    return s;
}

