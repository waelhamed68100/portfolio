#include "cles_partagees.h"

/** @brief initialise les clés des segments de mémoire partagées et des sémaphores
 */
void init_IDS(){
   
    /* initialisation des clés */
    
    ///identifiant de la sémaphore de demaned de requete à pcap
    const int requete_pcap = 1;
    if( (_clefs.cle_requete_pcap = ftok(identifiant_semaphore, requete_pcap)) == -1){
        perror("init_IDS() : ftok cle_requete_pcap");
        exit(EXIT_FAILURE);
    };

    ///identifiant du sgmt. mém. partagée du type de requete faite à pcap
    const int shared_int = 2;
    if( (_clefs.cle_shared_int = ftok(identifiant_semaphore, shared_int)) == -1){
        perror("init_IDS() : ftok cle_shared_int");
        exit(EXIT_FAILURE);
    };

    ///identifiant du sgmt mém partagée de la liste des quais
    const int quais = 3;
    if( (_clefs.cle_quais = ftok(identifiant_semaphore, quais)) == -1){
        perror("init_IDS() : ftok cle_quais");
        exit(EXIT_FAILURE);
    };

    ///identifiant de la sémaphore des chargements des navires
    const int chargmts_navires = 4;
    if( (_clefs.cle_chargmts_navires = ftok(identifiant_semaphore, chargmts_navires)) == -1){
        perror("init_IDS() : ftok cle_chargmts_navires");
        exit(EXIT_FAILURE);
    };

    ///identifiant de la sémaphore des navires en attante de libération d'un quai
    const int nav_attentes = 5;
    if( (_clefs.cle_navires_attente = ftok(identifiant_semaphore, nav_attentes)) == -1){
        perror("init_IDS() : ftok cle_navires_attente");
        exit(EXIT_FAILURE);
    };

    ///identifiant du sgmt mem partagée du booléen indiquant la fin de la simulation
    const int fin_simu = 6;
    if( (_clefs.cle_fin_sim = ftok(identifiant_semaphore, fin_simu)) == -1){
        perror("init_IDS() : ftok cle_fin_sim");
        exit(EXIT_FAILURE);
    };
    
    
    

}
