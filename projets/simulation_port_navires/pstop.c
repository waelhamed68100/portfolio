#include "utils.h"
#include "cles_partagees.h"

int *shared_int;

int semid; //intercepteur des id des semaphores
int shmid; //intercepteur des id des segments de mémoire partagée
struct sembuf sops; //pour les P/V

/** @brief Fonction qui signale la fin de la simulation à pcap
 */
void signaler_fin_sim(){
    /* on notifie pcap qu'on veut faire une demande */

    //V(requete_pcap.a)
    sops.sem_num = 0; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("signaler_fin_sim : notification de la demande à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("signaler_fin_sim : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }
    /* on se met dans la file des progs qui demandent une requete */

    //P(requete_pcap.b)
    sops.sem_num = 1; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("signaler_fin_sim : en attente que pcap soit prêt à recevoir notre demande...\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("signaler_fin_sim : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* c'est notre tour : pcap en attente du type de la requete, on lui envoi */

    *shared_int = REQ_STOP_SIMULATION;
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("signaler_fin_sim : type de la requete envoyé à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("signaler_fin_sim : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* on sait que pcap a été notifié, on peut partir */
    
    print_debug("signaler_fin_sim : pcap a été notifié. Fin du prog. Bye.\n");
    exit(EXIT_SUCCESS);
}


/** @brief Fonction qui récupère les sémaphores et sgmt de mém. partagée necessaires à pnav
 */
void get_semid_shmid(){

    /* sémaphore de signalement de requete */
    
    if((semid = semget(_clefs.cle_requete_pcap, 0, droits)) == -1){
        perror("get_semid_shmid : semget cle_requete_pcap");
        exit(EXIT_FAILURE);
    }
    _clefs.semid_requete_pcap = semid;

    /* segment de mémoire partagée contenant le type de la requete faite à pcap */
    
    if((shmid = shmget(_clefs.cle_shared_int, 0, droits)) == -1){
        perror("get_semid_shmid : shmget cle_shared_int");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_shared_int = shmid;
	shared_int = shmat(_clefs.shmid_shared_int, NULL, 0);
}


int main(int argc, char** argv){
    /* vérification : aucun paramètre */
    if(argc != 1){
        printf("main : ERREUR : usage: %s \n",argv[0]);
        exit(EXIT_FAILURE);
    }
    (void)argc; (void)argv;
    /* init DEBUG_PORT */
    init_DEBUG_PORT();

    /* récupération des identifiants */
    init_IDS();

    /* récupération des sémaphores et segments de mémoire partagée necessaires à pnav */
    print_debug("main : récupération des sémaphores et sgmt. de mém. partagée...\n");
    get_semid_shmid();
    print_debug("Ok.\n");

    /* on signale la fin de simulation */
    signaler_fin_sim();

    
    return EXIT_SUCCESS;


}