/** @file utils.h
 */
#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdarg.h>

///booléen de type int
#define bool int
///valeur vrai du booléen
#define true 1
///valeur fausse 
#define false 0

///booléen indiquant quels types de messages afficher
bool DEBUG_PORT;

int max(int a, int b);

void init_DEBUG_PORT();

void print_debug(const char* msg, ...);

void print_info(const char* msg, ...);

char * get_current_hour();

#endif //UTILS_H