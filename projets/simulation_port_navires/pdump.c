#include "utils.h"
#include "cles_partagees.h"
#include <pthread.h>
#include <unistd.h>

///Nombre total de quais dans le port
int nb_quais;
///liste des quais du port
navire *quais;

int semid; //intercepteur des id des semaphores
int shmid; //intercepteur des id des segments de mémoire partagée
struct sembuf sops; //pour les P/V

///Le segmt de mém. partagée dans lequel sera indiqué le quai du navire
///et aussi utilisé pour indiquer le type de la requete (camion demande quai navire)
int *shared_int;

///Pas le droit d'utiliser le param de compilation c99 mdr
int i; //pour les boucles, only 80s kids will remember

/** @brief Fonction qui récupère les sémaphores et sgmt de mém. partagée necessaires à pfcam
 */
void get_semid_shmid(){

    /* sémaphore de signalement de requete */
    
    if((semid = semget(_clefs.cle_requete_pcap, 0, droits)) == -1){
        perror("get_semid_shmid : semget cle_requete_pcap");
        exit(EXIT_FAILURE);
    }
    _clefs.semid_requete_pcap = semid;
    
    /* segment de mémoire partagée contenant la liste des quais (et les navires qui y sont amarrés) */
    
    if((shmid = shmget(_clefs.cle_quais, 0, droits)) == -1){
        perror("get_semid_shmid : shmget cle_quais");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_quais = shmid;
	quais = shmat(_clefs.shmid_quais, NULL, 0);

    /* segment de mémoire partagée contenant le type de la requete faite à pcap */
    
    if((shmid = shmget(_clefs.cle_shared_int, 0, droits)) == -1){
        perror("get_semid_shmid : shmget cle_shared_int");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_shared_int = shmid;
	shared_int = shmat(_clefs.shmid_shared_int, NULL, 0);
}


/** @brief Fonction qui demande à pcap le nombre total de quais
 */
void requete_nb_quais(){
      /* on notifie pcap qu'on veut faire une demande */

    //V(requete_pcap.a)
    sops.sem_num = 0; sops.sem_op = 1; sops.sem_flg = 0;
    print_info("requete_nb_quais : notification de la demande à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_nb_quais : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }
    /* on se met dans la file des progs qui demandent une requete */

    //P(requete_pcap.b)
    sops.sem_num = 1; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("requete_nb_quais : en attente que pcap soit prêt à recevoir notre demande...\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_nb_quais : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* c'est notre tour : pcap en attente du type de la requete, on lui envoi */

    *shared_int = REQ_PDUMP;
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("requete_nb_quais : type de la requete envoyé à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_nb_quais : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* on se met en attente du résultat */

    //P(requete_pcap.d)
    sops.sem_num = 3; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("requete_nb_quais : en attente du résultat (nb de quais).\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_nb_quais : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* resultat reçu : on récupère la valeur et débloque pcap, qui attendait qu'on la récupère */
    
    nb_quais = *shared_int;

    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("requete_nb_quais : résultat de la demande récupéré ! On notifie pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_nb_quais : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* analyse du résultat */

    print_info("requete_nb_quaiiis : Infos sur les %d quais du port :\n\n", nb_quais);
    for(i = 0; i < nb_quais; i++){
        print_info("--------------------------------------\n- quai n° %d :\n",i);
        if(quais[i].quai_reserve == false){
            print_info("[quai libre]\n");
        }
        else{
            print_info("- nom du navire : %c\n", quais[i].nom);
            print_info("- nb de conteneurs restant : %d\n", quais[i].nb_conteneurs);
            print_info("- tps d'accostage : %d ms\n", quais[i].tps_accostage);
            print_info("- tps de déchargement : %d ms\n", quais[i].tps_dechargement);
        }
    }
    exit(EXIT_SUCCESS);
}


int main(int argc, char **argv){
    /* init DEBUG_PORT */
    init_DEBUG_PORT();
    
    /* vérification des arguments */
    if(argc != 1){
        printf("main : ERREUR : usage: %s \n",argv[0]);
        exit(EXIT_FAILURE);
    }

    /* init des clées des sémaphores/mémoires partagées */
    init_IDS();

    /* récupération des sémaphores et segments de mémoire partagée necessaires à pnav */
    print_debug("main : récupération des sémaphores et sgmt. de mém. partagée...\n");
    get_semid_shmid();
    print_debug("Ok.\n");

    /* deamnde du quai du navire */
    requete_nb_quais();

    return EXIT_SUCCESS;
}