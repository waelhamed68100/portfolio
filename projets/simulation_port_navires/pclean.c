#include "cles_partagees.h"
#include "utils.h"
#include <unistd.h>


int main(int argc, char** argv){
    (void)argc; (void) argv;
    /* vérification des arguments */
    if(argc != 1){
        printf("main : ERREUR : usage: %s \n",argv[0]);
        exit(EXIT_FAILURE);
    }
    /* init DEBUG_PORT */
    init_DEBUG_PORT();

    init_IDS();

    int semid;
    int shmid;

    /* sémaphore de signalement de requete */
    
    printf("\n");
    print_debug("Suppression de la sémaphore de signalement de requete\n");
    //récupération de l'id
    if((semid = semget(_clefs.cle_requete_pcap, 0, droits)) == -1){
        perror("ERREUR :  main : semget cle_requete_pcap");
        //exit(EXIT_FAILURE);
    }
    //suppression de la sémaphore (mutex)
    if(semctl(semid, -1, IPC_RMID) == -1){
        perror("ERREUR :  main : semctl IPC_RMID stop_simulation");
        //exit(EXIT_FAILURE);
    }
    else
        print_info("sémaphore de signalement de requete supprimé\n");

    /* segment de mémoire partagée de l'entier multifonctions shared_int */
    
    printf("\n");
    print_debug("Suppression du sgmt. mém. partagée entier multifonctions shared_int\n");
    //récupération de l'id du segment de mémoire partagé
    shmid = shmget(_clefs.cle_shared_int, 0,  droits );
    if((shmid) == -1){
        perror("ERREUR :  main : shmget cle_shared_int");
        //exit(EXIT_FAILURE);
    }
    //suppression du segment de mémoire partagé associé
    if(shmctl(shmid, IPC_RMID, NULL) == -1){
        perror("ERREUR :  main : shmctl IPC_RMID shared_int");
        //exit(EXIT_FAILURE);
    }
    else
        print_info("sgmt. mém. partagée shared_int supprimé\n");

    
    /* segment de mémoire partagée de la liste des quais */
    
    printf("\n");
    print_debug("Suppression du sgmt. mém. partagée quais\n");
    //récupération de l'id du segment de mémoire partagé
    shmid = shmget(_clefs.cle_quais, 0,  droits );
    if((shmid) == -1){
        perror("ERREUR :  main : shmget quais");
        //exit(EXIT_FAILURE);
    }
    //suppression du segment de mémoire partagé associé
    if(shmctl(shmid, IPC_RMID, NULL) == -1){
        perror("ERREUR :  main : shmctl IPC_RMID quais");
        //exit(EXIT_FAILURE);
    }
    else
        print_info("sgmt. mém. partagée quais supprimé\n");
    
    /* sémaphore des chargements de conteneurs des différents navires*/
    
    printf("\n");
    print_debug("Suppression de la sémaphore des chargements de conteneurs\n");
    //récupération de l'id
    if((semid = semget(_clefs.cle_chargmts_navires, 0, droits)) == -1){
        perror("ERREUR :  main : semget cle_chargmts_navires");
        //exit(EXIT_FAILURE);
    }
    //suppression de la sémaphore 
    if(semctl(semid, -1, IPC_RMID) == -1){
        perror("ERREUR :  main : semctl IPC_RMID chargmts_navires");
        //exit(EXIT_FAILURE);
    }
    else
        print_info("sémaphore sémaphore des chargements de conteneurs supprimée.\n");


    return EXIT_SUCCESS;
}