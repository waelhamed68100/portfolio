# Simulation d'un port

Année : 2018.

Programmes C de simulation d'un port (pour navires).

Le but de ce projet était d'utiliser uniquement les sémaphores et les segments de mémoire partagées IPC System V (des OS unix) pour gérer de multiples synchronisations entre les programmes. 

La simulation est composée de 3 programmes principaux : 
 * **pcap** : simule un port composé d'un certain nombre de quais.
 * **pnav** : simule un navire voulant entrer dans le port.
 * **pfcam** : simule une flotte de camions de déchargement des conteneurs d'un navire (chaque camion ne peut porter qu'un seul conteneur). 

 La simulation comporte plusieurs types de synchronisations : 
 * Les navires attendent au large qu'un quai soit disponible.
 * Les camions attendent que la grue de déchargment des conteneurs soit disponible.

La simulation ne s'arrête pas brusquement : le programme **pstop** permet de signaler la fin de la simulation à **pcap** : 
* Lorsque pcap recoit le signal, il ne permet plus à aucun navire d'entrer dans le port. 
* Les navires qui attendaient qu'un quai se libère s'en vont vers un autre port (pnav termins son exécution). 
* pcap attend que tous les navires présent dans le port quittent le port.
* pour que 

## Compilation
----------------------------------------
Compiler les programmes en utilisant le Makefile : `make all` 

**Note** : des tests automatisés peuvent être éxecuter en tapant `make test`

## Exécution
----------------------------------------
* pour **pcap** : `./pcap <nb_quais>`, avec : 
    * _nb_quais_ : le nombre de quais dans le port.

* pour **pnav** : `./pnav <nom_navire> <nb_conteneurs> <tps_accostage> <tps_dechargement>`, avec : 
    * _nom_navire_ : le nom du navire (un charactère max)
    * _nb_conteneurs_ : le nombre de conteneurs que le navire transporte.
    * _tps_accostage_ : le temps d'accostage du navire.
    * _tps_dechargement_ : le temps pris pour décharger un conteneur sur le quai.
 

* pour **pfcam** : `./pfcam <nom_navire> <nb_camions> <tps_chargement>`, avec : 
    * _nom_navire_ : le nom du navire à décharger.
    * _nb_camions_ : le nombre de camions dans la flotte (un camion ne peut charger qu'un seul conteneur).
    * _tps_chargement_ : le temps pour charger un conteneur du quai sur le camion.

* **Verbosité** : on peut indiquer quels seront les types d'information seront affichés dans le terminal. Il faut pour cela définir une variable shell DEBUG_PORT à l'aide de la commande `export DEBUG_PORT=<x>`, avec _x_ un entier :
    * x = 0 : aucune information affichée sauf pour les messages d'erreurs.
    * x = 1 : messages des informations principales (les actions menées).
    * x > 1 : en plus des messages pour x=1, les messages techniques sont affichés (états des synchronisations sémaphores...).

* **Outil de nettoyage** : il peut arriver que les fichiers unix des sémaphores se soient mal supprimés. Il faut alors réinitialisser ces fichier. Le programme **pclean** permet de s'en charger.


## Exemple d'éxecution
----------------------------------------
* Scénario : 
    * création d'un port à 1 quai.
    * 3 navires a, b et c arrivent au port.
    * les navires b et c attendent qu'un quai se libère.
    * le navire a est en attente de déchargement de ses 2 conteneurs.
    * une flotte de 2 camions est envoyée charger les conteneurs de a.
    * a quitte le port.
    * b prend sa place et se met en attente de déchargement de ses 3 conteneurs.
    * une flotte de 2 camions est envoyée charger les conteneurs de b.
    * la fin de la simulation est signalée.
    * le capitaine du port signale sa fermeture.
    * le navire c s'en va vers un autre port.
    * une flotte d'un seul camion est envoyée charger le dernier conteneur de b.
    * b quitte le port.
    * port fermé : pcap termine son exécution. Fin. 

Commandes à taper dans x terminaux ( [x] signifie que la commande est à taper dans le terminal numéro x ) :    
    
    [0] ./pclean
    [0] ./pcap 1
    
    [1] ./pnav a 2 1 1

    [2] ./pnav b 3 1 1

    [3] ./pnav c 5 1 1

    [4] ./pfcam a 2 2

    [4] ./pfcam b 2 2

    [4] ./pstop 

    [4] ./pfcam b 1 1

**Note** : pour avoir les messages d'informations qui s'affichent il faut avoir tapé `èxport DEBUG_PORT=1` dans chaque terminal.

Résultat de l'éxecution : 

`[0] ./pcap 1` :

    [13:46:27]  Info : DEBUG_PORT = 1.
    [13:46:27]  Info : main : lancement du traitement des requetes...
    [13:46:27]  Info : ----------------------------------
    [13:46:27]  Info : traitement_req : attente d'une nouvelle requete...
    [13:46:36]  Info : traitement_req : un navire demande un quai.
    [13:46:36]  Info : req_navire_entree : quai disponible : quai n° 0.
    [13:46:36]  Info : ----------------------------------
    [13:46:36]  Info : traitement_req : attente d'une nouvelle requete...
    [13:46:44]  Info : traitement_req : un navire demande un quai.
    [13:46:44]  Info : req_navire_entree : aucun quai disponible. Bye le navire.
    [13:46:44]  Info : ----------------------------------
    [13:46:44]  Info : traitement_req : attente d'une nouvelle requete...
    [13:46:50]  Info : traitement_req : un navire demande un quai.
    [13:46:50]  Info : req_navire_entree : aucun quai disponible. Bye le navire.
    [13:46:50]  Info : ----------------------------------
    [13:46:50]  Info : traitement_req : attente d'une nouvelle requete...
    [13:47:12]  Info : traitement_req : un camion demande le quai d'un navire.
    [13:47:12]  Info : req_camion_quai : le navire 'a' a été trouvé : quai n° 0.
    [13:47:12]  Info : ----------------------------------
    [13:47:12]  Info : traitement_req : attente d'une nouvelle requete...
    [13:47:12]  Info : traitement_req : un navire quitte le port.
    [13:47:12]  Info : navire_quitte_port : un navire quitte le port. Nb de quais occupés--
    [13:47:12]  Info : ----------------------------------
    [13:47:12]  Info : traitement_req : attente d'une nouvelle requete...
    [13:47:12]  Info : traitement_req : un navire demande un quai.
    [13:47:12]  Info : req_navire_entree : quai disponible : quai n° 0.
    [13:47:12]  Info : ----------------------------------
    [13:47:12]  Info : traitement_req : attente d'une nouvelle requete...
    [13:47:29]  Info : traitement_req : un camion demande le quai d'un navire.
    [13:47:29]  Info : req_camion_quai : le navire 'b' a été trouvé : quai n° 0.
    [13:47:29]  Info : ----------------------------------
    [13:47:29]  Info : traitement_req : attente d'une nouvelle requete...
    [13:48:11]  Info : traitement_req : pstop signale la fin de la simulation.
    [13:48:11]  Info : req_stop_simulation : fin de simulation activée. Attente du départ de tous les navires du port.
    [13:48:11]  Info : req_stop_simulation : on indique la fin de sim aux navires qui attendaient.
    [13:48:11]  Info : ----------------------------------
    [13:48:11]  Info : traitement_req : attente d'une nouvelle requete...
    [13:48:53]  Info : traitement_req : un camion demande le quai d'un navire.
    [13:48:53]  Info : req_camion_quai : le navire 'b' a été trouvé : quai n° 0.
    [13:48:53]  Info : ----------------------------------
    [13:48:53]  Info : traitement_req : attente d'une nouvelle requete...
    [13:48:53]  Info : traitement_req : un navire quitte le port.
    [13:48:53]  Info : navire_quitte_port : un navire quitte le port. Nb de quais occupés--


`[1] ./pnav a 2 1 1` : 

    [13:46:36]  Info : DEBUG_PORT = 1.
    [13:46:36]  Info : requete_quai : notification de la demande de quai à pcap.
    [13:46:36]  Info : requete_quai : Quai n° 0 dispo.
    [13:46:36]  Info : -----------------------------------------
    [13:46:36]  Info : accoster : le navire accoste...
    [13:46:36]  Info : -----------------------------------------
    [13:46:36]  Info : dechargement : déchargement des conteneurs en cours...
    [13:47:12]  Info : dechargement : conteneurs déchargés. On peut quitter le port.
    [13:47:12]  Info : -----------------------------------------
    [13:47:12]  Info : quitter_port : notification de la demande à pcap.
    [13:47:12]  Info : quitter_port : pcap a été notifié, on quitte le port. Fin du prog. Bye.


`[2] ./pnav b 3 1 1` : 

    [13:46:44]  Info : DEBUG_PORT = 1.
    [13:46:44]  Info : requete_quai : notification de la demande de quai à pcap.
    [13:46:44]  Info : requete_quai : Aucun quai disponible. On attend au large qu'une place se libère.
    [13:46:44]  Info : attente_quai_libre : attente d'un quai libre...
    [13:47:12]  Info : attente_quai_libre : nouveau quai disponible.
    [13:47:12]  Info : requete_quai : notification de la demande de quai à pcap.
    [13:47:12]  Info : requete_quai : Quai n° 0 dispo.
    [13:47:12]  Info : -----------------------------------------
    [13:47:12]  Info : accoster : le navire accoste...
    [13:47:12]  Info : -----------------------------------------
    [13:47:12]  Info : dechargement : déchargement des conteneurs en cours...
    [13:48:53]  Info : dechargement : conteneurs déchargés. On peut quitter le port.
    [13:48:53]  Info : -----------------------------------------
    [13:48:53]  Info : quitter_port : notification de la demande à pcap.
    [13:48:53]  Info : quitter_port : pcap a été notifié, on quitte le port. Fin du prog. Bye.

`[3] ./pnav c 5 1 1` : 

    [13:46:50]  Info : DEBUG_PORT = 1.
    [13:46:50]  Info : requete_quai : notification de la demande de quai à pcap.
    [13:46:50]  Info : requete_quai : Aucun quai disponible. On attend au large qu'une place se libère.
    [13:46:50]  Info : attente_quai_libre : attente d'un quai libre...
    [13:48:11]  Info : attente_quai_libre : fin de simulation signalée. Fin du prog. Bye.

`[4] ./pfcam a 2 2` : 

    [13:47:12]  Info : DEBUG_PORT = 1.
    [13:47:12]  Info : requete_quai : notification de la demande du quai à pcap.
    [13:47:12]  Info : Navire présent sur le quai n° 0. Déchargement...
    [13:47:12]  Info : -----------------------------------------
    [13:47:12]  Info : decharger_navire : déchargement des conteneurs...
    [13:47:12]  Info : decharger_navire : tous les conteneurs ont été déchargé. Fin du job. Bye.

`[4] ./pfcam b 2 2` : 

    [13:47:29]  Info : DEBUG_PORT = 1.
    [13:47:29]  Info : requete_quai : notification de la demande du quai à pcap.
    [13:47:29]  Info : Navire présent sur le quai n° 0. Déchargement...
    [13:47:29]  Info : -----------------------------------------
    [13:47:29]  Info : decharger_navire : déchargement des conteneurs...
    [13:47:29]  Info : decharger_navire : tous les camions de la flotte ont été utilisé. Fin du job. Bye.

`[4] ./pstop` : 

    [13:47:29]  Info : DEBUG_PORT = 1.

`[4] ./pfcam b 1 1` : 

    [13:48:53]  Info : DEBUG_PORT = 1.
    [13:48:53]  Info : requete_quai : notification de la demande du quai à pcap.
    [13:48:53]  Info : Navire présent sur le quai n° 0. Déchargement...
    [13:48:53]  Info : -----------------------------------------
    [13:48:53]  Info : decharger_navire : déchargement des conteneurs...
    [13:48:53]  Info : decharger_navire : tous les conteneurs ont été déchargé. Fin du job. Bye.




