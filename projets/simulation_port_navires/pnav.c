#include "utils.h"
#include "cles_partagees.h"
#include <pthread.h>
#include <unistd.h>

///Nom du navire (en réalité seul le premier caractère compte)
char nom;
///Nombre de conteneurs
int nb_conteneurs;
///Temps d'accostage du navire dans le port en millisecondes
int tps_accostage;
///Temps de déchargement d'un conteneur du navire
int tps_dechargement;

///Le segmt de mém. partagée contenant la liste des quais du port
navire *quais;
///Le quais réservé pour ce navire
int mon_quai;
///Le segmt de mém. partagée dans lequel sera indiqué notre quais
///et utilisé pour indiquer le type de la requete (navire demande quai, navire quitte port)
int *shared_int;
///Booléen indiquant la fin de la simulation (plus aucun navire accepté, fin du prog lrosque tous les navires ont quitté le port)
bool *stop_simulation;

int semid; //intercepteur des id des semaphores
union semun arg; //paramètre pour les semctl
int shmid; //intercepteur des id des segments de mémoire partagée
struct sembuf sops; //pour les P/V

///Pas le droit d'utiliser le param de compilation c99 mdr
int i; //pour les boucles, only 80s kids will remember
void requete_quai();

/** @brief Fonction qui est appelée lorsque le navire quitte le port
 */
void quitter_port(){
    print_info("-----------------------------------------\n");
    quais[mon_quai].quai_reserve = false;
    /* on notifie pcap qu'on veut faire une demande */

    //V(requete_pcap.a)
    sops.sem_num = 0; sops.sem_op = 1; sops.sem_flg = 0;
    print_info("quitter_port : notification de la demande à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("quitter_port : semop V(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }
    /* on se met dans la file des progs qui demandent une requete */

    //P(requete_pcap.b)
    sops.sem_num = 1; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("quitter_port : en attente que pcap soit prêt à recevoir notre demande...\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("quitter_port : semop P(requete_pcap.b)");
        exit(EXIT_FAILURE);
    }

    /* c'est notre tour : pcap en attente du type de la requete, on lui envoi */

    *shared_int = REQ_NAVIRE_QUITTE_PORT;
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("quitter_port : type de la requete envoyé à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("quitter_port : semop V(requete_pcap.c)");
        exit(EXIT_FAILURE);
    }

    /* on se met en attente de la réponse */

    //P(requete_pcap.d)
    sops.sem_num = 3; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("quitter_port : en attente de l'accusé de reception...\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        printf("WTF : %d %d %d !\n", sops.sem_num, sops.sem_op, sops.sem_flg);
        perror("quitter_port : semop P(requete_pcap.d)");
        exit(EXIT_FAILURE);
    }
    /* on notifie pcap, qui attendait qu'on recoive l'accusé de reception pour exit() */
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("quitter_port : on idique à pcap qu'il peut exit sans que ce pnav risque d'erreur de sémaphore inexistante.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("quitter_port : semop V(requete_pcap.c)");
        exit(EXIT_FAILURE);
    }

    /* on sait que pcap a été notifié, on peut partir */
    
    print_info("quitter_port : pcap a été notifié, on quitte le port. Fin du prog. Bye.\n");
    exit(EXIT_SUCCESS);
}

/** @brief Fonction qui s'occupe de décharger les conteneurs du navire
 */
void dechargement(){
    print_info("-----------------------------------------\n");
    /* on met à disposition les conteneurs du chargement */
    print_info("dechargement : déchargement des conteneurs en cours...\n");
    //tant qu'il reste des conteneurs à décharger
    while(quais[mon_quai].nb_conteneurs > 0){
        //on attend le temps de décharger le conteneur 
        usleep(1000 * tps_dechargement);
        //on met à disposition le conteneur fraichement déchargé
        //V(chargements.mon_quai)
        sops.sem_num = mon_quai; sops.sem_op = 1; sops.sem_flg = 0;
        print_debug("dechargement : on met à disposition le conteneur %d.\n", quais[mon_quai].nb_conteneurs);
        if(semop(_clefs.semid_chargmts_navires, &sops, 1) == -1){
            perror("dechargement : semop V(chargements.mon_quai)");
            exit(EXIT_FAILURE);
        }
        //on attend que le conteneur soit (littéralement) pris en charge par un camion
        print_debug("dechargement : on attend que le conteneur soit pris en charge par un camion...\n");
        //0(chargements.mon_quai)
        sops.sem_num = mon_quai; sops.sem_op = 0; sops.sem_flg = 0;
        print_debug("dechargement : on met à disposition le conteneur %d.\n", quais[mon_quai].nb_conteneurs);
        if(semop(_clefs.semid_chargmts_navires, &sops, 1) == -1){
            perror("dechargement : semop 0(chargements.mon_quai)");
            exit(EXIT_FAILURE);
        }
        //on passe au conteneur suivant
        quais[mon_quai].nb_conteneurs--;
    }
    
    /* déchargement complet : on notifie pcap du départ, puis fin prog */
    print_info("dechargement : conteneurs déchargés. On peut quitter le port.\n");
    quitter_port();
}

/** @brief Fonction qui simule l'accostage 
 */
void accoster(){
    /* on attend le temps d'accoster */
    print_info("-----------------------------------------\n");
    print_info("accoster : le navire accoste...\n");
    usleep(1000* tps_accostage);

    quais[mon_quai].nom = nom;
    quais[mon_quai].nb_conteneurs = nb_conteneurs;
    quais[mon_quai].tps_accostage = tps_accostage;
    quais[mon_quai].tps_dechargement = tps_dechargement;

    /* accostage fini : on passe au déchargement */
    dechargement();
    
    
}

/** @brief Fonction qui récupère les sémaphores et sgmt de mém. partagée necessaires à pnav
 */
void get_semid_shmid(){

    /* sémaphore de signalement de requete */
    
    if((semid = semget(_clefs.cle_requete_pcap, 0, 0)) == -1){
        perror("get_semid_shmid : semget cle_requete_pcap");
        exit(EXIT_FAILURE);
    }
    _clefs.semid_requete_pcap = semid;
    
    /* segment de mémoire partagée contenant la liste des quais (et les navires qui y sont amarrés) */
    
    if((shmid = shmget(_clefs.cle_quais, 0, 0)) == -1){
        perror("get_semid_shmid : shmget cle_quais");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_quais = shmid;
	quais = shmat(_clefs.shmid_quais, NULL, 0);

    /* segment de mémoire partagée contenant le type de la requete faite à pcap */
    
    if((shmid = shmget(_clefs.cle_shared_int, 0, 0)) == -1){
        perror("get_semid_shmid : shmget cle_shared_int");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_shared_int = shmid;
	shared_int = shmat(_clefs.shmid_shared_int, NULL, 0);

    /* sémaphore des chargements de conteneurs */
    
    if((semid = semget(_clefs.cle_chargmts_navires, 0, 0)) == -1){
        perror("get_semid_shmid : semget cle_chargmts_navires");
        exit(EXIT_FAILURE);
    }
    _clefs.semid_chargmts_navires = semid;

    /* sémaphore simulant les navires qui attendent au large qu'un quai se libère */

    if((semid = semget(_clefs.cle_navires_attente, 0, 0)) == -1){
        perror("init_semid_shmid : semget cle_navires_attente");
        exit(EXIT_FAILURE);
    }
    _clefs.semid_navires_attente = semid;

     
    if((shmid = shmget(_clefs.cle_fin_sim, 0, 0)) == -1){
        perror("init_semid_shmid : shmget cle_fin_sim");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_fin_sim = shmid;
	stop_simulation = shmat(_clefs.shmid_fin_sim, NULL, 0);

}

/** @brief Fonction qui attend qui quai se libère (ou que la fin de la simulation soit signalée)
 */
void attente_quai_libre(){
    /* on attend dans la liste d'attente des navires, au large */
    //P(navires_attente)
    sops.sem_num = 0; sops.sem_op = -1; sops.sem_flg = 0;
    print_info("attente_quai_libre : attente d'un quai libre...\n");
    if(semop(_clefs.semid_navires_attente, &sops, 1) == -1){
        perror("attente_quai_libre : semop P(navires_attente)");
        exit(EXIT_FAILURE);
    }

    /* on regarde si c'est la fin de la simulation */
    //si c'est le cas fin du prog
    if(*stop_simulation == true){
        print_info("attente_quai_libre : fin de simulation signalée. Fin du prog. Bye.\n");
        exit(EXIT_SUCCESS);
    }
    //sinon on récupère le quai libre
    else{
        print_info("attente_quai_libre : nouveau quai disponible.\n");
        requete_quai();
    }
}

/** @brief Fonction qui demande à pcap un quai disponible
 */
void requete_quai(){
    /* on notifie pcap qu'on veut faire une demande */

    //V(requete_pcap.a)
    sops.sem_num = 0; sops.sem_op = 1; sops.sem_flg = 0;
    print_info("requete_quai : notification de la demande de quai à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }
    /* on se met dans la file des progs qui demandent une requete */

    //P(requete_pcap.b)
    sops.sem_num = 1; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("requete_quai : en attente que pcap soit prêt à recevoir notre demande...\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* c'est notre tour : pcap en attente du type de la requete, on lui envoi */

    *shared_int = REQ_NAVIRE_QUAI;
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("requete_quai : type de la requete envoyé à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* on se met en attente du résultat */

    //P(requete_pcap.d)
    sops.sem_num = 3; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("requete_quai : en attente du résultat (n° quai).\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* resultat reçu : on récupère la valeur et débloque pcap, qui attendait qu'on la récupère */
    
    mon_quai = *shared_int;
    //on indique que le quai est réservé
    quais[mon_quai].quai_reserve = true;
    quais[mon_quai].nom = '\0';
    
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("requete_quai : résultat de la demande récupéré ! (quai n° %d). On notifie pcap.\n", mon_quai);
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* analyse du résultat */

    //si aucun quai disponible on attend qu'une place se libère
    if(mon_quai == -1){
        if(*stop_simulation == true){
            print_info("requete_quai : Fin de simulation active. Fin du prog. Bye.\n");
            exit(EXIT_SUCCESS);
        }
        else{
            print_info("requete_quai : Aucun quai disponible. On attend au large qu'une place se libère.\n");
            attente_quai_libre();
        }
        
    }
    //sinon on accoste
    else{
        print_info("requete_quai : Quai n° %d dispo.\n", mon_quai);
        accoster();
    }

}

int main(int argc, char ** argv){
    /* init DEBUG_PORT */
    init_DEBUG_PORT();

    /* vérification des arguments */
    if(argc != 5){
        printf("main : ERREUR : usage: %s <nom> <nb conteneurs> <tps accostage> <tps dechargt>\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    /* extraction des arguments */
    nom = argv[1][0];
    print_debug("main : nom du navire : %c\n", nom);
    nb_conteneurs = atoi(argv[2]);
    print_debug("main : nb de conteneurs : %d\n", nb_conteneurs);
    tps_accostage = atoi(argv[3]);
    print_debug("main : temps d'accostage : %d\n", tps_accostage);
    tps_dechargement = atoi(argv[4]);
    print_debug("main : temps de déchargement : %d\n", tps_dechargement);
    /* tests des arguments */
    if(nb_conteneurs <= 0 || tps_accostage < 0 || tps_dechargement < 0){
        printf("main : ERREUR : usage: %s <nom> <nb conteneurs> <tps accostage> <tps dechargt>\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    
    /* init des clées des sémaphores/mémoires partagées */
    init_IDS();

    /* récupération des sémaphores et segments de mémoire partagée necessaires à pnav */
    print_debug("main : récupération des sémaphores et sgmt. de mém. partagée...\n");
    get_semid_shmid();
    print_debug("Ok.\n");

    /* deamnde de quai libre à pcap */
    requete_quai();
    
    return EXIT_SUCCESS;
}