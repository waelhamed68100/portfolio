#include "utils.h"
#include "cles_partagees.h"
#include <pthread.h>
#include <unistd.h>

///Nom du navire qu'on doit décharger
char nom_navire;
///Nombre de camion dans la flotte 
int nb_camions;
///Temps de chargement d'un conteneur sur un camion
int tps_chargement;
///Nombre de conteneurs déchargés
int nb_c_decharges;

///quai du navire
int quai_navire;

///liste des quais du port
navire *quais;

int semid; //intercepteur des id des semaphores
union semun arg; //paramètre pour les semctl
int shmid; //intercepteur des id des segments de mémoire partagée
struct sembuf sops; //pour les P/V

///Le segmt de mém. partagée dans lequel sera indiqué le quai du navire
///et aussi utilisé pour indiquer le type de la requete (camion demande quai navire)
int *shared_int;

///Pas le droit d'utiliser le param de compilation c99 mdr
int i; //pour les boucles, only 80s kids will remember

/** @brief Fonction qui récupère les sémaphores et sgmt de mém. partagée necessaires à pfcam
 */
void get_semid_shmid(){

    /* sémaphore de signalement de requete */
    
    if((semid = semget(_clefs.cle_requete_pcap, 0, 0)) == -1){
        perror("get_semid_shmid : semget cle_requete_pcap");
        exit(EXIT_FAILURE);
    }
    _clefs.semid_requete_pcap = semid;
    
    /* segment de mémoire partagée contenant la liste des quais (et les navires qui y sont amarrés) */
    
    if((shmid = shmget(_clefs.cle_quais, 0, 0)) == -1){
        perror("get_semid_shmid : shmget cle_quais");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_quais = shmid;
	quais = shmat(_clefs.shmid_quais, NULL, 0);

    /* segment de mémoire partagée contenant le type de la requete faite à pcap */
    
    if((shmid = shmget(_clefs.cle_shared_int, 0, 0)) == -1){
        perror("get_semid_shmid : shmget cle_shared_int");
        exit(EXIT_FAILURE);
    }
    _clefs.shmid_shared_int = shmid;
	shared_int = shmat(_clefs.shmid_shared_int, NULL, 0);

    /* sémaphore des chargements de conteneurs */
    
    if((semid = semget(_clefs.cle_chargmts_navires, 0, 0)) == -1){
        perror("get_semid_shmid : semget cle_chargmts_navires");
        exit(EXIT_FAILURE);
    }
    _clefs.semid_chargmts_navires = semid;

}

/** @brief Fonction qui s'occupe de décharger le navire
 */
void decharger_navire(){
    print_info("-----------------------------------------\n");
    /* tant qu'il y a des conteneurs à charger */
    //note : on ignore le cas où il y a plus de camions que de conteneurs pour un chargement donné
    int nb_c = quais[quai_navire].nb_conteneurs; //pour éviter que pnav quitte le prog avant d'avoir déchargé le conteneur et qu'un autre navire écrase la valeur
    print_info("decharger_navire : déchargement des conteneurs...\n");
    while(nb_c){
        //peut être qu'il ne reste plus de camions dans la flotte
        if(nb_camions == 0){
            print_info("decharger_navire : tous les camions de la flotte ont été utilisé. Fin du job. Bye.\n");
            exit(EXIT_SUCCESS);
        }
        //sinon, on attend que le conteneur soit disponible pour le chargement
        print_debug("decharger_navire : en attente du prochain conteneur déchargé (conteneur %d)...\n", quais[quai_navire].nb_conteneurs);
        //P(chargmts.navire)
        sops.sem_num = quai_navire; sops.sem_op = -1; sops.sem_flg = 0;
        if(semop(_clefs.semid_chargmts_navires, &sops, 1) == -1){
            perror("decharger_navire : semop P(chargmts.navire)");
            exit(EXIT_FAILURE);
        }
        //on attend le temps de chargement
        print_debug("decharger_navire : conteneur dispnible. Attente du chargement sur le camion...\n");
        usleep(1000*tps_chargement);
        //on passe au conteneur suivant
        nb_c--;
        nb_camions--;
    }
    /* tous les conteneurs ont été déchargé */
    print_info("decharger_navire : tous les conteneurs ont été déchargé. Fin du job. Bye.\n");
    exit(EXIT_SUCCESS);
}

/** @brief Fonction qui demande à pcap le quai d'un navire
 */
void requete_quai_navire(){
    /* on notifie pcap qu'on veut faire une demande */

    //V(requete_pcap.a)
    sops.sem_num = 0; sops.sem_op = 1; sops.sem_flg = 0;
    print_info("requete_quai : notification de la demande du quai à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }
    /* on se met dans la file des progs qui demandent une requete */

    //P(requete_pcap.b)
    sops.sem_num = 1; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("requete_quai : en attente que pcap soit prêt à recevoir notre demande...\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* c'est notre tour : pcap en attente du type de la requete, on lui envoi */

    *shared_int = REQ_CAMION_QUAI_NAVIRE;
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("requete_quai : type de la requete envoyé à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* on attend que pcap soit prêt à receptionner le paramètre */

    //P(requete_pcap.d)
    sops.sem_num = 3; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("requete_quai : on attend que pcap soit prêt à réceptionner le param.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* envoi du paramètre */

    *shared_int = (int)nom_navire;
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("requete_quai : param (nom) envoyé à pcap.\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* on se met en attente du résultat */

    //P(requete_pcap.d)
    sops.sem_num = 3; sops.sem_op = -1; sops.sem_flg = 0;
    print_debug("requete_quai : en attente du résultat (n° quai).\n");
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* resultat reçu : on récupère la valeur et débloque pcap, qui attendait qu'on la récupère */
    
    quai_navire = *shared_int;
    //V(requete_pcap.c)
    sops.sem_num = 2; sops.sem_op = 1; sops.sem_flg = 0;
    print_debug("requete_quai : résultat de la demande récupéré ! (quai n° %d). On notifie pcap.\n", quai_navire);
    if(semop(_clefs.semid_requete_pcap, &sops, 1) == -1){
        perror("requete_quai : semop P(requete_pcap.a)");
        exit(EXIT_FAILURE);
    }

    /* analyse du résultat */

    //si le navire n'est pas présent dans le port
    if(quai_navire == -1){
        print_info("Navire pas présent dans le port. On annule. Bye.\n");
        exit(EXIT_FAILURE);
    }
    //sinon on le décharge
    else{
        print_info("Navire présent sur le quai n° %d. Déchargement...\n", quai_navire);
        nb_c_decharges = 0;
        decharger_navire();
    }
}

int main(int argc, char **argv){
    /* init DEBUG_PORT */
    init_DEBUG_PORT();

    /* vérification des arguments */
    if(argc != 4){
        printf("main : ERREUR : usage: %s <nom navire> <nb camions> <tps chargt conteneur>\n",argv[0]);
        exit(EXIT_FAILURE);
    }

    /* extraction des arguments */
    nom_navire = argv[1][0];
    print_debug("main : nom du navire : %c\n", nom_navire);
    nb_camions = atoi(argv[2]);
    print_debug("main : nb de camions dans la flotte : %d\n", nb_camions);
    tps_chargement = atoi(argv[3]);
    print_debug("main : temps de chargement d'un conteneur : %d\n", tps_chargement);
    /* vérification des arguments */
    if(nb_camions <= 0 || tps_chargement < 0){
        printf("main : ERREUR : param(s) invalide(s).\n");
        printf("main : ERREUR : usage: %s <nom> <nb conteneurs> <tps accostage> <tps dechargt>\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    /* init des clées des sémaphores/mémoires partagées */
    init_IDS();

    /* récupération des sémaphores et segments de mémoire partagée necessaires à pnav */
    print_debug("main : récupération des sémaphores et sgmt. de mém. partagée...\n");
    get_semid_shmid();
    print_debug("Ok.\n");

    /* deamnde du quai du navire */
    requete_quai_navire();

    return EXIT_SUCCESS;
}