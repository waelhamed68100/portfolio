#ifndef CLES_PARTAGEES_H
#define CLES_PARTAGEES_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h> //sémaphores
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>

#include "utils.h"
/*
#define bool int
#define true 1
#define false 0
*/
///Droits indiqués lors de la création des sémaphores / sgments de mém. partagée.
#define droits S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH

///Strcuture décrivant un navire/quai
typedef struct u_navire {
    ///indique si le quai est réservé
    bool quai_reserve;
    ///nom du navire
    char nom;
    ///nombre de conteneurs à décharger
    int nb_conteneurs;
    ///temps d'accostage en millisecondes
    unsigned int tps_accostage;
    ///temps de déchargement en millisecondes
    unsigned int tps_dechargement;
} navire;

///Structure représentant la demande du quai d'un navire par un camion
typedef struct u_c_demande_quai {
    ///navire concerné
    char nom;
    ///index du quai
    int quai;
} c_demande_quai;

///Struct contenant toutes les cléfs des sémaphores / segments de mémoire partagés
typedef struct u_IDS {
    ///clé de la sémphore pour faire une requte à pcap
    key_t cle_requete_pcap; 
    int semid_requete_pcap;
    ///programmes séquentiels : on peut attribuer plusieurs fonctions à la même variable partagée :
    ///clé du sgmt mém partagée pour pour indiquer le type de requete faite à pcap
    ///permet aussi d'indiquer un numéro de quai
    key_t cle_shared_int; 
    int shmid_shared_int; 
    ///clé du sgm. mém. partagée contenant la liste des quais
    key_t cle_quais;
    int shmid_quais;
    ///clé de la sémaphore simulant les chargements de conteneurs des navires
    key_t cle_chargmts_navires;
    int semid_chargmts_navires;
    ///clé de la sémaphore de la liste des navires qui attendent qu'une place se libère 
    key_t cle_navires_attente;
    int semid_navires_attente;
    ///clé du sgm. mém. partagée contenant le booléean indiquant si la fin de la sim. est active
    key_t cle_fin_sim;
    int shmid_fin_sim;
    
} IDS;

IDS _clefs;

/* types de requetes que pcap peut recevoir */

///Lorsqu'un navire veut entrer dans le port et demande un quai
#define REQ_NAVIRE_QUAI 1
///Lorsqu'un camion veut le quai d'un navire
#define REQ_CAMION_QUAI_NAVIRE 2
///Lorsqu'un navire quitte le port
#define REQ_NAVIRE_QUITTE_PORT 3
///Lorsque pstop signale la fin de simulation
#define REQ_STOP_SIMULATION 4
///Lorsque pdump demande le nb de quais
#define REQ_PDUMP 5

///Union necessaire, utilisé comme paramètre de semctl (le programme appelant doit la définir, malgrés sa présence dans sys/sem.h, chelou)
union semun {
    int              val;    /* Valeur pour SETVAL */
    struct semid_ds *buf;    /* Tampon pour IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Tableau pour GETALL, SETALL */
    struct seminfo  *__buf;  /* Tampon pour IPC_INFO
                                (spécifique à Linux) */
};

/* toutes les constantes suivantes ne doivent jamais être utilisées ! */
/* il faut seulement utiliser les identifiants contenus dans IDS */

///1er paramètre du ftok de TOUTES les sémaphore 
///(c'est le 2ème paramètre proj_id qui est variable)
#define identifiant_semaphore "./pcap.c"

//initialise les identifiants des sémaphores/sgmt mem. partagée
void init_IDS();


#endif //CLES_PARTAGEES_H