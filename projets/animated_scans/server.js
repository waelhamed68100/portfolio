///variables globals
const _ = require('./globalVars.js');

///on utlise express pour simplifier la vie
var express = require('express');
var app = express();
global.app = app;

///on met à disposition tout le dossier public sous l'url '/serv/*'
app.use('/serv', express.static('public')); //'/serv' = faux système de fichier que le client doit utiliser


///pour n'importe quelle requette, cette methode est appellée
app.use(function (req, res, next) {
    console.log("Requête reçue...\n"
        + '' + new Date()
        + "\nIP: " + req.ip
        + "\nreq: " + req.method + ' ' + req.url
        + '\n');
    next(); //méthode middleman, il faut passer à la vraie méthode correspondante
});

///page d'accueil
app.get('/tool', function (req, res) {
    res.sendFile(_.PATH_PUBLIC_FILES+'/tool.html');
});

app.get('/chapter354', function (req, res) {
    res.sendFile(_.PATH_PUBLIC_FILES+'/chapter354.html');
});

//req inconnue
app.use('*', function (req, res) {
    res.sendStatus(404);
});

///on lance l'écoute sur le serveur
app.listen(_.PORT, () => {
    console.log('Serveur lancé sur le port ' + _.PORT);
    console.log('urls : ');
    _.URLS.forEach(url => {
        console.log('\t' + url + "/chapter354");
    });
    //console.log(process.env);

});