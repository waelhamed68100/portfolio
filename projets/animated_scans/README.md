# Animation d'images statiques

Année : 2017.

Prototype d'animation d'images (png, jpg). 

Le programme charge des images (des scans de bandes dessinées en l'occurrence) et utilise des données précalculées pour découper les images et les animer. Les animations sont visible depuis une page web. Chaque clic de souris permet de passer à l'animation suivante.

Le traitement des images (découpage, translation...) se fait grâce à la bibliothèque javascript Pixi.js.

Le code source et l'interface sont rustiques car ce projet n'est qu'un prototype. Avec plus de temps ce projet pourrait par exemple être une plateforme de création et de partage d'images statiques, mais idée abandonnée pour des raisons de droits d'auteur (images de bande dessinées, par exemple).

![gif introuvable](./preview_animated_scans.gif "gif preview, 5 fps")

[Lien vers une vidéo de démonstration](https://drive.google.com/file/d/1LPG71OGf8IjaqCQS_jLVPMV75IiWTdvW/view?usp=sharing ) 

(note: la vidéo a comprimé les animations de 60 fps vers 24 fps)

## Dépendances
---
* NodeJS, pour servir les fichiers (version v8.9.4)
* NPM, pour installer les dépendances module de node pour ce projet (version v4.8.6)

note : les versions utilisées sont à titre indicatif, il est fort probable que des versions antérieurs fonctionnent aussi.

## Exécution
---
* version simple : éxecuter le fichier `run.sh`

## Utilisation
---
Après lancement du serveur, une url sera indiquée dans le terminal. 

Accédez à cette url depuis un navigateur moderne pour visualiser les animations.

Note : une page noire est affichée lors de l'accès à la page web. Une fois que toutes les données sont chargées (les images scannées), on peut cliquer n'importe où sur la page pour passer à l'animation suivante. (interface rustique donc pas de barre de chargement...)


