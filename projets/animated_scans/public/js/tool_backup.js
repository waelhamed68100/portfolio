let type = "WebGL"
if (!PIXI.utils.isWebGLSupported()) {
    type = "canvas"
}

PIXI.utils.sayHello(type)


//Aliases
let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite,
    Graphics = PIXI.Graphics;
//Create a Pixi Application
let app = new Application();

//renderer takes full screen space available at launch
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);


//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);

var images_url = [
    'serv/scans/0354-001.jpg',
];
//load an image and run the `setup` function when it's done
loader
    .add('0354-001', images_url[0])
    .load(setup);

var polygon = [];
var pageContainer;
var padding = 15;
var ratio; 
function setup() {

    console.log('scan page 1 loaded');

    pageContainer = new PIXI.Container();

    


    var scan = new PIXI.Sprite(new PIXI.Texture(resources['0354-001'].texture));

    pageContainer.scan = scan;
    pageContainer.addChild(scan);

    pageContainer.debugGraphicsContainer = new PIXI.Container();
    pageContainer.addChild(pageContainer.debugGraphicsContainer);
    pageContainer.debugGraphicsContainer.graphics = new Graphics();

    scan.interactive = true;
    scan.on('pointerdown', function (e) {
        console.log('scan cliqué');
        //console.log(e);
        console.log(this);

        var e_x = e.data.global.x;
        var e_y = e.data.global.y;
        var x = e_x - this.parent.x;
        var y = e_y - this.parent.y;
        console.log({ x, y });

        //creation du point 
        this.parent.debugGraphicsContainer.graphics.lineStyle(2, 0xFF00FF);  //(thickness, color)
        this.parent.debugGraphicsContainer.graphics.drawCircle(x, y, 5);   //(x,y,radius)
        this.parent.debugGraphicsContainer.graphics.endFill();
        this.parent.debugGraphicsContainer.addChild(this.parent.debugGraphicsContainer.graphics);

        var d = polygon[0];
        var radius_start = 15;
        //si le chemin comporte au moins 3 points
        //et qu'on est assez proche du point de départ
        if (polygon.length >= 3 
            && Math.abs(x - d.x) < radius_start && Math.abs(y - d.y) < radius_start) {
            //on ferme le circuit
            polygon.push(d);
            console.log('new polygon created');
            //on dessine le polygone
            this.parent.debugGraphicsContainer.graphics.lineStyle(2, 0xFF00FF);  //(thickness, color)
            this.parent.debugGraphicsContainer.graphics.drawPolygon(polygon);   //(x,y,radius)
            this.parent.debugGraphicsContainer.graphics.endFill();
            this.parent.debugGraphicsContainer.addChild(this.parent.debugGraphicsContainer.graphics);
            //on découpe le panel pour tester si c'est correct
            cut_panel(polygon);
            //on passe au polygone suivant
            polygon = [];
            
        }
        else{

            //ajoute le point clické au chemin
            polygon.push(new PIXI.Point(x,y));
        }
    })

    app.renderer.resize(scan.width * 2, scan.height );
    app.stage.addChild(pageContainer);
}

var panel_preview_container = new PIXI.Container();
var animation = [];

function cut_panel(polygon) {

    console.log('creating a sprite from the the polygon zone');
    

    //on determine le rectangle de découpage
    var left, right, top, bottom;
    left = right = polygon[0].x;
    top = bottom = polygon[0].y;
    //on cherche les limites de tous les points du polygone
    for (let i = 0; i < polygon.length; i++) {
        var p = polygon[i];
        if (p.x < left) left = p.x;
        if (p.x > right) right = p.x;
        if (p.y < top) top = p.y;
        if (p.y > bottom) bottom = p.y;
    }
    //on créer le rectangle
    var rectangle = new PIXI.Rectangle(left, top, right - left, bottom - top);
    //on affiche le rectangle à l'écran
    console.log('red = cutting rectangle');
    pageContainer.debugGraphicsContainer.graphics.lineStyle(2, 0xff0000);  //(thickness, color)
    pageContainer.debugGraphicsContainer.graphics.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);   //(x,y,radius)
    pageContainer.debugGraphicsContainer.graphics.endFill();
    pageContainer.debugGraphicsContainer.addChild(pageContainer.debugGraphicsContainer.graphics);
    


    panel_preview_container.x = (2*padding) + pageContainer.scan.width;
    panel_preview_container.y = padding;
    var pannel_sprite =  new Sprite(new PIXI.Texture(resources['0354-001'].texture, rectangle)); 
    panel_preview_container.addChild(pannel_sprite);
    pageContainer.addChild(panel_preview_container);

    var maskPoly = new PIXI.Graphics();
    maskPoly.beginFill(0x555555)
    var poly_offsetted = [];
    polygon.forEach(p => {
        poly_offsetted.push(new PIXI.Point(p.x-left, p.y-top));
    });
    maskPoly.drawPolygon(poly_offsetted);
    maskPoly.endFill();
    panel_preview_container.addChild(maskPoly);
    pannel_sprite.mask = maskPoly;

    var animate = {
        polygon,
        transition: 'appear',
    }
    console.log(JSON.stringify(polygon));
    console.log(JSON.stringify(rectangle));
    console.log(JSON.stringify(animate));
}