let type = "WebGL"
if (!PIXI.utils.isWebGLSupported()) {
    type = "canvas"
}

PIXI.utils.sayHello(type)


//Aliases
let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite,
    Graphics = PIXI.Graphics;
//Create a Pixi Application
let app = new Application();

//renderer takes full screen space available at launch
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);


//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);


//load an image and run the `setup` function when it's done
loader
    .add('0354-001', 'serv/scans/0354-001.jpg')
    .add('0354-003', 'serv/scans/0354-003.jpg')
    .load(setup);



var polygon = [];
var windowContainer = new PIXI.Container();
windowContainer.width = window.innerWidth;
windowContainer.height = window.innerHeight;
windowContainer.interactive = true;
windowContainer.hitArea = new PIXI.Rectangle(0, 0, 100000, 100000);

app.stage.addChild(windowContainer);

var page;

var padding = 15;
var ratio;

var animation = [
    {
        page: '0354-001',
        panels: [
            {
                polygon: JSON.parse('[{"x":770,"y":4},{"x":834,"y":1590},{"x":1038,"y":1592},{"x":1058,"y":8},{"x":770,"y":4}]'),
                transition: 'appear',
            },
            {
                polygon: JSON.parse('[{"x":595.0016095741256,"y":2},{"x":586.0015852276262,"y":1598.0908813476562},{"x":811.0021938901107,"y":1598.0908813476562},{"x":755.0020424007812,"y":1},{"x":595.0016095741256,"y":2}]'),
                transition: 'appear',
            },
            {
                polygon: JSON.parse('[{"x":318.00086024297804,"y":2},{"x":316.00085483264485,"y":1599.0908813476562},{"x":573.0015500604604,"y":1597.0908813476562},{"x":579.00156629146,"y":2},{"x":318.00086024297804,"y":2}]'),
                transition: 'appear',
            },
            {
                polygon: JSON.parse('[{"x":172,"y":1079},{"x":205,"y":1083},{"x":231,"y":1087},{"x":255,"y":1102},{"x":271,"y":1119},{"x":284,"y":1137},{"x":282,"y":1170},{"x":281,"y":1201},{"x":277,"y":1226},{"x":271,"y":1242},{"x":251,"y":1260},{"x":243,"y":1280},{"x":236,"y":1304},{"x":234,"y":1321},{"x":227,"y":1335},{"x":214,"y":1357},{"x":211,"y":1372},{"x":193,"y":1387},{"x":186,"y":1396},{"x":182,"y":1407},{"x":180,"y":1427},{"x":180,"y":1461},{"x":169,"y":1502},{"x":120,"y":1513},{"x":98,"y":1502},{"x":93,"y":1464},{"x":76,"y":1445},{"x":65,"y":1440},{"x":50,"y":1443},{"x":24,"y":1422},{"x":9,"y":1406},{"x":9,"y":1361},{"x":4,"y":1314},{"x":11,"y":1273},{"x":16,"y":1235},{"x":53,"y":1212},{"x":57,"y":1190},{"x":82,"y":1176},{"x":88,"y":1164},{"x":86,"y":1146},{"x":97,"y":1121},{"x":111,"y":1100},{"x":126,"y":1089},{"x":147,"y":1081},{"x":172,"y":1079}]'),
                transition: 'appear'
            },
            {
                polygon: JSON.parse('[{"x":1.0000027051665976,"y":2},{"x":2,"y":1599},{"x":313,"y":1599},{"x":312,"y":0},{"x":1.0000027051665976,"y":2}]'),
                transition: 'appear',
            }
        ]
    }
];
var animation_page = -1;
var animation_panel = -1;
var current_page;
var current_panel;

function setup() {
    console.log('all scans loaded');


    windowContainer.on('pointerdown', function (e) {
        //si aucune page n'est chargée en mémoire on charge la page suivante
        if (!current_page) {
            console.log('should load next page');
            load_next_page();
        }
        //sinon on peut commencer à animer les panels
        else {
            animate_next_panel();
        }

    })
}

function load_next_page() {

    //on regarde si on est arrivé au bout du chapitre
    if (animation.length -1 === animation_page) {
        console.log('all pages have been shown');
    }
    else {
        //on passe à la page suivante
        animation_page++;
        console.log('loading page ' + animation_page);
        //on selectionne les données de l'animation de la bonne page
        current_page = animation[animation_page];
        var page_name = current_page.page;
        console.log('current page name : ' + page_name);
        //on ajoute le container à la fenetre
        page = new PIXI.Container();
        windowContainer.addChild(page);
        windowContainer.page = page;
        //on charge les panels
        for (let i = 0; i < current_page.panels.length; i++) {
            var polygon = current_page.panels[i].polygon;
            console.log('creating a sprite from the the polygon zone');
            //on determine le rectangle de découpage
            var left, right, top, bottom;
            left = right = polygon[0].x;
            top = bottom = polygon[0].y;
            //on cherche les limites de tous les points du polygone
            for (let i = 0; i < polygon.length; i++) {
                var p = polygon[i];
                if (p.x < left) left = p.x;
                if (p.x > right) right = p.x;
                if (p.y < top) top = p.y;
                if (p.y > bottom) bottom = p.y;
            }
            //on créer le rectangle
            var rectangle = new PIXI.Rectangle(left, top, right - left, bottom - top);
            //on créer le container du sprite et de son mask
            var panelContainer = new PIXI.Container();
            //on créer le sprite et on l'ajoute au container
            var pannel_sprite = new Sprite(new PIXI.Texture(resources[page_name].texture, rectangle));
            var ratio = pannel_sprite.width / pannel_sprite.height;
            var reduc_percentage = window.innerHeight / pannel_sprite.height;
            console.log('reduc_percentage : ');
            console.log(reduc_percentage);
            pannel_sprite.height = window.innerHeight;
            pannel_sprite.width = ratio * pannel_sprite.height;
            panelContainer.addChild(pannel_sprite);
            
            //on créer le mask du sprite et on l'ajoute au container
            var maskPoly = new PIXI.Graphics();
            maskPoly.beginFill(0x555555)
            var poly_offsetted = [];
            polygon.forEach(p => {
                poly_offsetted.push(new PIXI.Point((p.x - left)*reduc_percentage, (p.y - top)*reduc_percentage));
            });
            maskPoly.drawPolygon(poly_offsetted);
            maskPoly.endFill();
            pannel_sprite.mask = maskPoly;
            panelContainer.addChild(maskPoly);
            //on scale pour que le scan soit en plein écran
            console.log('panelContainer :');
            console.log(panelContainer);
            //current_page.panels[i].ancre = { x: left, y: top };
            panelContainer.x = left*reduc_percentage;
            panelContainer.y = top*reduc_percentage;
            //on ajoute le container aux données de l'animation
            current_page.panels[i].container = panelContainer;
            
        }
        console.log('all panels from current page are loaded');
        console.log(current_page);
    }

}

function animate_next_panel() {
    animation_panel++;
    //si on a dépassé la dernière animation de la page courrante
    //alors on passe à la page suivante
    if (current_page.panels.length === animation_panel) {
        console.log('all animations of page ' + current_page.page + ' done. Should load next page.');
        current_page = null;
        //destruction de la page courrante
        windowContainer.removeChild(page);
        page.destroy({ children: true, texture: true, baseTexture: true });
    }
    else {
        //on affiche le bon panel
        current_panel = current_page.panels[animation_panel];
        console.log(current_panel);
        console.log('rendering the next panel');
        page.addChild(current_panel.container);
    }
}
