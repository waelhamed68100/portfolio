let type = "WebGL"
if (!PIXI.utils.isWebGLSupported()) {
    type = "canvas"
}

PIXI.utils.sayHello(type)


//Aliases
let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite,
    Graphics = PIXI.Graphics;
//Create a Pixi Application
let app = new Application();

//renderer takes full screen space available at launch
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);


//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);


//load an image and run the `setup` function when it's done
loader
    .add(pages)
    .load(setup);



var polygon = [];
var windowContainer = new PIXI.Container();
windowContainer.width = window.innerWidth;
windowContainer.height = window.innerHeight;
windowContainer.interactive = true;
windowContainer.hitArea = new PIXI.Rectangle(0, 0, 100000, 100000);

app.stage.addChild(windowContainer);

var displacementSprite = PIXI.Sprite.fromImage('serv/js/displacementSprite.png');
displacementSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
var displacementFilter = new PIXI.filters.DisplacementFilter(displacementSprite);
app.stage.addChild(displacementSprite);

function disp(displ_sprite) {
    displ_sprite.x += 0.5;
    displ_sprite.y += 0.1;
    setTimeout(disp, 1000 / 60, displ_sprite);
}
setTimeout(disp, 0, displacementSprite);

var page;

var padding = 15;
var ratio;



//var animation_page = 0;
var animation_page = 0;
var animation_panel = -1;
var current_page;
var current_panel;


function animate(panel) {
    var anim = panel.animation;
    if (panel.container && anim) {
        for (let i = 0; i < anim.length; i++) {
            let a = anim[i];
            if (a.type === 'slide_down') {
                if (!a.initial_position) {
                    a.initial_position = { y: panel.container.y };
                    panel.container.y -= a.start_offset.y;
                    panel.container.visible = true;
                }
                //condition d'arrêt
                if (panel.container.y < a.initial_position.y) {
                    panel.container.y += a.speed;
                    a.speed = a.speed + a.acceleration >= 1 ? a.speed + a.acceleration : 1;
                    setTimeout(animate, 1000 / 60, panel);
                }
                //return;
            }
            if (a.type === 'slide_up') {
                if (!a.initial_position) {
                    a.initial_position = { y: panel.container.y };
                    panel.container.y += a.start_offset.y;
                    panel.container.visible = true;
                }
                //condition d'arrêt
                if (panel.container.y > a.initial_position.y) {
                    panel.container.y -= a.speed;
                    a.speed = a.speed + a.acceleration >= 1 ? a.speed + a.acceleration : 1;
                    setTimeout(animate, 1000 / 60, panel);
                }
                //return;
            }
            if (a.type === 'slide_right') {
                if (!a.initial_position) {
                    a.initial_position = { x: panel.container.x };
                    panel.container.x -= a.start_offset.x;
                    panel.container.visible = true;
                }
                //condition d'arrêt
                if (panel.container.x < a.initial_position.x) {
                    panel.container.x += a.speed;
                    a.speed = a.speed + a.acceleration >= 1 ? a.speed + a.acceleration : 1;
                    setTimeout(animate, 1000 / 60, panel);
                }
                //return;
            }
            if (a.type === 'slide_left') {
                if (!a.initial_position) {
                    a.initial_position = { x: panel.container.x };
                    panel.container.x += a.start_offset.x;
                    panel.container.visible = true;
                }
                //condition d'arrêt
                if (panel.container.x > a.initial_position.x) {
                    panel.container.x -= a.speed;
                    a.speed = a.speed + a.acceleration >= 1 ? a.speed + a.acceleration : 1;
                    setTimeout(animate, 1000 / 60, panel);
                }
                //return;
            }
            if (a.type === 'floaty') {
                if (!a.initial_position) {
                    
                    panel.container.visible = true;
                    var grow_percentage = 15;
                    grow_percentage /= 100;
                    panel.container.scale.x *= 1 + grow_percentage;
                    panel.container.scale.y *= 1 + grow_percentage;
                    panel.container.x -= panel.container.width * grow_percentage;
                    panel.container.y -= (panel.container.height * grow_percentage);
                    panel.container.x += panel.container.width * a.float_correction.x;
                    panel.container.y += panel.container.height * a.float_correction.y;
                    a.initial_position = { y: panel.container.y };
                }

                panel.container.y = a.initial_position.y + Math.sin(+ new Date() / 300) * (a.offset.y);
                setTimeout(animate, 1000 / 60, panel);
                return;
            }

            if (a.type === 'fade_in') {
                if (panel.container.visible === false) {
                    panel.container.visible = true;
                    panel.container.alpha = a.start_alpha;
                }

                //condition d'arrêt
                if (panel.container.alpha > 1) {
                    panel.container.alpha = 1;
                    return;
                }
                panel.container.alpha += a.speed;
                a.speed += a.acceleration;
                setTimeout(animate, 1000 / 60, panel);
                //return;
            }

            //console.log('transition error : unknown type');
            //console.log(a.type);
        }
    }
}



function setup() {
    console.log('all scans loaded');


    windowContainer.on('pointerdown', function (e) {
        //si aucune page n'est chargée en mémoire on charge la page suivante
        if (!current_page) {
            console.log('should load next page');
            load_next_page();
        }
        //sinon on peut commencer à animer les panels
        else {
            animate_next_panel();
        }

    })
}

function load_next_page() {

    //on regarde si on est arrivé au bout du chapitre
    if (animation.length - 1 === animation_page) {
        console.log('all pages have been shown');
    }
    else {
        //on passe à la page suivante
        animation_page++;
        console.log('loading page ' + animation_page);
        //on selectionne les données de l'animation de la bonne page
        current_page = animation[animation_page];
        var page_name = current_page.page;
        console.log('current page name : ' + page_name);
        //on ajoute le container à la fenetre
        page = new PIXI.Container();

        windowContainer.addChild(page);
        windowContainer.page = page;
        //on charge les panels
        var scan = new Sprite(new PIXI.Texture(resources[page_name].texture));
        console.log('scan : ');
        console.log(scan);

        var ratio = scan.width / scan.height;
        var reduc_percentage = window.innerHeight / scan.height;
        page.x = (window.innerWidth / 2) - (scan.width * reduc_percentage / 2);
        for (let i = 0; i < current_page.panels.length; i++) {
            var polygon = current_page.panels[i].polygon;
            //on créer le rectangle
            var rectangle = new PIXI.Rectangle(0, 0, scan.width, scan.height);

            //on créer le container du sprite et de son mask
            var panelContainer = new PIXI.Container();

            //on créer le sprite et on l'ajoute au container
            var pannel_sprite = new Sprite(new PIXI.Texture(resources[page_name].texture), rectangle);


            //on créer le mask du sprite et on l'ajoute au container
            var maskPoly = new PIXI.Graphics();
            maskPoly.beginFill(0x555555)

            var poly_offsetted = [];
            poly_offsetted.push(new PIXI.Point(0, 0));
            poly_offsetted.push(new PIXI.Point(0, scan.height));
            poly_offsetted.push(new PIXI.Point(0, 0));
            polygon.forEach(p => {
                poly_offsetted.push(new PIXI.Point(p.x, p.y));
            });
            poly_offsetted.push(new PIXI.Point(0, 0));
            maskPoly.drawPolygon(poly_offsetted);

            maskPoly.drawPolygon(polygon);
            maskPoly.endFill();
            pannel_sprite.mask = maskPoly;

            panelContainer.addChild(maskPoly);
            panelContainer.addChild(pannel_sprite);

            if (current_page.panels[i].filter) {
                if (current_page.panels[i].filter === 'DisplacementFilter') {


                    panelContainer.filters = [displacementFilter];
                }
            }

            console.log('window.innerHeight = ' + window.innerHeight);
            panelContainer.height = window.innerHeight;
            console.log('panelContainer.height = ' + panelContainer.height);
            panelContainer.width = reduc_percentage * panelContainer.width;
            console.log('panelContainer.width = ' + panelContainer.width);

            //maskPoly.height = 50;

            //on scale pour que le scan soit en plein écran
            console.log('panelContainer :');
            console.log(panelContainer);


            //on ajoute le container aux données de l'animation
            current_page.panels[i].container = panelContainer;

        }
        console.log('all panels from current page are loaded');
        console.log(current_page);
        animate_next_panel();
    }

}

function animate_next_panel() {
    animation_panel++;
    //si on a dépassé la dernière animation de la page courrante
    //alors on passe à la page suivante
    if (current_page.panels.length === animation_panel) {
        console.log('all animations of page ' + current_page.page + ' done. Should load next page.');
        current_page = null;
        animation_panel = -1;
        //destruction de la page courrante
        windowContainer.removeChild(page);
        page.destroy({ children: true, texture: true, baseTexture: true });
        load_next_page();
    }
    else {
        //on affiche le bon panel
        current_panel = current_page.panels[animation_panel];
        console.log(current_panel);
        console.log('rendering the next panel');
        if (current_panel.animation) {
            current_panel.container.visible = false;
            setTimeout(animate, 0, current_panel);
        }

        if (current_panel.always_on_top_flag) {
            function stay_on_top(panel) {
                //console.log(panel.container.parent);
                let parent = panel.container.parent;
                if (panel.container && parent) {
                    parent.removeChild(panel.container);
                    parent.addChild(panel.container);
                    setTimeout(stay_on_top, 100, panel);
                }
            }
            setTimeout(stay_on_top, 0, current_panel);
        }

        page.addChild(current_panel.container);

    }
}
