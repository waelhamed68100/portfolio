
//port local (utilisé lorsque les tests se font sur le pc portable)
const LOCAL_PORT = 3000;
//port du serveur
const WEB_PORT = process.env.PORT || 1337;
//port utilisé
const PORT_USED = process.env.USER === 'joseffu' ? LOCAL_PORT : WEB_PORT;
//fonction qui determine les urls d'accès à cette application
function findURLS(){
    //console.log('findURL appelée');
    var os = require( 'os' );
    var networkInterfaces = os.networkInterfaces( );
    //console.log(networkInterfaces);
    var res = [];
    //on itère sur chacune des interfaces
    for(var k in networkInterfaces){
        var i = networkInterfaces[k];
        //on itère sur chacune des adresse de l'interface
        i.forEach(addr => {
            if(!addr.internal && (!addr.scopeid || addr.scopeid === 0)){
                if(addr.family === 'IPv4')
                    res.push('http://'+addr.address+':'+PORT_USED);
                else if(addr.family === 'IPv6')
                    res.push('http://['+addr.address+']:'+PORT_USED);
            }
        });
    }
    return res;
}

module.exports = Object.freeze({
    //port utilisé par l'application
    PORT :   PORT_USED,
    ///urls vers cette application
    URLS : findURLS(),
    ///chemin absolu des fichiers publics
    PATH_PUBLIC_FILES : __dirname + '/public',
});