# Regroupement de projets

Ce dépôt contient quelques projets informatiques sur lesquels j'ai travaillé ces dernières années. Ils peuvent être personnels ou en lien avec mes études.

# [Sommaire](#sommaire)

- [I. Projets personnels](#i-projets-personnels)
    - [Application React NodeJS](#application-react-nodejs)
    - [Ray tracer stochastique](#ray-tracer-stochastique)
    - [Animation d'images statiques](#animation-dimages-statiques)
    - [Petite application de chat](#petite-application-de-chat)

- [II. Projets pédagogiques](#ii-projets-pédagogiques)
    - [Code correcteur d'erreurs](#code-correcteur-derreurs)
    - [Calculateur réparti](#calculateur-réparti)
    - [Simulation d'un port](#simulation-dun-port)



[I. Projets personnels](#sommaire)
========================================================================
## [Application React NodeJS](#sommaire)
------------------------------------------------------------
Année : 2023

Application simulant un site de vente de plantes. La partie front-end est gérée avec React tandis que la partie back-end est gérée par NodeJS.
Application sous architecture MVC.

[Lien vers le projet](/projets/application-node-react)

![image introuvable](./projets/application-node-react/screenshots/app_react.png "page articles")

## [Ray tracer stochastique](#sommaire)
------------------------------------------------------------
Année : 2019.

Génération d'images par lancés aléatoires de rayons (path tracing). Technologie : uniquement c++ et ses bibliothèques de base (et accélération mutlithreads cpu par openmp, optionnelle). 

[Lien vers le projet](/projets/projet_raytracing)

Exemples de rendus : 

![image introuvable](./projets/projet_raytracing/img/scene_a_2000x1000-300r_164s.png "boules colorées (300 rayons par pixel)")

![image introuvable](./projets/projet_raytracing/img/bvh_menger_100r_269sec.png "éponge de Menger (100 rayons par pixel)")

![image introuvable](./projets/projet_raytracing/img/boite_cornell.png "boîte de Cornell (100 rayons par pixel)")

![image introuvable](./projets/projet_raytracing/img/couleurs_lumiere.png "matériau lumineux (100 rayons par pixel)")


## [Animation d'images statiques](#sommaire)
------------------------------------------------------------------
Année : 2017.

Prototype d'animation d'images (png, jpg). 

Le programme charge des images (des scans de bandes dessinées en l'occurrence) et utilise des données précalculées pour découper les images et les animer. Les animations sont visibles depuis une page web. Chaque clic de souris permet de passer à l'animation suivante.

Gif preview :
![gif introuvable](./projets/animated_scans/preview_animated_scans.gif "preview gif 5 fps")

[Lien vers une vidéo de démonstration](https://drive.google.com/file/d/1LPG71OGf8IjaqCQS_jLVPMV75IiWTdvW/view?usp=sharing ) 

(note: la vidéo a comprimé les animations de 60 fps vers 24 fps)

[Lien vers le projet](/projets/animated_scans )



## [Petite application de chat](#sommaire)
---
Année : 2018.

Simple application c++ avec interface graphique (Qt/qml) permettant de créer un serveur de chat local (websocket).

Screenshots :
![screenshot introuvable](./projets/chat_qt_qml/screenshots/chatroom_full.png "tentative connection salle pleine")

![screenshot introuvable](./projets/chat_qt_qml/screenshots/message+disconnection.png "utilisateur (hors host) se déconnecte")

![screenshot introuvable](./projets/chat_qt_qml/screenshots/host_disconnect.png "host se déconnecte")

[Lien vers le projet](/projets/chat_qt_qml )



[II. Projets pédagogiques](#sommaire)
=========

## [Code correcteur d'erreurs](#sommaire)
------------------------------------------------------------
Année : 2019

Programme C qui a pour but de détecter des erreurs de transmission sur un réseau (quelques bits inversés par octet transmis) et de les corriger si possible, grâce aux calculs polynomiaux.
Points particuliers : gestion de l'encodage et du décodage.

    [...]

    DECODING : bits recus :

    'cw[0] : t' -> 00111101 01110100 (b) <-> 15732 (d)
    'cw[1] : e' -> 11110101 01100101 (b) <-> 4294964581 (d)
    'cw[2] : s' -> 01111100 01110011 (b) <-> 31859 (d)
    'cw[3] : p' -> 00101101 01110000 (b) <-> 11632 (d)

    ------- Matrice de décodage H ( . = 0 ) :

    1 . . . . . . . . . 1 1 1 . . . 
    . 1 . . . . . . . . . 1 1 1 . . 
    . . 1 . . . . . 1 . . . 1 1 1 . 
    . . . 1 . . . . . 1 . . . 1 1 1 
    . . . . 1 . . . . . . 1 1 . 1 1 
    . . . . . 1 . . 1 . 1 1 . 1 . 1 
    . . . . . . 1 . 1 1 1 . . . 1 . 
    . . . . . . . 1 . 1 1 1 . . . 1 


    DECODING: détection d'erreurs par calcul matriciel (H) :

    [...]


[Lien vers le projet](/projets/code_correcteur_erreur )


## [Calculateur réparti](#sommaire)
------------------------------------------------------------
Année : 2018

Programme C d'un calculateur réparti. 

Un programme (appelé orchestrateur) orchestre l'envoi de calcul de calculs (comme une simple division, par exemple) à d'autres programmes appelés noeuds de calculs.

A la réception des requêtes de l'orchestrateur, ces noeuds de calculs executent le calcul (temps arbitrairement long de plusieurs secondes) puis envoient le résultat à l'orchestrateur.

Les messages sont échangés par **ipv6** ou **ipv4** (au choix) par le biais de **sockets udp C unix**.

L'orchestrateur utilise des **threads unix** pour empêcher les requêtes d'être bloquantes.

-> Un exemple d'éxecution est présent dans le README.md du projet. 

[Lien vers le projet](/projets/calculateur_reparti )


## [Simulation d'un port](#sommaire)
------------------------------------------------------------
Année : 2018.

Programmes C de simulation d'un port (pour navires).

Le but de ce projet était d'utiliser uniquement les sémaphores et les segments de mémoire partagées IPC System V (des OS unix) pour gérer de multiples synchronisations entre les programmes. 

La simulation est composées de 3 programmes principaux : 
 * **pcap** : simule un port composé d'un certain nombre de quais.
 * **pnav** : simule un navire voulant entrer dans le port.
 * **pcam** : simule un camion de déchargment des conteneurs de navires.

 La simulation comporte plusieurs types de synchronisations : 
 * Les navires attendent au large qu'un quai soit disponible.
 * Les camions attendent que la grue de déchargement des conteneurs soit disponible.

(note: voir le README.md du projet pour un exemple d'exécution)

[Lien vers le projet](/projets/simulation_port_navires )
